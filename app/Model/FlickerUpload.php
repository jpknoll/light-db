<?php
// app/Model/FlickerUpload.php

class FlickerUpload extends AppModel {
	
	public $actsAs = array(
		'Containable',
	);
	
	public $hasMany = array(
		'FlickerUploadAttachments' => array(
			'className' => 'FlickerUploadAttachment',
		),
		'FlickerUploadFacts' => array(
			'className' => 'FlickerUploadFact',
		),
		'FlickerUploadData' => array(
			'className' => 'FlickerUploadData',
		),
	);
	
	public $belongsTo = array(
		'User' => array(
			'className' => 'AppUser',
		),
	);
}