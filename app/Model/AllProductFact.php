<?php

App::uses('ProductFact', 'Model');

class AllProductFact extends ProductFact {
	
	public $useTable = 'all_product_facts';
	
}