<?php
// app/Model/Group.php

class Group extends AppModel {
	
	public $hasMany = array(
		'Users' => array(
			'className' => 'User',
			'foreignKey' => 'group_id',
			'table' => 'users',
		),
	);
}