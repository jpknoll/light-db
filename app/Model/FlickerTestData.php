<?php

App::uses('AppModel', 'Model');

class FlickerTestData extends AppModel {
	public $useTable = 'flicker_test_data';
	
	public $displayField = 'id';
	
	public $belongsTo = array(
		'FlickerTest' => array(
			'className' => 'FlickerTest',
		),
	);
	
	public $_schema = array(
		'filter_frequency',
		'flicker_index',
		'flicker_percent',
		'fundamental_frequency',
	);
};
