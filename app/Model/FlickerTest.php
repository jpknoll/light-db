<?php

App::uses('AppModel', 'Model');

class FlickerTest extends AppModel {
	public $actsAs = array(
		'Containable', 
		'Search.Searchable'
	);
	
	public $useTable = 'flicker_tests';
	
	public $displayField = 'id';
	
	public $belongsTo = array(
		'Sample' => array(
			'className' => 'Sample',
		),
		'Dimmer' => array(
			'className' => 'Dimmer',
		),
	); 
	
	public $hasMany = array(
		'FlickerTestData' => array(
			'className' => 'FlickerTestData',
			'order' => 'FlickerTestData.filter_frequency ASC',
		),
		'FlickerTestFacts' => array(
			'className' => 'FlickerTestFact',
		),
	);
	
	public $filterArgs = array(
		'product_id' => array('type' => 'value', 'field' => 'Sample.product_id'),
		'product_name' => array('type' => 'like', 'field' => 'Sample.Product.public_name'),
		'sample_id' => array('type' => 'value'),
		'sample_name' => array('type' => 'like', 'field' => 'Sample.public_name'),
		'test_datetime' => array('type' => 'query', 'method' => 'dateFilter'),
		'dimmer_id' => array('type' => 'value'),
		'dimmer_level' => array('type' => 'value'),
	);
	
	public $_schema = array(
		'sample_id',
		'test_datetime',
		'dimmer_level',
	);
}
