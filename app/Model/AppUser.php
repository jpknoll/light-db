<?php

App::uses('User', 'Users.Model');

class AppUser extends User {
	public $useTable = 'users';
	
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'table' => 'groups',
			'foreignKey' => 'group_id',
		)
	);
	
	
/**
 * Constructor
 *
 * @param bool|string $id ID
 * @param string $table Table
 * @param string $ds Datasource
 * */
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		
		// override User Class validation	
		$validator = $this->validator();
		$validator['username']['alpha'] = array(
			'rule' => '/^[a-z0-9_]{3,}$/i',
			'message' => 'The username must be alphanumeric.',
		);
		
		// remove tos validation
		$validator['tos'] = array();
	}	
}
