<?php

App::uses('AppModel', 'Model');

class SpectralTestFactType extends AppModel{
	public $useTable = 'spectral_test_fact_types';
	
	public $displayField = 'fact_type';
	
	public $_schema = array(
		'fact_type' => array(
			'type' => 'string',
			'null' => false, 
			'default' => NULL,
		),
		'unit' => array(
			'type' => 'string',
			'null' => false,
			'default' => NULL,
		),
		'description' => array(
			'type' => 'string',
			'null' => false,
			'default' => NULL,
		),
	);
}
