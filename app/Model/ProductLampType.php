<?php

App::uses('AppModel', 'Model');

class ProductLampType extends AppModel {
	public $actsAs = array(
		'Containable',
		'Search.Searchable',
	);
	
	public $useTable = 'product_lamp_types';
	
	public $displayField = 'product_lamp_type';
	
	public $hasMany = array(		
		'Products' => array(
			'className' => 'Product',
		),
	);
}
	