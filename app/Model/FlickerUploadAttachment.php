<?php
// app/Model/FlickerUploadAttachment.php

App::uses('AppModel', 'Model');

class FlickerUploadAttachment extends AppModel {
	public $actsAs = array(
		'Uploader.Attachment' => array(
			'csv' => array(
				'dbColumn' => 'path',
				'nameCallback' => 'createName',
				'metaColumns' => array(
					'basename' => 'basename',
					'name' => 'name',
					'ext' => 'ext',
					'type' => 'type',
					'size' => 'size',
				),
			),
		),
		'Uploader.FileValidation' => array(
			'csv' => array(
				'extension' => array('value' => array('csv'), 'error' => 'upload must be a csv'),
				//'type' => array('value' => array('image/jpeg', 'image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png'), 'error' => 'upload must be an image'),
				'required' => array('value' => true, 'error' => 'upload cannot be empty'),
			),
		),
	);
	
	public $belongsTo = array(
		'FlickerUpload' => array(
			'className' => 'FlickerUpload',
		),
	);
	
	public $fields = array(
		'id',
		'flicker_upload_id',
		'path',
	);
	
	public function createName($name, $file) {
		return hash('md5', $file);
	}
}