<?php

App::uses('AppModel', 'Model');
App::uses('Sample', 'Model');

class SpectralTestFact extends AppModel {
	
	public $actsAs = array('Containable', 'Search.Searchable');	
	
	public $useTable = 'spectral_test_facts';

	public $displayField = 'test';

	public $belongsTo = array(
		'SpectralTest' => array(
			'className' => 'SpectralTest',
		),
		'SpectralTestFactType' => array(
			'className' => 'SpectralTestFactType',
		),
	);

	public $filterArgs = array(
		'sample_id' => array('type' => 'value', 'field' => 'SpectralTest.sample_id'),
		'spectral_test_orientation_type_id' => array('type' => 'value', 'field' => 'SpectralTest.spectral_test_orientation_type_id'),
		'spectral_test_fact_type_id' => array('type' => 'value'),
		'product_id' => array('type' => 'subquery', 'method' => 'filterByProductId', 'field' => 'SpectralTest.sample_id'),
	);

	public function filterByProductId($data = array()) {
		$product_id = $data['product_id'];
		
		$Sample = new Sample();
		$query = $Sample->getQuery('all', array(
			'conditions' => array('product_id' => $product_id),
			'fields' => array('id'),
			'contain' => false,
		));
		return $query;
	}
	
	public $_schema = array(
		'spectral_test_id' => array(
			'type' => 'integer',
		),
		'spectral_test_fact_type_id',
		'fact' => array(
			'type' => 'string',
			'null' => false, 
			'default' => NULL,
		)
	);
}
