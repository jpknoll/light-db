<?php

App::uses('AppModel', 'model');

class Dimmer extends AppModel {
	public $useTable = 'dimmers';
	
	public $displayField = 'name';
	
	public $_schema = array(
		'name',
		'description',
	);
}
