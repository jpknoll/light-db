<?php

App::uses('AppModel', 'Model');

class PhotometricTestFact extends AppModel {
	public $useTable = 'photometric_test_facts';

	public $displayField = 'id';

	public $belongsTo = array(
		'PhotometricTest' => array(
			'className' => 'PhotometricTest',
		),
		'PhotometricTestFactType' => array(
			'className' => 'PhotometricTestFactType',
		),
	);

	public $_schema = array(
		'photometric_test_id',
		'photometric_test_fact_type_id',
		'fact',
	);
}
