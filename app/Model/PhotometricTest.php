<?php

App::uses('AppModel', 'Model');

class PhotometricTest extends AppModel {
	public $actsAs = array('Containable', 'Search.Searchable');
	
	public $useTable = 'photometric_tests';
	
	public $displayField = 'id';
	
	public $belongsTo = array(
		'Sample' => array(
			'className' => 'Sample',
		),
	);
	
	public $hasMany = array(
		'PhotometricTestData' => array(
			'className' => 'PhotometricTestData',
			'order' => 'PhotometricTestData.phi ASC, PhotometricTestData.theta ASC',
		),
		'PhotometricTestFact' => array(
			'className' => 'PhotometricTestFact',
		),
	);
	
	public $filterArgs = array(
		'sample_id' => array('type' => 'value'),
		'sample_name' => array('type' => 'like', 'field' => 'Sample.public_name'),
		'test_datetime' => array('type' => 'query', 'method' => 'dateFilter'),
		'product_id' => array('type' => 'value', 'field' => 'Sample.product_id'),
		'product_name' => array('type' => 'like', 'field' => 'Sample.Product.public_name'),
	);
	
	public $_schema = array(
		'sample_id',
		'light_level',
		'test_datetime',
	);
}
