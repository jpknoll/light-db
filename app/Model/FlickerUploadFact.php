<?php

App::uses('AppModel', 'Model');

class FlickerUploadFact extends AppModel {
	
	public $belongsTo = array(
		'FlickerUpload' => array(
			'className' => 'FlickerUpload',
		),
		'FlickerUploadFactType' => array(
			'className' => 'FlickerUploadFactType',
		),
	);
}
