<?php

App::uses('AppModel', 'Model');

class ProductFact extends AppModel {
	public $actsAs = array('Containable', 'Search.Searchable');
	
	public $useTable = 'product_facts';
	
	public $displayField = 'id';
	
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
		),
		'ProductFactType' => array(
			'className' => 'ProductFactType',
		),
	);
	
	public $filterArgs = array(
		'product_id' => array('type' => 'value'),
		'product_fact_type_id' => array('type' => 'value'),
		'source' => array('type' => 'like'),
	);

	public $_schema = array(
		'id',	
		'product_id',	
		'product_fact_type_id',
		'product_fact_source_id',
		'value',
		'source',
	);
}
	