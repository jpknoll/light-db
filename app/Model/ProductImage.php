<?php

App::uses('AppModel', 'Model');

class ProductImage extends AppModel {
	public $actsAs = array(
		'Uploader.Attachment' => array(
			'image' => array(
				'dbColumn' => 'path',
				'nameCallback' => 'createName',
				'metaColumns' => array(
					'basename' => 'basename',
					'name' => 'name',
					'ext' => 'ext',
					'type' => 'type',
					'size' => 'size',
				),
				'transforms' => array(
					'imageSmall' => array(
						'method' => 'resize',
						'append' => '-small',
						'dbColumn' => 'path_small',
						'overwrite' => true,
						'height' => 100,
						'aspect' => true,
					),
					'imageMedium' => array(
						'method' => 'resize',
						'append' => '-medium',
						'dbColumn' => 'path_medium',
						'overwrite' => true,
						'height' => 600,
						'aspect' => true,
					),
				),
			),
		),
		'Uploader.FileValidation' => array(
			'image' => array(
				'extension' => array('value' => array('gif', 'jpg', 'png', 'jpeg'), 'error' => 'upload must be a gif/jpg/png/jpeg'),
				'type' => array('value' => array('image/jpeg', 'image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png'), 'error' => 'upload must be an image'),
				'required' => array('value' => true, 'error' => 'upload cannot be empty'),
			),
		),
	);
	
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
		),
	);
	
	public $fields = array(
		'id',
		'product_id',
		'path',
		'order',
	);
	
	public function createName($name, $file) {
		return hash('md5', $file);
	}
}