<?php

App::uses('AppModel', 'Model');
App::uses('Sample', 'Model');

class PhotometricTestData extends AppModel {
	public $useTable = 'photometric_test_data';

	public $displayField = 'id';

	public $actsAs = array(
		'Containable', 
		'Search.Searchable'
	);

	public $belongsTo = array(
		'PhotometricTest' => array(
			'className' => 'PhotometricTest'
		),
	);

	public $filterArgs = array(
		'sample_id' => array('type' => 'value', 'field' => 'PhotometricTest.sample_id'),
		'product_id' => array('type' => 'subquery', 'method' => 'filterByProductId', 'field' => 'PhotometricTest.sample_id'),
	);

	public function filterByProductId($data = array()) {
		$product_id = $data['product_id'];
		
		$Sample = new Sample();
		$query = $Sample->getQuery('all', array(
			'conditions' => array('product_id' => $product_id),
			'fields' => array('id'),
			'contain' => false,
		));
		return $query;
	}

	public $_schema = array(
		'id',
		'photometric_test_id',
		'theta',
		'phi',
		'result',
	);
}
