<?php

App::uses('AppModel', 'Model');

class Sample extends AppModel {
	public $actsAs = array('Containable', 'Search.Searchable');

	public $useTable = 'samples';

	public $displayField = 'id';

	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
		),
	);
	
	public $hasMany = array(
		'FlickerTests' => array(
			'className' => 'FlickerTest',
		),
		'PhotometricTests' => array(
			'className' => 'PhotometricTest',
		),
		'SpectralTests' => array(
			'className' => 'SpectralTest'
		),
	);

	public $filterArgs = array(
		'product_id' => array('type' => 'value'),
		'public_name' => array('type' => 'like'),
		'internal_name' => array('type' => 'like'),
	);

	public $_schema = array(
		'id',		
		'product_id',
		'public_name',
		'internal_name',
	);
}