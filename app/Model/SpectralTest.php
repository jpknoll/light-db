<?php

App::uses('AppModel', 'Model');

class SpectralTest extends AppModel {
	public $actsAs = array('Containable', 'Search.Searchable');

	public $useTable = 'spectral_tests';

	public $displayField = 'id';

	public $belongsTo = array(
		'Sample' => array(
			'className' => 'Sample',
		),
		'SpectralTestOrientationType' => array(
			'className' => 'SpectralTestOrientationType',
		),
		'SpectralTestHousingType' => array(
			'className' => 'SpectralTestHousingType',
		),
	);

	public $hasMany = array(		
		'SpectralTestData' => array(
			'className' => 'SpectralTestData',
			'order' => 'SpectralTestData.nm ASC',
		),
		'SpectralTestFacts' => array(
			'className' => 'SpectralTestFact',
		),
	);

	public $filterArgs = array(
		'sample_id' => array('type' => 'value'),
		'sample_name' => array('type' => 'like', 'field' => 'Sample.public_name'),
		'test_datetime' => array('type' => 'query', 'method' => 'dateFilter'),
		'spectral_test_orientation_type_id' => array('type' => 'value'),
		'spectral_test_housing_type_id' => array('type' => 'value'),
		'product_id' => array('type' => 'value', 'field' => 'Sample.product_id'),
		'product_name' => array('type' => 'like', 'field' => 'Sample.Product.public_name'),
	);

	public $_schema = array(
		'id',		
		'sample_id',
		'test_datetime',
		'spectral_test_orientation_type_id',
		'spectral_test_housing_type_id',
	);

	public function dateFilter($data = array()) {
		$test_datetime = $data['test_datetime'];

		$date_before = DateTime::createFromFormat('m-d-Y', $test_datetime);
		$date_after  = DateTime::createFromFormat('m-d-Y', $test_datetime)->add(new DateInterval('P1D'));

		$cond = array(
			'AND' => array(
				$this->alias . '.test_datetime >=' => $date_before->format('Y-m-d'),
				$this->alias . '.test_datetime <=' => $date_after->format('Y-m-d'),
			)
		);

		return $cond;
	}
	
}