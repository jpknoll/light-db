<?php

App::uses('AppModel', 'Model');

class FlickerTestFactType extends AppModel {
	public $useTable = 'flicker_test_fact_types';
	
	public $displayField = 'id';
}
