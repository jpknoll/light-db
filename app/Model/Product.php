<?php

App::uses('AppModel', 'Model');

class Product extends AppModel {
	public $actsAs = array(
		'Containable',
		'Search.Searchable',
	);
	
	public $useTable = 'products';
	
	public $displayField = 'id';
	
	public $belongsTo = array(
		'ProductLampType' => array(
			'className' => 'ProductLampType',
		),
	);
	
	public $hasMany = array(		
		'Samples' => array(
			'className' => 'Sample',
			'order' => 'Samples.id ASC',
		),
		'ProductFacts' => array(
			'className' => 'AllProductFact',
		),
		'ProductImages' => array(
			'className' => 'ProductImage',
			'order' => 'ProductImages.order ASC',
		),
	);

	public $filterArgs = array(
		'public_name' => array('type' => 'like'),
		'part_number' => array('type' => 'like'),
		'manufacturer' => array('type' => 'like'),
		'internal_name' => array('type' => 'like'),
	);

	public $_schema = array(
		'id',		
		'public_name',
		'part_number',
		'manufacturer',
		'internal_name',
	);
}
	