<?php

App::uses('AppModel', 'Model');

class FlickerTestFact extends AppModel {
	public $useTable = 'flicker_test_facts';

	public $displayField = 'id';

	public $belongsTo = array(
		'FlickerTest' => array(
			'className' => 'FlickerTest',
		),
		'FlickerTestFactType' => array(
			'className' => 'FlickerTestFactType',
		),
	);
}
