<?php

App::uses('AppModel', 'Model');

class SpectralTestOrientationType extends AppModel {
	public $useTable = 'spectral_test_orientation_types';
	
	var $displayField = 'orientation_type';
	
	public $_schema = array(
		'orientation_type' => array(
			'type' => 'string',
			'null' => false, 
			'default' => NULL,
		)
	);
}
