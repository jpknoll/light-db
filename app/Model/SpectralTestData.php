<?php

App::uses('AppModel', 'Model');
App::uses('Sample', 'Model');

class SpectralTestData extends AppModel {
	public $useTable = 'spectral_test_data';
	
	public $displayField = 'test';
	
	public $actsAs = array(
		'Containable', 
		'Search.Searchable'
	);
	
	public $belongsTo = array(
		'SpectralTest' => array(
			'className' => 'SpectralTest',
		),
	);

	public $filterArgs = array(
		'spectral_test_id' => array('type' => 'value'),
		'sample_id' => array('type' => 'value', 'field' => 'SpectralTest.sample_id'),
		'sample_name' => array('type' => 'like', 'field' => 'Sample.public_name'),
		'spectral_test_orientation_type_id' => array('type' => 'value', 'field' => 'SpectralTest.spectral_test_orientation_type_id'),
		'product_id' => array('type' => 'subquery', 'method' => 'filterByProductId', 'field' => 'SpectralTest.sample_id'),
	);

	public function filterByProductId($data = array()) {
		$product_id = $data['product_id'];
		
		$Sample = new Sample();
		$query = $Sample->getQuery('all', array(
			'conditions' => array('product_id' => $product_id),
			'fields' => array('id'),
			'contain' => false,
		));
		return $query;
	}

	public $_schema = array(
		'nm' => array(
			'type' => 'integer',
			'null' => false, 
			'default' => NULL,
		),
		'result' => array(
			'type' => 'double',
			'null' => false, 
			'default' => NULL,
		)
	);
}
