<?php

App::uses('AppModel', 'Model');

class PhotometricTestFactType extends AppModel {
	public $useTable = 'photometric_test_fact_types';
	
	public $displayField = 'id';
	
	public $_schema = array(
		'fact_type',
		'unit',
		'description',
	);
}
