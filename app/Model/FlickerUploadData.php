<?php

App::uses('AppModel', 'Model');

class FlickerUploadData extends AppModel {
	public $useTable = 'flicker_upload_data';
		
	public $belongsTo = array(
		'FlickerUpload' => array(
			'className' => 'FlickerUpload',
		),
	);
};
