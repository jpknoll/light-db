<?php

App::uses('AppModel', 'Model');

class ProductFactType extends AppModel {
	public $actsAs = array('Containable', 'Search.Searchable');
	
	public $useTable = 'product_fact_types';
	
	public $displayField = 'name';
	
	public $hasMany = array(
		'ProductFacts' => array(
			'className' => 'ProductFact'
		)
	);
	
	public $filterArgs = array(
	);

	public $_schema = array(
		'id',	
		'name',
		'units',
		'description',	
	);
}
	