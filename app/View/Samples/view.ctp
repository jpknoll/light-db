<!-- app/View/Samples/index.ctp -->

<?php 
	$this->Html->addCrumb('Samples', '/samples');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Sample</h2>
<small class="edit-link"><?php echo $this->Html->link('edit', array('controller' => 'samples', 'action' => 'edit', $sample['Sample']['id'])); ?></small><br />
<div>
	<table>
		<tr>
			<td><strong>Product Id:</strong></td>
			<td><?php echo $sample['Product']['id']; ?></td>
		</tr>
		<tr>
			<td><strong>Product Name:</strong></td>
			<td><?php echo $sample['Product']['public_name']; ?></td>
		</tr>
		<tr>
			<td style="width:100px;"><strong>Sample Id:</strong></td>
			<td><?php echo $sample['Sample']['id']; ?></td>
		</tr>
		<tr>
			<td><strong>Sample Name:</strong></td>
			<td><?php echo $sample['Sample']['public_name']; ?></td>
		</tr>
	</table> 
	<br />
	<br />
</div>

<div id="divider" style="width=100%"></div>
<div id="chromaticity-chart" style="width:400px; height:400px; float:left;"></div>
<?php echo $this->element('Charts/chromatic-chart', array(
	'action' => 'summary', 
	'filter' => array(
		'sample_id' => $sample['Sample']['id'],
	),
	'series_filter' => array('Average'),
	'id' => 'chromaticity-chart'
)); ?>

<div id="chromaticity-chart-zoomed" style="width:400px; height:400px; float:left;"></div>
<?php echo $this->element('Charts/chromatic-chart-zoomed', array(
	'action' => 'summary',
	'filter' => array(
		'sample_id' => $sample['Sample']['id'],
	),
	'series_filter' => array('Average'),
	'id' => 'chromaticity-chart-zoomed'
)); ?>

<div id="photometric-chart-max-slice" style="width:400px; height:400px; float:left;"></div>
<?php echo $this->element('Charts/photometric-chart-max-slice', array(
	'filter' => array(
		'sample_id' => $sample['Sample']['id'],
	),
	'id' => 'photometric-chart-max-slice'
)); ?>

<div id="spectral-chart" style="width:100%; height:400px; clear:both;"></div>
<?php echo $this->element('Charts/spectral-chart', array(
	'action' => 'summary',
	'filter' => array(
		'sample_id' => $sample['Sample']['id'],
	),
	'id' => 'spectral-chart'
)); ?>

<h3>Photometric Test</h3>
<table id="spectral-fact-summary">
	<tr>
		<th></th>
		<th>Orientation</th>
		<th>Power (W)</th>
		<th>PF</th>
		<th>Sphere Output (lm)</th>
		<th>CCT (K)</th>
		<th>Chromaticity (u', v')</th>
		<th>CRI</th>
		<th>R9</th>
		<th>Efficacy (lm/W)</th>
	</tr>
	<tr>
		<td>Average</td>
		<td>Base-Down</td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Normal Power Factor']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Output'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Output']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Temperature'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Temperature']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['u prime']) AND isset($spectral_facts_summary_pivot['Base-Down']['v prime'])){ echo sprintf('%1.4f, %1.4f', $spectral_facts_summary_pivot['Base-Down']['u prime'], $spectral_facts_summary_pivot['Base-Down']['v prime']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Ra'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Ra']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['R9'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['R9']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Efficacy'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Efficacy']); } ?></td>
	</tr>
	<tr>
		<td>Average</td>
		<td>Base-Up</td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Normal Power Factor']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Output'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Output']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Temperature'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Temperature']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['u prime']) AND isset($spectral_facts_summary_pivot['Base-Up']['v prime'])){ echo sprintf('%1.4f, %1.4f', $spectral_facts_summary_pivot['Base-Up']['u prime'], $spectral_facts_summary_pivot['Base-Up']['v prime']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Ra'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Ra']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['R9'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['R9']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Efficacy'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Efficacy']); } ?></td>
	</tr>
	<tr>
		<td>Average</td>
		<td>Horizontal</td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Normal Power Factor']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Output'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Output']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Temperature'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Temperature']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['u prime']) AND isset($spectral_facts_summary_pivot['Horizontal']['v prime'])){ echo sprintf('%1.4f, %1.4f', $spectral_facts_summary_pivot['Horizontal']['u prime'], $spectral_facts_summary_pivot['Horizontal']['v prime']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Ra'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Ra']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['R9'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['R9']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Efficacy'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Efficacy']); } ?></td>
	</tr>
	<tr>
		<td>Average</td>
		<td>All Orientations</td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Normal Power Factor']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Output'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Output']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Temperature'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Temperature']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['u prime']) AND isset($spectral_facts_summary_pivot['All Orientations']['v prime'])){ echo sprintf('%1.4f, %1.4f', $spectral_facts_summary_pivot['All Orientations']['u prime'], $spectral_facts_summary_pivot['All Orientations']['v prime']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Ra'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Ra']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['R9'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['R9']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Efficacy'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Efficacy']); } ?></td>
	</tr>
</table>
<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'spectral_tests', 'action' => 'find', 'sample_id' => $sample['Sample']['id'] )); ?></small><br />

<h3>Electrical Test</h3>
<table id="electrical-summary-chart">
	<tr>
		<th></th>
		<th>Orientation</th>
		<th>Power (W)</th>
		<th>PF</th>
		<th>Voltage THD%</th>
		<th>Current THD%</th>
	</tr>
	<tr>
		<td>Average</td>
		<td>Base-Down</td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['Normal Power Factor']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['UTHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['UTHD%']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Down']['ITHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Down']['ITHD%']); } ?></td>
	</tr>
	<tr>
		<td>Average</td>
		<td>Base-Up</td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['Normal Power Factor']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['UTHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['UTHD%']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Base-Up']['ITHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Base-Up']['ITHD%']); } ?></td>
	</tr>
	<tr>
		<td>Average</td>
		<td>Horizontal</td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['Normal Power Factor']); } ?></td>		
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['UTHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['UTHD%']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['Horizontal']['ITHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['Horizontal']['ITHD%']); } ?></td>
	</tr>
	<tr>
		<td>Average</td>
		<td>All Orientations</td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Normal Power'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Normal Power']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['Normal Power Factor'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['Normal Power Factor']); } ?></td>		
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['UTHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['UTHD%']); } ?></td>
		<td><?php if(isset($spectral_facts_summary_pivot['All Orientations']['ITHD%'])){ echo sprintf('%1.2f', $spectral_facts_summary_pivot['All Orientations']['ITHD%']); } ?></td>
	</tr>
</table>
<span>* <small>All tests conducted at 120V</small></span>
<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'spectral_tests', 'action' => 'find', 'sample_id' => $sample['Sample']['id'] )); ?></small>

<h3>Distribution Test - Base Up</h3>
<table id="gonio-summary">
	<tr>
		<th>Power (W)</th>
		<th>Gonio Output (lm)</th>
		<th>Efficacy (lm/W)</th>
	</tr>
	<tr>
		<td><?php if(isset($gonio_summary['Wattage'])) { echo sprintf('%1.2f', $gonio_summary['Wattage']); } else { echo "N/A"; }?></td>
		<td><?php if(isset($gonio_summary['Output'])) { echo sprintf('%1.2f', $gonio_summary['Output']); } else { echo "N/A"; }?></td>
		<td><?php if(isset($gonio_summary['Efficacy'])) { echo sprintf('%1.2f', $gonio_summary['Efficacy']); } else { echo "N/A"; }?></td>
	</tr>
</table>
<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'photometric_tests', 'action' => 'find', 'product_id' => $sample['Sample']['id'] )); ?></small>

<h3>Flicker Tests</h3>
<div id="flicker-data-summary-fundamental-frequency">	
	<?php if(isset($flicker_data_summary_frequency)){ echo sprintf('Fundamantal Frequency (Hz): %s', $flicker_data_summary_frequency); } ?> 
	<br /><br />
</div>

<h4>Percent Flicker, Filter cutoff at 200Hz</h4>
<table id="flicker-data-summary-percent-filtered">
	<tr>
		<th>% Operable Power<sup>*</sup></th>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
		<th><?php echo $dimmer; ?></th>
		<?php endforeach; ?>
	</tr>
	<?php foreach($flicker_data_summary_levels as $level): ?>
	<tr>
		<td><?php echo $level ?></td>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
		<td><?php if(isset($flicker_data_summary[$dimmer][$level]['200']['percent'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['200']['percent']); } else { echo 'N/A'; } ?></td>
		<?php endforeach; ?>		
	</tr>
	<?php endforeach; ?>
</table>

<h4>Flicker Index, Filter cutoff at 200Hz</h4>
<table id="flicker-data-summary-index-filtered">
	<tr>
		<th>% Operable Power<sup>*</sup></th>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
		<th><?php echo $dimmer; ?></th>
		<?php endforeach; ?>
	</tr>
	<?php foreach($flicker_data_summary_levels as $level): ?>
	<tr>
		<td><?php echo $level ?></td>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
		<td><?php if(isset($flicker_data_summary[$dimmer][$level]['200']['index'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['200']['index']); } else { echo 'N/A'; } ?></td>
		<?php endforeach; ?>		
	</tr>
	<?php endforeach; ?>
</table>

<h4>Percent Flicker, Unfiltered</h4>
<table id="flicker-data-summary-percent-unfiltered">
	<tr>
		<th>% Operable Power<sup>*</sup></th>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
		<th><?php echo $dimmer; ?></th>
		<?php endforeach; ?>
	</tr>
	<?php foreach($flicker_data_summary_levels as $level): ?>
	<tr>
		<td><?php echo $level ?></td>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
		<td><?php if(isset($flicker_data_summary[$dimmer][$level]['10000']['percent'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['10000']['percent']); } else { echo 'N/A'; } ?></td>
		<?php endforeach; ?>		
	</tr>
	<?php endforeach; ?>
</table>

<h4>Flicker Index, Unfiltered</h4>
<table id="flicker-data-summary-index-unfiltered">
	<tr>
		<th>% Operable Power<sup>*</sup></th>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
		<th><?php echo $dimmer; ?></th>
		<?php endforeach; ?>
	</tr>
	<?php foreach($flicker_data_summary_levels as $level): ?>
	<tr>
		<td><?php echo $level ?></td>
		<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
		<td><?php if(isset($flicker_data_summary[$dimmer][$level]['10000']['index'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['10000']['index']); } else { echo 'N/A'; } ?></td>
		<?php endforeach; ?>		
	</tr>
	<?php endforeach; ?>
</table>
<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'flicker_tests', 'action' => 'find', 'sample_id' => $sample['Sample']['id'] )); ?></small>
<span>* <small>% Operable Power is defined by the luminous output range of each lamp, with 100% power equaling the power of each lamp at its maximum luminous output, and Min power equaling the power of each lamp while at minimum luminous output. This power range is divided into quartiles to arrive at values for 75%, 50% and 25% power levels.</small></span>
