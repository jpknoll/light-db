<!-- app/View/Samples/index.ctp -->

<?php 
	$this->Html->addCrumb('Samples', '/samples');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Samples</h2>
<?php echo $this->Html->link('Search/Filter', array('controller' => 'samples', 'action' => 'find')); ?>

<table>
    <tr>
		<th>Name</th>
    </tr>

    <?php foreach ($samples as $sample): ?>
    <tr id="sample-<?php echo $sample['Sample']['id']; ?>">
		<td><?php echo $this->Html->link($sample['Sample']['public_name'], array('controller' => 'samples', 'action' => 'view', $sample['Sample']['id'])); ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($product); ?>
</table>

<?php 
	echo $this->element('paginator', array('list' => $samples)); 
?>