<?php
if (!isset($auth['User'])) {
	echo $this->Html->link('Log in', array('controller' => 'users', 'action' => 'login'));
	echo " - ";
	echo $this->Html->link('Sign up', array('controller' => 'users', 'action' => 'add'));
}
else {
	echo $this->Html->link($auth['User']['username'], array('controller' => 'users', 'action' => 'edit', $auth['User']['id']));
	echo " - ";
	
	if ($this->Permissions->check('Admin')) {
		echo $this->Html->link('Admin', array('controller' => 'admin', 'action' => 'index'));
		echo " - ";
	}
	echo $this->Html->link('Log out', array('controller' => 'users', 'action' => 'logout'));
}
