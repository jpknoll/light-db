<?php 
if(!empty($list)){ 
    echo $this->Paginator->first(__('First', true), array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->prev('� Back', null, null, array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->numbers();
    echo " ";
    echo $this->Paginator->next('Next �', null, null, array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->last(__('Last', true), array('class' => 'disabled'));
} ?>

<?php 
	$this->Paginator->options(array('url' => $this->passedArgs));
?>