<?php 
	# set defaults
	if(!isset($id)) { $id = 'chromatic-chart-zoomed'; }
?>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {

	var chartRedrawEvent = function() {}; 	 // placeholder
	
	var chart_el = $('#<?php echo $id ?>').highcharts({
		chart: {
			renderTo: 'container',
			backgroundColor: 'transparent',
			events: {
				redraw: function() { chartRedrawEvent(); },
			},
		},
		title: 'Chromaticity Data',
		xAxis: {
			min: 0.4,
			max: 0.5,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
		},
		yAxis: {
			min: 0.35,
			max: 0.45,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
		}
	});
	
	$.ajax({
		url: '/js/data/4step.json', 
		dataType: 'json',
		error: function(data, textStatus, errorThrown) {
		},
		success: function(data) {
			var chart = chart_el.highcharts();
			
			var series = {
				id: '4 Step',
				name: '4 Step',
				type: 'scatter',
				color: 'blue',
				data: [],
				lineWidth: 2,
				marker: {
					fillColor: 'blue',
					symbol: 'diamond'
				}
			}
			
			//add centers
			for(key in data["centers"].data) {
				series.data.push(
					data["centers"].data[key],
					null
				);
			}
			
	    	//add ellipses
	    	for (key in data["ellipses"]) {
	    		series.data.push(
	    			data["ellipses"][key]["data"][0],
	    			data["ellipses"][key]["data"][1],
	    			data["ellipses"][key]["data"][2],
	    			data["ellipses"][key]["data"][3],
	    			data["ellipses"][key]["data"][0],
	    			null
	    		);
	    	}
	    	
	    	chart.addSeries(series);
		}
	});
	
	$.ajax({
		url: '/js/data/7step.json',
		dataType: 'json',
		error: function(data, textStatus, errorThrown) {
		},
		success: function(data) {
			var chart = chart_el.highcharts();
			
			var series = {
				id: '7 Step',
				name: '7 Step',
				type: 'scatter',
				color: 'green',
				data: [],
				marker: {
					fillColor: 'green',
					symbol: 'diamond'
				},
				lineWidth: 2
			}
			
			//add centers
			for(key in data["centers"].data) {
				series.data.push(
					data["centers"].data[key],
					null
				);
			}
			
	    	//add ellipses
	    	for (key in data["ellipses"]) {
	    		series.data.push(
	    			data["ellipses"][key]["data"][0],
	    			data["ellipses"][key]["data"][1],
	    			data["ellipses"][key]["data"][2],
	    			data["ellipses"][key]["data"][3],
	    			data["ellipses"][key]["data"][0],
	    			null
	    		);
	    	}
	    	
	    	chart.addSeries(series);
		}
	});
	
	$.ajax({
		url: '/js/data/locus.json',
		dataType: 'json',
		error: function(data, textStatus, errorThrown) {
		},
		success: function(data) {
			
			var seriesData = data.locus.map(function(value) {
				return {
					name: value.temperature,
					x: value.point[0],
					y: value.point[1]
				};
			});
			
			seriesData.sort(function(a, b) {
				return ((a.x < b.x) ? -1 : ((a.x > b.x) ? 1 : 0));
			});
			
			var series = {
				id: 'Planckian Locus',
				name: 'Planckian Locus',
				data: seriesData,
				type: 'line',
				color: 'black',
				marker: {enabled: false},
				enableMouseTracking: false
			};
			
			chart_el.highcharts().addSeries(series);
		}
	});
	
	// add background
	var chart = chart_el.highcharts();
	var x = chart.plotLeft;
	var y = chart.plotTop;
	var w = chart.plotSizeX;
	var h = chart.plotSizeY;
	
	var background_image = chart.renderer.image('/img/CIE1931xy_blank_zoom.png', x, y, w, h);
	background_image.add();
	
	// listen to add event to fix xy/wh
	chartRedrawEvent = function() {
		background_image.element.setAttributeNS(null, "x", chart.plotLeft);
		background_image.element.setAttributeNS(null, "y", chart.plotTop);
		background_image.element.setAttributeNS(null, "width", chart.plotSizeX);
		background_image.element.setAttributeNS(null, "height", chart.plotSizeY);
	};
	chartRedrawEvent();
	
});
</script>