<script type="text/javascript" language="JavaScript">

	$.ajax({
		url: '<?php echo $this->Html->url(array_merge(array('controller' => 'PhotometricTestData', 'action' => 'maxSlices'), $filter)); ?>/.json',
		success: function(data) {
		
			data.maxSlices.maxVerticalSlice.sort(function(a, b) {
				var phi1 = parseInt(a.PhotometricTestData.phi);
				var phi2 = parseInt(b.PhotometricTestData.phi);
				return ((phi1 < phi2) ? -1 : ((phi1 > phi2) ? 1 : 0));
			});
			
			values = data.maxSlices.maxVerticalSlice.map(function(point, i) {
				return [parseInt(point.PhotometricTestData.phi), parseFloat(point.PhotometricTestData.result)];
			});
				
			theta = data.maxSlices.maxVerticalSlice[0].PhotometricTestData.theta;
				
			photometricDataVertical = {
				name: 'Vertical at ' + theta + '',
				data: values,
				type: 'line',
				marker: {enabled: false}
			};
			
			data.maxSlices.maxHorizontalSlice.sort(function(a, b) {
				var theta1 = parseInt(a.PhotometricTestData.theta);
				var theta2 = parseInt(b.PhotometricTestData.theta);
				return ((theta1 < theta2) ? -1 : ((theta1 > theta2) ? 1 : 0));
			});
			
			values = data.maxSlices.maxHorizontalSlice.map(function(point, i) {
				return [parseInt(point.PhotometricTestData.theta), parseFloat(point.PhotometricTestData.result)];
			});
			
			phi = data.maxSlices.maxHorizontalSlice[0].PhotometricTestData.phi;
				
			photometricDataHorizontal = {
				name: 'Horizontal at ' + phi + '',
				data: values,
				type: 'line',
				marker: {enabled: false}
			};
			
			photometricChart = $('#<?php echo $id; ?>').highcharts({
				chart: {
					renderTo: 'container',
					polar: true
				},
				title: 'Photometric Data',
				tooltip: {
					valueDecimals: 2,
					valueSuffix: ' cd'
				},
				series: [
					photometricDataVertical,
					photometricDataHorizontal
				],
				xAxis: {
					min: 0,
					max: 360,
					tickInterval: 45,
				},
				yAxis: {
					min: 0
				},
			});	
		},
		cache: false
	});
</script>