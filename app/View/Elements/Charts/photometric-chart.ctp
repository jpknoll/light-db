<?php 
	# set defaults
	if(!isset($id_phi)) { $id_phi = 'photometric-chart-phi'; }								#div id
	if(!isset($id_phi_select)) { $id_phi_select = 'photometric-chart-phi-select'; }			#select id
	if(!isset($id_phi_slider)) { $id_phi_slider = 'photometric-chart-phi-slider'; }			#slider id
	if(!isset($id_phi_data)) { $id_phi_data = 'photometric_chart_phi_data'; } 				#javascript variable name, global scope
	
	if(!isset($id_theta)) { $id_theta = 'photometric-chart-theta'; } 						#div id
	if(!isset($id_theta_select)) { $id_theta_select = 'photometric-chart-theta-select'; } 	#select id
	if(!isset($id_theta_slider)) { $id_theta_slider = 'photometric-chart-theta-slider'; } 	#slider id
	if(!isset($id_theta_data)) { $id_theta_data = 'photometric_chart_theta_data'; } 		#javascript variable name, global scope
	
	if(!isset($action)) { $action = 'find'; }
	if(!isset($filter)) { $filter = array(); }
	if(!isset($series_filter)) { $series_filter = array(); }
?>

<script type="text/javascript" language="JavaScript">

	// setup series of series
	var <?php echo $id_phi_data; ?> = {};
	var <?php echo $id_theta_data; ?> = {};

	$.ajax({
		url: '<?php echo $this->Html->url(array_merge(array('controller' => 'PhotometricTestData', 'action' => $action), $filter)); ?>/.json',
		success: function(data) {
			// for each data series
			for(i in data.data) {
				var lamp = data.data[i].Sample.public_name;
				
				// get list of distinct angles
				var phi = [];
				var theta = [];
				for(j in data.data[i].PhotometricTestData) {
					phi[data.data[i].PhotometricTestData[j].phi] = 1;
					theta[data.data[i].PhotometricTestData[j].theta] = 1;
				}
				
				// setup empty series sets
				<?php echo $id_phi_data; ?>[lamp] = [];
				for(j in phi) {
					<?php echo $id_phi_data; ?>[lamp][j] = {
						name: lamp,
						angle: j,
						data: [],
						type: 'line',
						marker: { enabled: false },
						animation: false,
					};
				}
				<?php echo $id_theta_data; ?>[lamp] = [];
				for(j in theta) {
					<?php echo $id_theta_data; ?>[lamp][j] = {
						name: lamp, 
						angle: j,
						data: [],
						type: 'line',
						marker: { enabled: false },
						animation: false,
					};
				}
				
				// parse all values
				for(j in data.data[i].PhotometricTestData) {
					<?php echo $id_phi_data; ?>[lamp][data.data[i].PhotometricTestData[j].phi].data.push(
						[parseInt(data.data[i].PhotometricTestData[j].theta), parseFloat(data.data[i].PhotometricTestData[j].result)]
					);
					<?php echo $id_theta_data; ?>[lamp][data.data[i].PhotometricTestData[j].theta].data.push(
						[parseInt(data.data[i].PhotometricTestData[j].phi), parseFloat(data.data[i].PhotometricTestData[j].result)]
					);
				}
				
				// sort values
				// fix hash keys
				var tempArray = [];
				for(j in <?php echo $id_phi_data; ?>[lamp]) {
					<?php echo $id_phi_data; ?>[lamp][j].data.sort(function(a,b) {
						return ((a[0] < b[0]) ? -1 : ((a[0] > b[0]) ? 1 : 0));
					});
					tempArray.push(<?php echo $id_phi_data; ?>[lamp][j]);
				}
				<?php echo $id_phi_data; ?>[lamp] = tempArray;
				delete tempArray;
				
				// sort values
				// fix hash keys
				var tempArray = [];
				for(j in <?php echo $id_theta_data; ?>[lamp]) {
					<?php echo $id_theta_data; ?>[lamp][j].data.sort(function(a,b) {
						return ((a[0] < b[0]) ? -1 : ((a[0] > b[0]) ? 1 : 0));
					});
					tempArray.push(<?php echo $id_theta_data; ?>[lamp][j]);
				}
				<?php echo $id_theta_data; ?>[lamp] = tempArray;
				delete tempArray;
			}
			
			
			// setup select options
			// get list of distinct angles
			var temp = [];
			for(var lamp in <?php echo $id_phi_data; ?>) {
				for(var series in <?php echo $id_phi_data; ?>[lamp]) {
					temp[ <?php echo $id_phi_data; ?>[lamp][series].angle ] = 1;
				}
			}
			// sort distinct list
			var phi = [];
			for(var key in temp) {
				phi.push(Number(key));
			}
			phi.sort(function(a,b){ return a-b; });
			// append options
			for(var i in phi) {
				$('#<?php echo $id_phi_select ?>')
					.append($("<option></option>")
						.attr("value", phi[i])
						.text(phi[i]));
			}
			
			var temp = [];
			for(var lamp in <?php echo $id_theta_data; ?>) {
				for(var series in <?php echo $id_theta_data; ?>[lamp]) {
					temp[ <?php echo $id_theta_data; ?>[lamp][series].angle ] = 1;
				}
			}
			// sort distinct list
			var theta = [];
			for(var key in temp) {
				theta.push(Number(key));
			}
			theta.sort(function(a,b){ return a-b; });
			for(var i in theta) {
				$('#<?php echo $id_theta_select ?>')
					.append($("<option></option>")
						.attr("value", theta[i])
						.text(theta[i]));
			}
			
			// setup slider options
			$('#<?php echo $id_phi_slider ?>').slider({
				min: 0,
				max: phi.length - 1,
				slide: function( event, ui ) {
					$('#<?php echo $id_phi_select ?>').val(phi[ui.value]).change();
				},
			});
			$('#<?php echo $id_phi_select ?>').change(function() {
				$('#<?php echo $id_phi_slider ?>').slider('value', this.selectedIndex);
			});
	
			$('#<?php echo $id_theta_slider ?>').slider({
				min: 0,
				max: theta.length - 1,
				slide: function( event, ui ) {
					$('#<?php echo $id_theta_select ?>').val(theta[ui.value]).change();
				},
			});
			$('#<?php echo $id_theta_select ?>').change(function() {
				$('#<?php echo $id_theta_slider ?>').slider('value', this.selectedIndex);
			});
			
			// trigger slider value change
			$('#<?php echo $id_phi_select ?>').change();
			$('#<?php echo $id_theta_select ?>').change();
			
		},
		cache: false
	});
	
	// setup select change functions to update chart
	$('#<?php echo $id_phi_select ?>').change(function() {
		//get angle
		var angle = $('#<?php echo $id_phi_select ?>').val();
		if (angle == '') { return; }
		
		//create new series array
		var series = [];
		for(var lamp in <?php echo $id_phi_data; ?>) {
			for(var i = 0; i < <?php echo $id_phi_data; ?>[lamp].length; i++) {
				if (<?php echo $id_phi_data; ?>[lamp][i].angle == angle) {
					series.push(<?php echo $id_phi_data; ?>[lamp][i]);
				}
			}
		}
		
		//recreate chart
		$('#<?php echo $id_phi; ?>').highcharts({
			chart: {
				renderTo: 'container',
				polar: true
			},
			title: 'Photometric Data',
			tooltip: {
				valueDecimals: 2,
				valueSuffix: ' cd'
			},
			series: series,
			xAxis: {
				min: 0,
				max: 360,
				tickInterval: 45,
			},
			yAxis: {
				min: 0
			},
		});	
	});
	
	$('#<?php echo $id_theta_select ?>').change(function() {
		//get angle
		var angle = $('#<?php echo $id_theta_select ?>').val();
		if (angle == '') { return; }
		
		//create new series array
		var series = [];
		for(var lamp in <?php echo $id_theta_data; ?>) {
			for(var i = 0; i < <?php echo $id_theta_data; ?>[lamp].length; i++) {
				if (<?php echo $id_theta_data; ?>[lamp][i].angle == angle) {
					series.push(<?php echo $id_theta_data; ?>[lamp][i]);
				}
			}
		}
		
		$('#<?php echo $id_theta; ?>').highcharts({
			chart: {
				renderTo: 'container',
				polar: true
			},
			title: 'Photometric Data',
			tooltip: {
				valueDecimals: 2,
				valueSuffix: ' cd'
			},
			series: series,
			xAxis: {
				min: 0,
				max: 360,
				tickInterval: 45,
			},
			yAxis: {
				min: 0
			},
		});	
	});

	

</script>