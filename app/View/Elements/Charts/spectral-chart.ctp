<script type="text/javascript" language="JavaScript">
	spectralChart = $('#<?php echo $id; ?>').highcharts({
		chart: {
			renderTo: 'container',
		},
		title: 'Spectral Data',
		tooltip: {
			shared: true,
			valueDecimals: 2,
			valueSuffix: ' uW/nm'
		},
		xAxis: {
			type: 'linear',
			title: {text: 'nm'}
		},
		yAxis: {
			type: 'linear',
			title: {text: 'uW/nm'},
			min: 0
		},
	});
</script>
