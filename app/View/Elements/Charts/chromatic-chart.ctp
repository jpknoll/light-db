<?php 
	# set defaults
	if(!isset($id)) { $id = 'chromatic-chart'; }
?>

<script type="text/javascript" language="JavaScript">
	
$(document).ready(function() {
	
	var chartRedrawEvent = function() {}; 	 // placeholder
    	        
    var chart_el = $('#<?php echo $id; ?>').highcharts({
    	chart: {
			renderTo: 'container',
			backgroundColor: 'transparent',
			events: {
				redraw: function() { chartRedrawEvent(); },
			},
		},
		title: 'Chromaticity Data',
		tooltip: {
			valueDecimals: 4,
		},
		xAxis: {
			min: 0,
			max: 0.8,
			tickInterval: 0.1,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
		},
		yAxis: {
			min: 0,
			max: 0.9,
			tickInterval: 0.1,
			gridLineWidth: 0,
			minorGridLineWidth: 0,
		}
    });
    
    $.ajax({
    	url: '/js/data/locus.json',
    	error: function(data, textStatus, errorThrown) {
    	},
    	success: function(data) {
    		
			var seriesData = data.locus.map(function(value) {
				return {
					name: value.temperature,
					x: value.point[0],
					y: value.point[1]
				};
			});
			
			seriesData.sort(function(a, b) {
				return ((a.x < b.x) ? -1 : ((a.x > b.x) ? 1 : 0));
			});
			
			var series = {
				name: 'Planckian Locus',
				data: seriesData,
				type: 'line',
				color: 'black',
				marker: {enabled: false},
				enableMouseTracking: false
			};
			
			chart_el.highcharts().addSeries(series);
    	}
	});
	
	// add background
	var chart = chart_el.highcharts();
	var x = chart.plotLeft;
	var y = chart.plotTop;
	var w = chart.plotSizeX;
	var h = chart.plotSizeY;
	
	var background_image = chart.renderer.image('/img/CIE1931xy_blank.png', x, y, w, h);
	background_image.add();
	
	// listen to add event to fix xy/wh
	chartRedrawEvent = function() {
		background_image.element.setAttributeNS(null, "x", chart.plotLeft);
		background_image.element.setAttributeNS(null, "y", chart.plotTop);
		background_image.element.setAttributeNS(null, "width", chart.plotSizeX);
		background_image.element.setAttributeNS(null, "height", chart.plotSizeY);
	};
	chartRedrawEvent();
	
});
</script>