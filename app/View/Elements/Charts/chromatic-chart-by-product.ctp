<?php 
	# set defaults
	if(!isset($id)) { $id = 'chromatic-chart'; }
	if(!isset($action)) { $action = 'group_by_product'; }
	if(!isset($filter)) { $filter = array(); }
?>

<script type="text/javascript" language="JavaScript">
	$.ajax({
		url: '<?php echo $this->Html->url(array_merge(array('controller' => 'spectral_test_facts', 'action' => $action), $filter)); ?>/.json',
		success: function(data) {
			var series = [];
			for(product_id in data.data) {
				
				var product_name = data.data[product_id].Product.public_name;
				var facts = data.data[product_id].Facts;

				// cie coordinates
				series.push({
					name: product_name,
					data: [[parseFloat(facts['CIE x'].Avg), parseFloat(facts['CIE y'].Avg)]],
					type: 'scatter',
					marker: {
						fillColor: 'black'
					}
				});
			}
	        	        
	        var chromaticityChart = $('#<?php echo $id; ?>').highcharts({
	        	chart: {
					//renderTo: 'container',
					backgroundColor: 'transparent',
					events: {
						load: function(event) {
							x = this.plotLeft;
							y = this.plotTop;
							w = this.plotSizeX;
							h = this.plotSizeY;
							this.renderer.image('/img/CIE1931xy_blank.png', x, y, w, h).add();
						}
					}
				},
				title: 'Chromaticity Data',
				tooltip: {
					valueDecimals: 4
					//valueSuffix: ' ' + units
				},
				series: series,
				xAxis: {
					min: 0,
					max: 0.8,
					tickInterval: 0.1,
					gridLineWidth: 0,
					minorGridLineWidth: 0,
				},
				yAxis: {
					min: 0,
					max: 0.9,
					tickInterval: 0.1,
					gridLineWidth: 0,
					minorGridLineWidth: 0,
				}
	        });
	        
	        $.ajax({
	        	url: '/js/data/locus.json',
	        	error: function(data, textStatus, errorThrown) {
	        	},
	        	success: function(data) {
	        		
        			var seriesData = data.locus.map(function(value) {
        				return {
        					name: value.temperature,
        					x: value.point[0],
        					y: value.point[1]
        				};
        			});
        			
        			seriesData.sort(function(a, b) {
						return ((a.x < b.x) ? -1 : ((a.x > b.x) ? 1 : 0));
					});
        			
        			var series = {
        				name: 'Planckian Locus',
						data: seriesData,
						type: 'line',
						color: 'black',
						marker: {enabled: false},
						enableMouseTracking: false
        			};
        			
					chromaticityChart.highcharts().addSeries(series);
	        	}
	        });
		},
		cache: false
	});
</script>