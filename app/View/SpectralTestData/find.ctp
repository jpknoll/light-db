<!-- app/View/SpectralTestData/summary.ctp -->

<?php 
	$this->Html->addCrumb('Spectral Tests', '/spectral_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<script type="text/javascript" language="JavaScript">

//globals
var spectralChart;
var spectralData;


function getSpectralData() {
	$.ajax({
		url: '/SpectralTestData/view/product_id:<?php echo $product['Product']['id'] ?>/.json',
		success: function(data)  {
		
			var spectralDataDict = {}
			$.each(data.summary, function(i, point) {
				if (!spectralDataDict[point.SpectralTestData.spectral_test_id]) {
					spectralDataDict[point.SpectralTestData.spectral_test_id] = [];
				}
				spectralDataDict[point.SpectralTestData.spectral_test_id].push(
					[parseInt(point.SpectralTestData.nm), parseFloat(point.SpectralTest.result)]
				);
			});
			
			spectralData = [];
			for(key in spectralDataDict) {
				spectralData.push({
					name: key,
					data: spectralDataDict[key]	
				});
			}
						
			spectralChart = $('#spectral-chart').highcharts({
				chart: {
					renderTo: 'container',
					type: 'line',
				},
				xAxis: {
					type: 'linear',
					title: {text:'nm'}
				},
				yAxis: {
					title: {text:'unit'}
				},
				title: 'Spectral Data',
				series: spectralData
			});			
		},
		cache: false
	});
}

$(document).ready(function() {
	getSpectralData();
});

</script>


<h1>Spectral Test</h1>
<div id="spectral-chart" style="width:100%; height:400px;"></div>
<table>
    <tr>
        <th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
        <th>Orientation</th>
        
        <?php foreach ($facts as $fact): ?>
    	<th><?php echo $fact['SpectralTestFactType']['fact_type'];  ?></th>
    	<?php endforeach; ?>
    	<?php unset($fact); ?>
    	
    	<?php foreach ($test['SpectralTestData'] as $data): ?>
    	<th><?php echo $data['nm']; ?></th>
    	<?php endforeach; ?>
    	<?php unset($data); ?>
        
    </tr>

    <tr>
        <td><?php echo $test['SpectralTest']['id']; ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['SpectralTest']['test_datetime']; ?></td>
        <td><?php echo $test['SpectralTestOrientationType']['orientation_type']; ?></td>
        
        <?php foreach ($facts as $fact): ?>
    	<td><?php echo $fact['SpectralTestFact']['fact'];  ?></td>
    	<?php endforeach; ?>
    	<?php unset($fact); ?>
    	
    	<?php foreach ($test['SpectralTestData'] as $data): ?>
    	<td><?php echo $data['result']; ?></td>
    	<?php endforeach; ?>
    	<?php unset($data); ?>
    	
    </tr>

</table>