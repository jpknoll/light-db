<!-- app/View/Permissions/index.ctp -->

<table>
<thead>
	<tr>
		<th>ID</th>
		<th>Alias</th>
		<th>Model</th>
		<th>Key</th>
	</tr>
</thead>
<tbody>
<?php foreach($data as $aco): ?>
	<tr>
		<td><?php echo $aco['Aco']['id']; ?></td>
		<td><?php echo $this->Html->link($aco['Aco']['alias'], array('controller' => 'permissions', 'action' => 'edit', $aco['Aco']['id'])) ?></td>
		<td><?php echo $aco['Aco']['model']; ?></td>
		<td><?php echo $aco['Aco']['foreign_key']; ?></td>
		<td><?php echo $this->Html->link('Delete', array('controller' => 'permissions', 'action' => 'delete', $aco['Aco']['id'])) ?></td>
	</tr>
<?php endforeach; ?>
<?php unset($group); ?>
</tbody>
</table>

<?php echo $this->Html->link('New Permission Control', array('action' => 'add')); ?>
