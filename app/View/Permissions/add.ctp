<?php echo $this->Form->create('Aco'); ?>
<fieldset>
	<?php echo $this->Form->input('Aco.alias', array('label' => 'Name')); ?>
	<?php echo $this->Form->input('Aco.parent_id', array('label' => 'Parent', 'options' => $parents, 'empty' => '')); ?>
	<?php echo $this->Form->input('Aco.model', array('label' => 'Model')); ?>
	<?php echo $this->Form->input('Aco.foreign_key', array('label' => 'Key', 'type' => 'text')); ?>
	
	<?php echo $this->Form->submit(__('Submit', true), array('name' => 'ok', 'div' => false)); ?>
	<?php echo $this->Form->submit(__('Cancel', true), array('name' => 'cancel','div' => false)); ?>
</fieldset>
<?php echo $this->Form->end(); ?>
