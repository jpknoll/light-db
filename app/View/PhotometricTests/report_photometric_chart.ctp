<!-- app/View/PhotometricTests/report_photometric_chart.ctp -->

<?php 
	$this->Html->addCrumb('Photometric Tests', '/photometric_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<div id="photometric-chart-phi" style="width:400px; height:400px; float:left;"></div>
<div id="photometric-chart-theta" style="width:400px; height:400px; float:left;"></div>
<?php echo $this->element('Charts/photometric-chart', array(
	'action' => 'find',
	'filter' => $this->passedArgs,
	'id_phi' => 'photometric-chart-phi',
	'id_theta' => 'photometric-chart-theta',
)); ?>

<table id="photometric-table">
	
</table>
