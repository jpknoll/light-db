<!-- app/View/PhotometricTests/index.ctp -->

<?php 
	$this->Html->addCrumb('Photometric Tests', '/photometric_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h1>Photometric Tests</h1>
<table>
    <tr>
    	<th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
		<th>Light Level</th>
    </tr>

    <?php foreach ($tests as $test): ?>
    <tr>
    	<td><?php echo $this->Html->link($test['PhotometricTest']['id'], array('controller' => 'PhotometricTests', 'action' => 'view', $test['PhotometricTest']['id'])); ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['PhotometricTest']['test_datetime']; ?></td>
        <td><?php echo $test['PhotometricTest']['light_level']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($test); ?>
</table>

<?php 
if(!empty($tests)){ 
    echo $this->Paginator->first(__('First', true), array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->prev('� Back', null, null, array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->numbers();
    echo " ";
    echo $this->Paginator->next('Next �', null, null, array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->last(__('Last', true), array('class' => 'disabled'));
} ?>

<?php 
	$this->Paginator->options(array('url' => $this->passedArgs));
?>