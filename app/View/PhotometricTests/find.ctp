<!-- app/View/PhotometricTests/find.ctp -->

<?php 
	$this->Html->addCrumb('Photometric Tests', '/photometric_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<?php echo $this->Form->create('PhotometricTest', array(
)); ?>
	<fieldset>
		<?php
			echo $this->Form->input('product_id', array('type' => 'select', 'options' => $Products, 'empty' => ''));
			echo $this->Form->input('sample_name');
			echo $this->Form->input('test_datetime', array('type' => 'text', 'class' => 'datepicker'));			
			echo $this->Form->submit('Search', array('name' => 'data[SpectralTest][submit_search]')); #formhelper doesn't auto add data[Form] to submit
		?>
	</fieldset>
	<fieldset>
		<?php
			echo $this->Form->input('report', array('type' => 'select', 'options' => $reportTypes, 'empty' => ''));
			echo $this->Form->submit('Generate Report', array('name' => 'data[PhotometricTest][submit_report]')); 
		?>
	</fieldset>
<?php echo $this->Form->end(); ?>

<h1>Photometric Tests</h1>
<table>
    <tr>
    	<th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
		<th>Light Level</th>
    </tr>

    <?php foreach ($tests as $test): ?>
    <tr>
    	<td><?php echo $this->Html->link($test['PhotometricTest']['id'], array('controller' => 'PhotometricTests', 'action' => 'view', $test['PhotometricTest']['id'])); ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['PhotometricTest']['test_datetime']; ?></td>
        <td><?php echo $test['PhotometricTest']['light_level']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($test); ?>
</table>

<?php 
	echo $this->element('paginator', array('list' => $tests)); 
?>