<!-- app/View/PhotometricTests/view.ctp -->

<?php 
	$this->Html->addCrumb('Photometric Tests', '/photometric_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h1>Spectral Test</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
        <th>Light Level</th>
        
        <?php foreach ($facts as $fact): ?>
    	<th><?php echo $fact['PhotometricTestFactType']['fact_type'];  ?></th>
    	<?php endforeach; ?>
    	<?php unset($fact); ?>
    	        
    </tr>

    <tr>
        <td><?php echo $test['PhotometricTest']['id']; ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['PhotometricTest']['test_datetime']; ?></td>
        <td><?php echo $test['PhotometricTest']['light_level']; ?></td>
        
        <?php foreach ($facts as $fact): ?>
    	<td><?php echo $fact['PhotometricTestFact']['fact'];  ?></td>
    	<?php endforeach; ?>
    	<?php unset($fact); ?>
    	
    </tr>

</table>

<table>
	<tr>
		<th>Theta</th>
		<th>Phi</th>
		<th>Lumens</th>
	</tr>
	
	<?php foreach ($test['PhotometricTestData'] as $data): ?>
	<tr>
		<td><?php echo $data['theta'] ?></td>
		<td><?php echo $data['phi'] ?></td>
		<td><?php echo $data['result'] ?></td>
	</tr>
	<?php endforeach; ?>
	<?php unset($data); ?>
</table>