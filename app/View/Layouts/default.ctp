<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html ng-app="project">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		LED Performance Database: <?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
	?>
	
	<?php
		echo $this->Html->css('cake.generic');
		echo $this->Html->css('smoothness/jquery-ui-1.10.3.custom.min.css');
		echo $this->Html->css('app.css');
		echo $this->Html->css('loading-bar.css');
		echo $this->fetch('css');
		
		echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js');
		echo $this->Html->script('jquery-ui-1.10.3.custom.min.js');
		echo $this->Html->script('http://malsup.github.com/jquery.form.js'); 

		echo $this->Html->script('angular-file-upload/dist/angular-file-upload-shim.min.js');
		echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/angularjs/1.2.14/angular.min.js');
		echo $this->Html->script('loading-bar.js');

		echo $this->Html->script('http://code.highcharts.com/highcharts.js');
		echo $this->Html->script('http://code.highcharts.com/highcharts-more.js');
	
		echo $this->fetch('script');
	?>
</head>
<body>
	<style>
		#header-2 {
			background: #fff;
			height: 17px;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		#breadcrumb {
			float: left;
			color: #333;
		}
		#usercp {
			float: right;
			color: #333;
		}
	</style>

	<div id="container">
		<div id="header">
			LED Performance Database
		</div>
		<div id="content">
			<div id="header-2">
				<div id="breadcrumb">
					<?php echo $this->Html->getCrumbs(' > ', 'Home'); ?>
				</div>
				<div id="usercp">
					<?php echo $this->element('usercp', array('auth' => $this->Session->read('Auth'))); ?>
				</div>			
			</div>
			
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
		</div>
	</div>
		
	<script>
		$(function() {
			$( ".datepicker" ).datepicker({
				showOn: "button",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd"
			}).next()
				.text("").button({icons:{primary:"ui-icon-calendar"}});
		});
	</script>
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-49164116-1', 'ledperformancedatabase.org');
	  ga('send', 'pageview');
	</script>
</body>
</html>
