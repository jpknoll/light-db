<!-- app/View/flicker_uploads/admin_transfer.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Uploads', '/flicker_uploads');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>
<div ng-controller="FlickerController">

<h2>Flicker Upload Results</h2>

<span>Enter test information:</span>
<br /><br />

<?php echo $this->Form->create(); ?>
<h3>Test Info</h3>
<?php 
	echo $this->Form->input('FlickerTest.product_id', array('label' => 'Product', 'id' => 'product', 'options' => $products, 'empty' => ''));
	echo $this->Form->input('FlickerTest.sample_id', array('label' => 'Sample', 'id' => 'sample_container', 'empty' => 'Select Product'));
	echo $this->Form->input('FlickerTest.dimmer_id', array('label' => 'Dimmer', 'options' => $dimmers));
	echo $this->Form->input('FlickerTest.dimmer_level', array('label' => 'Dimmer Level'));
	echo $this->Form->input('FlickerTest.test_datetime', array('label' => 'Date (yyyy-mm-dd)', 'class' => 'datepicker'));
?>

<h3>Facts</h3>
<table>
	<thead>
		<tr>
			<th>Type</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($flicker_upload['FlickerUploadFacts'] as $fact): ?>
		<tr>
			<td><?php echo $fact['FlickerUploadFactType']['flicker_upload_fact_type']; ?></td>
			<td><?php echo $fact['value']; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<span>Using the upload information above, create test facts:</span>
<br /><br />

<table>
	<thead>
		<tr>
			<th>Type</th>
			<th>Value</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr ng-repeat="fact in flicker_test_facts">
			<td id="fact_type_container">
				<select name="data[FlickerTestFact][{{$index}}][flicker_test_fact_type_id]">
					<option value=""></option>
					<?php foreach($flicker_test_fact_types as $i => $type): ?>
					<option value="<?php echo $i; ?>"><?php echo $type; ?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td><input name="data[FlickerTestFact][{{$index}}][value]" type="text" ng-model="f.value"></td>
			<td><button ng-click="delete_flickerTestFact($index)" type="button">Delete</button></td>
		</tr>
		<tr>
			<td></td>
			<td></td>			
			<td><button ng-click="add_flickerTestFact()" type="button">Add</button></td>
		</tr>
	</tbody>
</table>
	
<h3>Data</h3>
<table>
	<thead>
		<tr>
			<th>Filter Freq.</th>
			<th>Filter Fund. Freq.</th>
			<th>Flicker Index</th>
			<th>Flicker Percent</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($flicker_upload['FlickerUploadData'] as $i => $result): ?>
		<tr>
			<td><?php echo $this->Form->input("FlickerTestData.{$i}.filter_frequency", array(
				'label' => false,
				'value' => $result['filter_frequency'],
				'readonly')); ?></td>
			<td><?php echo $this->Form->input("FlickerTestData.{$i}.fundamental_frequency", array(
				'label' => false,
				'value' => $result['fundamental_frequency'],
				'readonly')); ?></td>
			<td><?php echo $this->Form->input("FlickerTestData.{$i}.flicker_index", array(
				'label' => false,
				'value' => $result['flicker_index'],
				'readonly')); ?></td>
			<td><?php echo $this->Form->input("FlickerTestData.{$i}.flicker_percent", array(
				'label' => false,
				'value' => $result['flicker_percent'],
				'readonly')); ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<span>Verify the above test results before hitting upload:</span>
<br /><br />

<?php echo $this->Form->submit(__('Upload')); ?>
<?php echo $this->Form->end(); ?>

</div>

<script language="JavaScript">
$('#product').change(function(){
	$('#sample_container').load('/products/get_samples/' + $(this).val(), function() {
		$('#sample').attr('name', 'data[FlickerTest][sample_id]');
	});
});
</script>

<?php echo $this->Html->script('controller/flicker_uploads/flicker_uploads_admin_transfer.js'); ?>