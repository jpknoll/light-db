<!-- app/View/flicker_uploads/admin_index.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Uploads', '/flicker_uploads');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>
	
<h2>Flicker Upload Results</h2>

<small class="edit-link">
<?php echo $this->Html->link('Transfer to Test', array('action' => 'admin_transfer', $flicker_upload['FlickerUpload']['id'])); ?>
</small>

<h3>Facts</h3>
<table>
	<thead>
		<tr>
			<th>Type</th>
			<th>Value</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($flicker_upload['FlickerUploadFacts'] as $fact): ?>
		<tr>
			<td><?php echo $fact['FlickerUploadFactType']['flicker_upload_fact_type']; ?></td>
			<td><?php echo $fact['value']; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
	
<h3>Data</h3>
<table>
	<thead>
		<tr>
			<th>Filter Freq.</th>
			<th>Filter Fund. Freq.</th>
			<th>Flicker Index</th>
			<th>Flicker Percent</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($flicker_upload['FlickerUploadData'] as $result): ?>
		<tr>
			<td><?php echo $result['filter_frequency']; ?></td>
			<td><?php echo $result['fundamental_frequency']; ?></td>
			<td><?php echo $result['flicker_index']; ?></td>
			<td><?php echo $result['flicker_percent']; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?php echo $this->Html->script('controller/flicker_uploads/flicker_uploads_parse.js'); ?>