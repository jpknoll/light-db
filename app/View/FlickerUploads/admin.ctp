<!-- app/View/flicker_uploads/admin.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Uploads', '/flicker_uploads');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Flicker Uploads</h2>

<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>User</th>
			<th>Upload Datetime</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($flicker_uploads as $upload): ?>
		<tr>
			<td><?php echo $this->Html->link($upload['FlickerUpload']['id'], array('action' => 'admin_view', $upload['FlickerUpload']['id'])); ?></td>
			<td><?php echo $upload['User']['username']; ?></td>
			<th><?php echo $upload['FlickerUpload']['upload_datetime']; ?></th>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>