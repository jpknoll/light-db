<!-- app/View/flicker_uploads/index.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Uploads', '/flicker_uploads');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Flicker Uploads</h2>
<div style="margin-left:10%; margin-right:10%;">
	<span>Introduction text</span>
	<br /><br />
	<?php if ($this->Session->check('Auth.User')): ?>
		<?php echo $this->Html->link('Flicker Upload Tool', array('action' => 'parse')); ?>
		<br /><br />
	<?php else: ?>
		You will need to log in or register to continue:
		<br /><br />
		<?php echo $this->element('usercp', array(null)); ?>
		<br /><br />
	<?php endif; ?>
</div>