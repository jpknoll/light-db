<!-- app/View/flicker_uploads/parse.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Uploads', '/flicker_uploads');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<?php
	echo $this->Html->script('fft.js', array('block' => 'script'));
	echo $this->Html->script('assets/array_extensions.js', array('block' => 'script'));
	
	echo $this->Html->script('angular-file-upload/dist/angular-file-upload.min.js', array('block' => 'script'));
?>
	
<style>
div.quarter-chart {
	float: left;
	width: 25%; 
	height: 400px; 
}
</style>

<div ng-controller="FlickerController">
<?php echo $this->Form->create('FlickerUpload', array(
	'url' => '/flicker_uploads/parse',
	'type' => 'file',
)); 

	echo $this->Form->input('FlickerUploadAttachment.csv', array(
		'label' => 'Select CSV File',
		'type' => 'file',
		'ng-file-select' => 'readFile($files)',
	)); 

?>

	<div ng-hide="flicker_upload_facts === undefined">
	<h2>Facts</h2>
	<table>
		<thead>
			<tr>
				<th>Type</th>
				<th>Value</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="f in flicker_upload_facts">
				<td>
					<!-- use hidden because angular is dumb, doesn't use the correct value in select -->
					<select ng-model="f.flicker_upload_fact_type_id" ng-options="t.FlickerUploadFactType.id as t.FlickerUploadFactType.flicker_upload_fact_type for t in flicker_upload_fact_types"></select>
					<input name="data[FlickerUploadFact][{{$index}}][flicker_upload_fact_type_id]" type="hidden" value="{{f.flicker_upload_fact_type_id}}">
				</td>
				<td><input  name="data[FlickerUploadFact][{{$index}}][value]" type="text" ng-model="f.value"></td>
				<td><button ng-click="delete_flickerUploadFact($index)">Delete</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><button ng-click="add_flickerUploadFact()">Add</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	<div ng-hide="flicker_upload_data === undefined">
	<h2>Data</h2>
	<table ng-cloak>
		<thead>
			<tr>
				<th>Filter Freq.</th>
				<th>Filter Fund. Freq.</th>
				<th>Flicker Index</th>
				<th>Flicker Percent</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="d in flicker_upload_data">
				<td><input type="text" readonly name="data[FlickerUploadData][{{$index}}][filter_frequency]" ng-model="d.filter_frequency"></td>
				<td><input type="text" readonly name="data[FlickerUploadData][{{$index}}][fundamental_frequency]" ng-model="d.fundamental_frequency"></td>
				<td><input type="text" readonly name="data[FlickerUploadData][{{$index}}][flicker_index]" ng-model="d.flicker_index"></td>
				<td><input type="text" readonly name="data[FlickerUploadData][{{$index}}][flicker_percent]" ng-model="d.flicker_percent"></td>
			</tr>
		</tbody>
	</table>
	<input type="submit" ng-hide="flicker_upload_data === undefined" value="Upload"></submit>
<?php echo $this->Form->end; ?>
</div>

<?php echo $this->Html->script('controller/flicker_uploads/flicker_uploads_parse.js'); ?>