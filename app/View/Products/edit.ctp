<!-- app/View/Products/edit.ctp -->

<?php 
	$this->Html->addCrumb('Products', '/products');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h1>Product</h1>

<p><strong>Id:</strong> <?php echo $this->request->data['Product']['id']; ?></p>

<?php 
echo $this->Form->create('Product', array(
	'type' => 'post',
	'action' => 'edit',
));

echo $this->Form->input('Product.id', array('type' => 'hidden'));
echo $this->Form->input('Product.public_name');
if ($this->Permissions->check('Product->internal_name')) {
	echo $this->Form->input('Product.internal_name');
}
echo $this->Form->submit('Edit', array('action' => 'edit', 'id' => 'edit-product'));
echo $this->Form->end();
?>

<h2>Pictures</h2>

<style>
	#picture-list {
		list-style-type: none; 
		width: 95%;
		padding: .5em;
		margin-bottom: 1em;
	}
	#picture-list li {		
		border: 1px solid black;
		margin: 0px 0px 3px 0px;
	}
	#picture-list li span.ui-icon {
		position: absolute;
		display: inline;
	}
	#picture-list li img.product-image {
		display: inline;
		max-width:200px;
		max-height:100px;
		margin: 3px;
	}
	#picture-list li div.picture-delete {
		display: inline;
	}
	#picture-add {
		border: 1px solid black;
		width: 95%;
		padding: .5em;
	}
</style>
	
<?php echo $this->Form->create('ProductImage', array(
	'type' => 'file',
	'action' => 'add',
));
?>
	
<ul id="picture-list">
	<?php foreach($this->request->data['ProductImages'] as $image): ?>
	<li id="picture-<?php echo $image['id']; ?>">
		<?php echo $this->Html->image('/'.$image['path_small'], array('class' => 'product-image')); ?>
		<div class="picture-delete"><?php echo $this->Html->link('Delete', array('controller' => 'ProductImages', 'action' => 'delete', $image['id'])); ?></div>
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
	</li>
	<?php endforeach; ?>
	<?php unset($image); ?>
</ul>

<div id="picture-add">
<?php
	echo $this->Form->input('product_id', array('type' => 'hidden', 'value' => $this->request->data['Product']['id']));
	echo $this->Form->input('image', array('type' => 'file', 'label' => 'Add Image')); 
	echo $this->Form->submit('Upload');
?>
</div>
<?php echo $this->Form->end(); ?>

<script language="JavaScript">
	$("#picture-list").sortable({
		update: function(event, ui) {
			var list = $(ui.item).parent();
			var items = list.sortable("toArray").map(function(point, i) {
				return parseInt(point.split('-')[1]);
			});
			var data = {};
			$.each(items, function(i, point) { 
				key = 'order[' + i + ']'; 
				data[key] = point; 
			});
			
			$.ajax({
				type: "POST",
				url: '<?php echo $this->Html->url(array('controller' => 'product_images', 'action' => 'order', $this->request->data['Product']['id'])); ?>',
				data: data,
				success: function() {
					
				},
			});
		}
	});
	$("#picture-list").disableSelection();
</script>

<h2>Facts</h2>
<table id="ProductFactsTable">
	<thead>
	<tr>
		<th>ID</th>
		<th>Product Fact Type</th>
		<th>Value</th>
		<th>Source</th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	<?php for ($i = 0; $i < count($this->request->data['ProductFacts']); $i++): ?>
	<tr id="<?php echo "ProductFacts{$i}Row"; ?>">
		<td><?php echo $this->Form->input("ProductFacts.{$i}.id", array('type' => 'hidden')); ?></td>
		<td><?php echo $this->Form->input("ProductFacts.{$i}.product_fact_type_id", array('label' => false, 'class' => 'product-fact-input')); ?></td>
		<td><?php echo $this->Form->input("ProductFacts.{$i}.value", array('label' => false, 'class' => 'product-fact-input')); ?></td>
		<td><?php echo $this->Form->input("ProductFacts.{$i}.source", array('label' => false, 'class' => 'product-fact-input')); ?></td>
		<td><?php echo $this->Form->submit('Delete', array('id' => "ProductFacts{$i}Delete", 'action' => 'delete', 'class' => 'delete-fact', 'onclick' => 'deleteFact(this);')); ?></td>
		<td><?php echo $this->Html->image('ajax.gif', array('id' => "ProductFacts{$i}Ajax", 'style' => 'display:none')); ?></td>	
	</tr>
	<?php endfor; ?>
	
	<tr id="<?php echo "ProductFacts{$i}Row"; ?>">
		<td><?php echo $this->Form->input("ProductFacts.{$i}.id", array('type' => 'hidden')); ?></td>
		<td><?php echo $this->Form->input("ProductFacts.{$i}.product_fact_type_id", array('label' => false)); ?></td>
		<td><?php echo $this->Form->input("ProductFacts.{$i}.value", array('label' => false)); ?></td>
		<td><?php echo $this->Form->input("ProductFacts.{$i}.source", array('label' => false)); ?></td>
		<td><?php echo $this->Form->submit('Add', array('id' => "ProductFacts{$i}Add", 'action' => 'add', 'class' => 'add-fact', 'onclick' => 'addFact(this);')); ?></td>	
		<td><?php echo $this->Html->image('ajax.gif', array('id' => "ProductFacts{$i}Ajax", 'style' => 'display:none')); ?></td>
	</tr>
	</tbody>
</table>

<script language="JavaScript">
	$('.product-fact-input').change(function(sender) {
		editFact(this);
	});
	
	function editFact(sender) {
		// strip out the iterator value
		var i = sender.id.replace('ProductFacts', '').replace('ProductFactTypeId', '').replace('Value', '').replace('Source', '');
		
		// find the product fact id
		var id = $('#ProductFacts' + i + 'Id').val();
		
		// fetch data
		var product_fact_type_id = $('#ProductFacts' + i + 'ProductFactTypeId').val();
		var value = $('#ProductFacts' + i + 'Value').val();
		var source = $('#ProductFacts' + i + 'Source').val();
		
		// build post
		var data = {
			ProductFact: {
				id: id,
				product_fact_type_id: product_fact_type_id,
				value: value,
				source: source,
			}
		};
		
		// show ajax gif
		$('#ProductFacts' + i + 'Ajax').show();
		
		// submit post
		$.post('/product_facts/edit/' + id, data, function(data, textStatus, jqXHR) {
			// hide ajax gif
			$('#ProductFacts' + i + 'Ajax').hide();
		});
	}
	
	function addFact(sender) {
		// strip out the iterator value
		var i = sender.id.replace('ProductFacts', '').replace('Add', '');
		
		// fetch data
		var product_fact_type_id = $('#ProductFacts' + i + 'ProductFactTypeId').val();
		var value = $('#ProductFacts' + i + 'Value').val();
		var source = $('#ProductFacts' + i + 'Source').val();
		
		// build post
		var data = {
			ProductFact: {
				product_id: <?php echo $this->request->data['Product']['id']; ?>,
				product_fact_type_id: product_fact_type_id,
				value: value,
				source: source,
			}
		};
		
		// show ajax gif
		$('#ProductFacts' + i + 'Ajax').show();
		
		// submit post
		$.ajax({
			type: 'POST',
			url: '/product_facts/add/', 
			data: data, 
			dataType: 'json',
			success: function(data, textStatus, jqXHR) {			
				// hide ajax gif
				$('#ProductFacts' + i + 'Ajax').hide();
				
				// clone/create new row
				var new_i = Number(i) + 1;
				var new_row = $("<tr id='ProductFacts" + new_i + "Row'></tr>");
				$('#ProductFactsTable').append(new_row);
				
				var new_hidden = $('#ProductFacts' + i + 'Id').clone(true);
				new_hidden.attr('id', 'ProductFacts' + new_i + 'Id');
				new_row.append($('<td></td>').append(new_hidden));
				
				var new_type = $('#ProductFacts' + i + 'ProductFactTypeId').clone(true);
				new_type.attr('id', 'ProductFacts' + new_i + 'ProductFactTypeId');
				new_row.append($('<td></td>').append(new_type));
				
				var new_value = $('#ProductFacts' + i + 'Value').clone(true);
				new_value.attr('id', 'ProductFacts' + new_i + 'Value');
				new_value.val('');
				new_row.append($('<td></td>').append(new_value));
				
				var new_source = $('#ProductFacts' + i + 'Source').clone(true);
				new_source.attr('id', 'ProductFacts' + new_i + 'Source');
				new_row.append($('<td></td>').append(new_source));
				
				var new_add = $('#ProductFacts' + i + 'Add').clone(true);
				new_add.attr('id', 'ProductFacts' + new_i + 'Add');
				new_row.append($('<td></td>').append(new_add));
				
				var new_ajax = $('#ProductFacts' + i + 'Ajax').clone(true);
				new_ajax.attr('id', 'ProductFacts' + new_i + 'Ajax');
				new_row.append($('<td></td>').append(new_ajax));
				
				// convert old row
				var old_hidden = $('#ProductFacts' + i + 'Id');
				old_hidden.val(data.data.ProductFact.id);
				
				var old_type = $('#ProductFacts' + i + 'ProductFactTypeId');
				old_type.attr('onchange', 'editFact(this);');
				
				var old_value = $('#ProductFacts' + i + 'Value');
				old_value.attr('onchange', 'editFact(this);');
				
				var old_source = $('#ProductFacts' + i + 'Source');
				old_source.attr('onchange', 'editFact(this);');
				
				var old_button = $('#ProductFacts' + i + 'Add');
				old_button.attr('id', 'ProductFacts' + i + 'Delete');
				old_button.attr('action', 'Delete');
				old_button.attr('onclick', 'deleteFact(this);');
				old_button.removeClass('add-fact').addClass('delete-fact');
				old_button.val('Delete');
			},
		});
		
		// stop submit
		return false;
	}
	
	function deleteFact(sender) {
		
		if (!confirm("Are you sure you want to delete this fact?")) {
			return;
		}
		
		// strip out the iterator value
		var i = sender.id.replace('ProductFacts', '').replace('Delete', '');
		
		// find the product fact id
		var id = $('#ProductFacts' + i + 'Id').val();
		
		// show ajax gif
		$('#ProductFacts' + i + 'Ajax').show();
		
		// submit request to delete
		$.ajax({
			url: '/product_facts/delete/' + id, 
			success: function(data, textStatus, jqXHR) {
				// hide ajax gif
				$('#ProductFacts' + i + 'Ajax').hide();
				
				// remove deleted row
				$('#ProductFacts' + i + 'Row').remove();
			},
		});
		
		// stop submit
		return false;
	}
</script>
