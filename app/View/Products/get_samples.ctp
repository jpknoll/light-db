<label for="sample">Sample</label>
<select name="sample" id="sample">
	<option value=""></option>
	<?php foreach($samples as $sample): ?>
	<option value="<?php echo $sample['id']; ?>"><?php echo $sample['public_name']; ?></option>
	<?php endforeach; ?>
</select>