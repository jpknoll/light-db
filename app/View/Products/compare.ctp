<!-- app/View/Products/compare.ctp -->

<?php 
	$this->Html->addCrumb('Products', '/products');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>
<div ng-controller="ProductsController">
	<div id="source-select"><p>Set Table Source: </p><select ng-model="source" ng-options="s for s in sources"></select></div>
	<table>
		<thead>
			<tr>
				<th></th>
				<th ng-repeat="product in products">{{ product.Product.public_name }}</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($this->Permissions->check('ProductImage')): ?>
			<tr id="product-images">
				<td></td>
				<td class="col0"><?php if(array_key_exists(0, $products)) { echo $this->Html->image('/'.$products[0]['ProductImages'][0]['path_medium'], array('class' => 'product-image')); } ?></td>
				<td class="col1"><?php if(array_key_exists(1, $products)) { echo $this->Html->image('/'.$products[1]['ProductImages'][0]['path_medium'], array('class' => 'product-image')); } ?></td>
				<td class="col2"><?php if(array_key_exists(2, $products)) { echo $this->Html->image('/'.$products[2]['ProductImages'][0]['path_medium'], array('class' => 'product-image')); } ?></td>
			</tr>
			<?php endif; ?>
			<tr ng-repeat="t in product_fact_types | productFactTypeFilter:source">
				<td>{{ t.ProductFactType.name }}</td>
				<td ng-repeat="product in products">{{ getFactValue(product, t.ProductFactType.name, source) }}</td>
			</tr>
		</tbody>
	</table>
</div>

<?php echo $this->Html->script('controller/products/products_compare.js'); ?>

<div class="chart">
	<div id="photometric-chart-phi"></div>
	<select id="photometric-chart-phi-select"></select>
	<div id="photometric-chart-phi-slider" class="slider"></div>
</div>

<div class="chart">
	<div id="photometric-chart-theta"></div>
	<select id="photometric-chart-theta-select"></select>
	<div id="photometric-chart-theta-slider" class="slider"></div>
</div>
<?php echo $this->element('Charts/photometric-chart', array(
	'filter' => array(
		'product_id' => $product_ids,
	),
)); ?>

<div style="clear:both;" ></div>

<div id="chromaticity-chart" class="chart"></div>
<?php echo $this->element('Charts/chromatic-chart', array('id' => 'chromaticity-chart')); ?>

<div id="chromaticity-chart-zoomed" class="chart"></div>
<?php echo $this->element('Charts/chromatic-chart-zoomed', array('id' => 'chromaticity-chart-zoomed')); ?>
	
<script>
	$.ajax({
		url: '<?php echo $this->Html->url(array(
				'controller' => 'SpectralTestFacts', 
				'action' => 'group_by_product', 
				'product_id' => $product_ids)); ?>/.json',
		success: function(data) {			
			var chart = $('#chromaticity-chart').highcharts(); 
			var chartZoomed = $('#chromaticity-chart-zoomed').highcharts();
			
			// fetch coordinates
			var cie_x_facts = data.spectral_test_facts.filter(function(fact) {
				return fact.SpectralTestFactType.fact_type == 'CIE x';
			});
			var cie_y_facts = data.spectral_test_facts.filter(function(fact) {
				return fact.SpectralTestFactType.fact_type == 'CIE y';
			});
			
			// cie coordinates
			for(i in cie_x_facts) {	
				// find corresponding y
				var cie_x_fact = cie_x_facts[i];
				var less_cie_y_facts = cie_y_facts.filter(function(fact) { return fact.Product.id = cie_x_fact.Product.id });
				if (less_cie_y_facts.length == 0) {
					continue;
				}
				var cie_y_fact = less_cie_y_facts[0];
				
				var series_id = cie_x_fact.Product.id;
				var series_name = cie_x_fact.Product.public_name;
				
				// round off to 4 digits as string, then back to float!
				var cie_x_value = parseFloat(cie_x_fact[0].fact_average).toFixed(4);
				var cie_y_value = parseFloat(cie_y_fact[0].fact_average).toFixed(4);
				cie_x_value = parseFloat(cie_x_value);
				cie_y_value = parseFloat(cie_y_value);
				
				series = {
					id: series_id,
					name: series_name,
					data: [[cie_x_value, cie_y_value]],
					type: 'scatter',
					marker: {
						fillColor: 'black'
					},
				};
				
				chart.addSeries(series);
				chartZoomed.addSeries(series);
			}
		}
	});
</script>