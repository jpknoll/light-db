<!-- app/View/Products/find.ctp -->

<?php 
	$this->Html->addCrumb('Products', '/products');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<style>
	.search div {
		clear: both;
		width: 100%;
	}
	
	.search div select,input,p {
		clear: none;
		float: left;
		font-size: 120%;
		max-width: 300px;
		padding: 0;
		margin: 2px;
	}
		
	.search div button,span {
		float: right;
		font-size: 120%;
		padding: 0;
		margin: 2px;
	}
</style>

<h2>Products</h2>

<div ng-controller="ProductsController" class="search">
	<div><p><b>Limit By: </b>Product Fact Source: </p><select ng-model="source" ng-options="s for s in sources"></select></div>
	<br /><br />
	<div><p><b>Limit By: </b> Lamp Type: </p><select ng-model="product_lamp_type" ng-options="t.ProductLampType.product_lamp_type for t in product_lamp_types"></select></div>
	<div ng-repeat="query in queries">
		<select ng-model="query.key" ng-options="t.ProductFactType.name for t in product_fact_types"></select>
		<select ng-show="query.key.ProductFactType.value_type == 'number'" ng-model="query.comparer" ng-options="c for c in query_comparers"></select>
		<input ng-show="query.key.ProductFactType.value_type == 'number'" type="text" ng-model="query.text" placeholder="({{query.key.ProductFactType.min}} - {{query.key.ProductFactType.max}})">
		<select ng-show="query.key.ProductFactType.value_type == 'select'" ng-model="query.option" ng-options="o for o in query.key.ProductFactType.options"></select>
		<input ng-show="query.key.ProductFactType.value_type == 'boolean'" ng-model="query.bool" ng-options="b for b in query_bools"></select>
		<button ng-click="removeQuery($index)">Remove</button>
	</div>
	<div>
		<select ng-model="query_mode" ng-options="q for q in query_modes"></select>
		<button ng-click="addQuery()">Add</button>
	</div>
	<div ng-cloak><span>{{filterProductCount()}} of {{productCount()}} Products</span></div>
	<table>
		<thead>
			<th>ID</th>
			<th>Name</th>
			<?php if($this->Permissions->check('Product.internal_name')) { ?><th>Internal Name</th><?php } ?>
			<th>Watts</th>
			<th>Lumens</th>
			<th>CCT</th>
			<th>CRI</th>
			<th>Efficacy</th>
			<th>Lifetime Rating</th>
		</thead>
		<tbody ng-cloak>
			<tr ng-repeat="product in products | productFactFilter:queries:query_mode:source">
				<td>{{product.Product.id}}</td>
				<td><a href="view/{{product.Product.id}}">{{product.Product.public_name}}</a></td>
				<?php if($this->Permissions->check('Product.internal_name')) { ?><td>{{product.Product.internal_name}}</td><?php } ?>
				<td>{{ getFactValue(product, "Wattage", source) }}</td>
				<td>{{ getFactValue(product, "Luminous Flux", source) }}</td>
				<td>{{ getFactValue(product, "Correlated Color Temperature", source) }}</td>
				<td>{{ getFactValue(product, "Color Rendering Index", source) }}</td>
				<td>{{ getFactValue(product, "Efficacy", source) }}</td>
				<td>{{ getFactValue(product, "Warranty", source) }}</td>
			</tr>
		</tbody>
	</table>
</div>

<?php echo $this->Html->script('controller/products/products_find.js'); ?>