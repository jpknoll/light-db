<!-- app/View/Products/index.ctp -->

<?php 
	$this->Html->addCrumb('Products', '/products');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<style>
	#product-compare {
		float: right;
	}
	div.product-compare-item {
		position: relative;
		float: left;
		clear: none;
		
		display: block;
		border: 1px solid grey;
		width: 150px;
		height: 100px;
		margin: 8px 0 0 6px;
		padding: 0;
	}
	div.product-compare-item img {
		max-width: 150px;
		max-height: 100px;
	}
	div.product-compare-item span.icon-overlay {
		position: absolute;
		top: 0;
		right: 0;
	}
	
	#product-compare-submit {		
		margin: 8px 0 0 6px;
		padding: 0;
	}
	
	#source-select p,select {
		float: left;
		clear: none;
		
		font-size: 120%;
		margin: 2px;
	}
	
	#lamp-select p,select {
		float: left;
		clear: none;
		
		font-size: 120%;
		margin: 2px;
	}
</style>

<div ng-controller="ProductsController">
	<form ng-submit="submitCompare()">
	<div id="product-compare">
		<div class="product-compare-item" ng-repeat="compare in compares" ng-click="toggleCompare(compare)">
			<span ng-show="compare.show_img !== undefined" class="ui-icon ui-icon-circle-close icon-overlay"></span>
			<input ng-model="compare.product.Product.id" type="hidden" id="{{ $index }}">
			<div ng-hide="compare.show_img">{{ compare.product.Product.id }}</div>
			<img ng-show="compare.show_img" src="{{ compare.product.ProductImages[0].path_small }}">
		</div>
		<input id="product-compare-submit" type="submit" value="Compare">
	</div>
	</form>
	
	<h2>Products</h2>
	<?php echo $this->Html->link('Search/Filter', array('controller' => 'products', 'action' => 'find')); ?>
	
	<br /><br />
	<div id="source-select"><p>Set Table Source: </p><select ng-model="source" ng-options="s for s in sources"></select></div>
	<br /><br />
	<div id="lamp-select"><p>Select Lamp Type: </p><select ng-model="product_lamp_type" ng-options="t.ProductLampType.product_lamp_type for t in product_lamp_types"></select></div>
	
	<table>
		<thead>
			<th></th>
			<th>ID</th>
			<th>Name</th>
			<?php if($this->Permissions->check('Product.internal_name')) { ?><th>Internal Name</th><?php } ?>
			<th>Watts</th>
			<th>Lumens</th>
			<th>CCT</th>
			<th>CRI</th>
			<th>Efficacy</th>
			<th>Lifetime Rating</th>
			<?php if ($this->Permissions->check('ProductImage')) { ?><th></th><?php } ?>
		</thead>
		<tbody ng-cloak>			
			<tr ng-repeat="product in products | productLampTypeFilter:product_lamp_type">
				<td><input type="checkbox" class="product-compare-selector" ng-model="product.compare" ng-change="toggleProduct(product)"></td>
				<td>{{product.Product.id}}</td>
				<td><a href="/products/view/{{product.Product.id}}">{{product.Product.public_name}}</a></td>
				<?php if($this->Permissions->check('Product.internal_name')) { ?><td>{{product.Product.internal_name}}</td><?php } ?>
				<td>{{ getFactValue(product, "Wattage", source) }}</td>
				<td>{{ getFactValue(product, "Luminous Flux", source) }}</td>
				<td>{{ getFactValue(product, "Correlated Color Temperature", source) }}</td>
				<td>{{ getFactValue(product, "Color Rendering Index", source) }}</td>
				<td>{{ getFactValue(product, "Efficacy", source) }}</td>
				<td>{{ getFactValue(product, "Warranty", source) }}</td>
				<?php if($this->Permissions->check('ProductImage')) { ?><td><img src="{{ product.ProductImages[0].path_small }}"></td><?php } ?>
			</tr>
		</tbody>
	</table>
</div>

<?php echo $this->Html->script('controller/products/products_index.js'); ?>