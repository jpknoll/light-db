<!-- app/View/Products/view.ctp -->

<?php 
	$this->Html->addCrumb('Products', '/products');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<style>
	img.product-image {
		float: right;
		max-width:400px;
		max-height:400px;
	}
	td.first_col {
		width: 25%;
	}
</style>

<div ng-controller="ProductsController">
	<h2>Product</h2>
	<small class="edit-link"><?php echo $this->Html->link('edit', array('controller' => 'products', 'action' => 'edit', $product['Product']['id'])); ?></small>
	<br /><br />
	<div class="left-half">
		<table id="product-summary" ng-cloak>
			<tr>
				<td class="first_col"><strong>Id:</strong></td>
				<td>{{ product.Product.id }}</td>
			</tr>
			<tr>
				<td class="first_col"><strong>Name:</strong></td>
				<td>{{ product.Product.public_name }}</td>
			</tr>
			<?php if ($this->Permissions->check('Product.internal_name')): ?>
			<tr>
				<td class="first_col"><strong>Internal Name:</strong></td>
				<td>{{ product.Product.internal_name }}</td>
			</tr>
			<?php endif; ?>
			<?php if ($this->Permissions->check('Product.manufacturer')): ?>
			<tr>
				<td class="first_col"><strong>Manufacturer / Model:</strong></td>
				<td>{{ product.Product.manufacturer }}</td>
			</tr>
			<?php endif; ?>
			<?php if ($this->Permissions->check('Product.part_number')): ?>
			<tr>
				<td class="first_col"><strong>Part Number:</strong></td>
				<td>{{ product.Product.part_number }}</td>
			</tr>
			<?php endif; ?>
		</table> 
		<br />
		<br />
		<?php if($this->Permissions->check('Sample')) : ?>
		<h3>Samples</h3>
		<table id="samples-summary" ng-cloak>
			<tr>
				<th class="first_col">Testing Configuration</th>
				<th>Count</th>
			</tr>
			<tr ng-repeat="configuration in spectral_test_configurations">
				<td class="first_col">{{ configuration }}</td>
				<td>{{ getSpectralTestCountByConfiguration(configuration) }}</td>
			</tr>
		</table>
		<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'samples', 'action' => 'find', 'product_id' => $product['Product']['id'])); ?></small>
		<br /><br />
		<?php endif; ?>
	</div>
	
	<?php if ($this->Permissions->check('ProductImage')) {
		if(isset($product['ProductImages'][0])) {
			echo $this->Html->image('/'.$product['ProductImages'][0]['path_medium'], array(
				'alt' => 'CakePHP',
				'class' => 'product-image',		
			));
		}
	} ?>
	
	<?php if ($this->Permissions->check('ProductFact')): ?>
		<h2 style="clear:both;">Product Characteristics</h2>
		<table id="product-facts-summary" ng-cloak>
			<tr>
				<th class="first_col"></th>
				<th ng-repeat="source in sources">{{ source }}</th>
			</tr>
			<tr ng-repeat="t in product_fact_types | productFactTypeFilter:source">
				<td class="first_col">{{ t.ProductFactType.name }}</td>
				<td ng-repeat="source in sources">{{ getProductFact(t.ProductFactType.name, source) }}</td>
			</tr>
		</table>
		<?php if ($product['ProductLampType']['product_lamp_type'] == 'Omni') : ?>
		<span>* <small>CLTC facts derived from Photometric Test configuration 'Base-Down'</small></span>
		<?php else: ?>
		<span>* <small>CLTC facts derived from Photometric Test configuration 'Base-Up - Open'</small></span>
		<?php endif; ?>
		<br /><br />
	<?php endif; ?>
	
	<?php if ($this->Permissions->check('PhotometricTest')): ?>
		<div class="divider"></div>
		<div id="chromaticity-chart" class="chart"></div>
		<?php echo $this->element('Charts/chromatic-chart', array('id' => 'chromaticity-chart')); ?>
				
		<div id="chromaticity-chart-zoomed" class="chart"></div>
		<?php echo $this->element('Charts/chromatic-chart-zoomed', array('id' => 'chromaticity-chart-zoomed')); ?>
		
		<script>
		$.ajax({
			url: '<?php echo $this->Html->url(array(
					'controller' => 'spectral_test_facts', 
					'action' => 'group_by_product', 
					'product_id' => $product['Product']['id'])); ?>/.json',
			success: function(data) {
				var chart = $('#chromaticity-chart').highcharts();
				var chartZoomed = $('#chromaticity-chart-zoomed').highcharts();
				
				// fetch coordinates
				var cie_x_facts = data.spectral_test_facts.filter(function(fact) {
					return fact.SpectralTestFactType.fact_type == 'CIE x';
				});
				var cie_y_facts = data.spectral_test_facts.filter(function(fact) {
					return fact.SpectralTestFactType.fact_type == 'CIE y';
				});
				
				// cie coordinates
				for(i in cie_x_facts) {	
					// find corresponding y
					var cie_x_fact = cie_x_facts[i];
					var less_cie_y_facts = cie_y_facts.filter(function(fact) { return fact.Product.id = cie_x_fact.Product.id });
					if (less_cie_y_facts.length == 0) {
						continue;
					}
					var cie_y_fact = less_cie_y_facts[0];
					
					var series_id = cie_x_fact.Product.id;
					var series_name = cie_x_fact.Product.public_name;
					
					// round off to 4 digits as string, then back to float!
					var cie_x_value = parseFloat(cie_x_fact[0].fact_average).toFixed(4);
					var cie_y_value = parseFloat(cie_y_fact[0].fact_average).toFixed(4);
					cie_x_value = parseFloat(cie_x_value);
					cie_y_value = parseFloat(cie_y_value);
					
					series = {
						id: series_id,
						name: series_name,
						data: [[cie_x_value, cie_y_value]],
						type: 'scatter',
						marker: {
							fillColor: 'black'
						},
					};
					
					chart.addSeries(series);
					chartZoomed.addSeries(series);
				}
			}
		});
		</script>
		
		<div id="photometric-chart-max-slice" class="chart"></div>
		<?php echo $this->element('Charts/photometric-chart-max-slice', array(
			'filter' => array(
				'product_id' => $product['Product']['id'],
			),
			'id' => 'photometric-chart-max-slice'
		)); ?>
	<?php endif; ?>
	
	<?php if ($this->Permissions->check('SpectralTest')): ?>
		<div id="spectral-chart" class="wide-chart"></div>
		<?php echo $this->element('Charts/spectral-chart', array('id' => 'spectral-chart')); ?>
		
		<script>
		$.ajax({
			url: '<?php echo $this->Html->url(array(
				'controller' => 'spectral_test_data', 
				'action' => 'group_by_configuration',
				'product_id' => $product['Product']['id'])); ?>/.json',
			success: function(data)  {
			
				var chart = $('#spectral-chart').highcharts();
			
				// parse configurations
				var configurations = {};
				for (i in data.spectral_test_data) {
					var orientation = data.spectral_test_data[i].SpectralTestOrientationType;
					var housing = data.spectral_test_data[i].SpectralTestHousingType;
					
					var configuration = orientation.orientation_type;
					if (housing !== undefined && housing.housing_type !== null) {
						configuration += ' - ' + housing.housing_type;
					}
					configurations[configuration] = true;
					
					data.spectral_test_data[i].configuration = configuration;
				}
				configurations = $.map(configurations, function(val, key) { return key; });
				
				// create series
				var series = {};
				for (i in configurations) {
					series[configurations[i]] = {
						name: configurations[i],
						data: [],
						type: 'line',
						marker: {enabled: false},
					}; 
				}
				
				// append data
				for (i in data.spectral_test_data) {
					var point = data.spectral_test_data[i];
					var configuration = point.configuration;
					
					series[configuration].data.push(
						[parseInt(point.SpectralTestData.nm), parseFloat(point[0].result_average)]
					);
				}
				
				// sort data, then append
				for (i in series) {
					if (series[i].data.length > 0) {
						series[i].data.sort(function(a,b){ return a[0] - b[0]; });
						chart.addSeries(series[i]);
					}
				}
			},
			cache: false
		});
		</script>
	<?php endif; ?>
	
	<?php if ($this->Permissions->check('SpectralTest')): ?>
		<h3>Photometric Test</h3>
		<table id="spectral-fact-summary" ng-cloak>
			<tr>
				<th></th>
				<th>Testing Configuration</th>
				<th>Power (W)</th>
				<th>PF</th>
				<th>Sphere Output (lm)</th>
				<th>CCT (K)</th>
				<th>Chromaticity (u', v')</th>
				<th>CRI</th>
				<th>R9</th>
				<th>Efficacy (lm/W)</th>
			</tr>
			<tr ng-repeat="configuration in spectral_test_fact_configurations">
				<td>Average</td>
				<td>{{ configuration }}</td>
				<td>{{ getSpectralFact("Normal Power", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralFact("Normal Power Factor", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralFact("Output", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralFact("Temperature", configuration).toFixed(0) }}</td>
				<td>{{ getSpectralFact("u prime", configuration).toFixed(4) }}, {{ getSpectralFact("v prime", configuration).toFixed(4) }}</td>
				<td>{{ getSpectralFact("Ra", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralFact("R9", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralEfficacy(configuration).toFixed(2) }}</td>
			</tr>
			<tr>
				<td>Average</td>
				<td>All Configurations</td>
				<td>{{ getSpectralFact("Normal Power", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralFact("Normal Power Factor", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralFact("Output", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralFact("Temperature", "All Configurations").toFixed(0) }}</td>
				<td>{{ getSpectralFact("u prime", "All Configurations").toFixed(4) }}, {{ getSpectralFact("v prime", "All Configurations").toFixed(4) }}</td>
				<td>{{ getSpectralFact("Ra", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralFact("R9", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralEfficacy("All Configurations").toFixed(2) }}</td>
			</tr>
		</table>
		<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'spectral_tests', 'action' => 'find', 'product_id' => $product['Product']['id'] )); ?></small>
		<br /><br />
		
		<h3>Electrical Test</h3>
		<table id="electrical-summary-chart" ng-cloak>
			<tr>
				<th></th>
				<th>Testing Configuration</th>
				<th>Power (W)</th>
				<th>PF</th>
				<th>Voltage THD%</th>
				<th>Current THD%</th>
			</tr>
			<tr ng-repeat="configuration in spectral_test_fact_configurations">
				<td>Average</td>
				<td>{{ configuration }}</td>
				<td>{{ getSpectralFact("Normal Power", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralFact("Normal Power Factor", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralFact("UTHD%", configuration).toFixed(2) }}</td>
				<td>{{ getSpectralFact("ITHD%", configuration).toFixed(2) }}</td>
			</tr>
			<tr>
				<td>Average</td>
				<td>All Configurations</td>
				<td>{{ getSpectralFact("Normal Power", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralFact("Normal Power Factor", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralFact("UTHD%", "All Configurations").toFixed(2) }}</td>
				<td>{{ getSpectralFact("ITHD%", "All Configurations").toFixed(2) }}</td>
			</tr>
		</table>
		<span>* <small>All tests conducted at 120V</small></span>
		<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'spectral_tests', 'action' => 'find', 'product_id' => $product['Product']['id'] )); ?></small>
		<br /><br />
	<?php endif; ?>
	
	<?php if ($this->Permissions->check('PhotometricTest')): ?>
	<h3>Distribution Test - Base Up</h3>
	<table id="gonio-summary" ng-cloak>
		<tr>
			<th>Power (W)</th>
			<th>Gonio Output (lm)</th>
			<th>Efficacy (lm/W)</th>
		</tr>
		<tr>
			<td><?php if(isset($gonio_summary['Wattage'])) { echo sprintf('%1.2f', $gonio_summary['Wattage']); } else { echo "N/A"; }?></td>
			<td><?php if(isset($gonio_summary['Output'])) { echo sprintf('%1.2f', $gonio_summary['Output']); } else { echo "N/A"; }?></td>
			<td><?php if(isset($gonio_summary['Efficacy'])) { echo sprintf('%1.2f', $gonio_summary['Efficacy']); } else { echo "N/A"; }?></td>
		</tr>
	</table>
	<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'photometric_tests', 'action' => 'find', 'product_id' => $product['Product']['id'] )); ?></small>
	<br /><br />
	<?php endif; ?>
	
	<?php if ($this->Permissions->check('FlickerTest')): ?>
	<h3>Flicker Tests</h3>
	<div id="flicker-data-summary-fundamental-frequency">	
		<?php if(isset($flicker_data_summary_frequency)){ echo sprintf('Fundamantal Frequency (Hz): %s', $flicker_data_summary_frequency); } ?> 
		<br /><br />
	</div>
	
	<h4>Percent Flicker, Filter cutoff at 200Hz</h4>
	<table id="flicker-data-summary-percent-filtered">
		<tr>
			<th>% Operable Power<sup>*</sup></th>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
			<th><?php echo $dimmer; ?></th>
			<?php endforeach; ?>
		</tr>
		<?php foreach($flicker_data_summary_levels as $level): ?>
		<tr>
			<td><?php echo $level ?></td>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
			<td><?php if(isset($flicker_data_summary[$dimmer][$level]['200']['percent'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['200']['percent']); } else { echo 'N/A'; } ?></td>
			<?php endforeach; ?>		
		</tr>
		<?php endforeach; ?>
	</table>
	
	<h4>Flicker Index, Filter cutoff at 200Hz</h4>
	<table id="flicker-data-summary-index-filtered">
		<tr>
			<th>% Operable Power<sup>*</sup></th>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
			<th><?php echo $dimmer; ?></th>
			<?php endforeach; ?>
		</tr>
		<?php foreach($flicker_data_summary_levels as $level): ?>
		<tr>
			<td><?php echo $level ?></td>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
			<td><?php if(isset($flicker_data_summary[$dimmer][$level]['200']['index'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['200']['index']); } else { echo 'N/A'; } ?></td>
			<?php endforeach; ?>		
		</tr>
		<?php endforeach; ?>
	</table>
	
	<h4>Percent Flicker, Unfiltered</h4>
	<table id="flicker-data-summary-percent-unfiltered">
		<tr>
			<th>% Operable Power<sup>*</sup></th>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
			<th><?php echo $dimmer; ?></th>
			<?php endforeach; ?>
		</tr>
		<?php foreach($flicker_data_summary_levels as $level): ?>
		<tr>
			<td><?php echo $level ?></td>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
			<td><?php if(isset($flicker_data_summary[$dimmer][$level]['10000']['percent'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['10000']['percent']); } else { echo 'N/A'; } ?></td>
			<?php endforeach; ?>		
		</tr>
		<?php endforeach; ?>
	</table>
	
	<h4>Flicker Index, Unfiltered</h4>
	<table id="flicker-data-summary-index-unfiltered">
		<tr>
			<th>% Operable Power<sup>*</sup></th>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>
			<th><?php echo $dimmer; ?></th>
			<?php endforeach; ?>
		</tr>
		<?php foreach($flicker_data_summary_levels as $level): ?>
		<tr>
			<td><?php echo $level ?></td>
			<?php foreach($flicker_data_summary_dimmers as $dimmer): ?>		
			<td><?php if(isset($flicker_data_summary[$dimmer][$level]['10000']['index'])) { echo sprintf('%1.2f', $flicker_data_summary[$dimmer][$level]['10000']['index']); } else { echo 'N/A'; } ?></td>
			<?php endforeach; ?>		
		</tr>
		<?php endforeach; ?>
	</table>
	<small class="detail-link"><?php echo $this->Html->link('details', array('controller' => 'flicker_tests', 'action' => 'find', 'product_id' => $product['Product']['id'] )); ?></small>
	<span>* <small>% Operable Power is defined by the luminous output range of each lamp, with 100% power equaling the power of each lamp at its maximum luminous output, and Min power equaling the power of each lamp while at minimum luminous output. This power range is divided into quartiles to arrive at values for 75%, 50% and 25% power levels.</small></span>
	<?php endif; ?>
</div>
<?php echo $this->Html->script('controller/products/products_view.js'); ?>
