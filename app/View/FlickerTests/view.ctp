<!-- app/View/FlickerTests/view.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Tests', '/flicker_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Flicker Test</h2>
<table>
	<tr>
		<th>Filter Frequency</th>
		<th>Flicker Index</th>
		<th>Flicker Percent</th>
		<th>Fundamental Frequency</th>	
	</tr>
	
	<?php foreach ($test['FlickerTestData'] as $data): ?>
	<tr>
		<td><?php echo $data['filter_frequency']; ?></td>
		<td><?php echo $data['flicker_index']; ?></td>
		<td><?php echo $data['flicker_percent']; ?></td>
		<td><?php echo $data['fundamental_frequency']; ?></td>
	</tr>
	<?php endforeach; ?>
	<?php unset($data); ?>	
</table>