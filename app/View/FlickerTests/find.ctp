<!-- app/View/FlickerTests/find.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Tests', '/flicker_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Flicker Tests</h2>

<?php echo $this->Form->create('FlickerTest', array(
	'url' => array_merge(array('action' => 'find'), $this->request['pass']))); ?>
	<fieldset>
		<?php
			echo $this->Form->input('product_id', array('type' => 'select', 'options' => $Products, 'empty' => ''));
			echo $this->Form->input('sample_name');
			echo $this->Form->input('test_datetime', array('type' => 'text', 'class' => 'datepicker'));
			echo $this->Form->input('dimmer_id', array('type' => 'select', 'options' => $Dimmers, 'empty' => ''));
			echo $this->Form->input('dimmer_level');
			echo $this->Form->submit('Search', array('name' => 'data[FlickerTest][submit_search]')); #formhelper doesn't auto add data[Form] to submit
		?>
	</fieldset>
	<fieldset>
		<?php
			echo $this->Form->input('report', array('type' => 'select', 'options' => $reportTypes, 'empty' => ''));
			echo $this->Form->submit('Generate Report', array('name' => 'data[FlickerTest][submit_report]')); #formhelper doesn't auto add data[Form] to submit
		?>
	</fieldset>
<?php echo $this->Form->end(); ?>

<table>
    <tr>
    	<th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
        <th>Dimmer</th>
        <th>Dimmer Level</th>
    </tr>

    <?php foreach ($tests as $test): ?>
    <tr>
    	<td><?php echo $this->Html->link($test['FlickerTest']['id'], array('controller' => 'flicker_tests', 'action' => 'view', $test['FlickerTest']['id'])); ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['FlickerTest']['test_datetime']; ?></td>
        <td><?php echo $test['Dimmer']['name']; ?></td>
        <td><?php echo $test['FlickerTest']['dimmer_level']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($test); ?>
</table>

<?php 
	echo $this->element('paginator', array('list' => $tests)); 
?>