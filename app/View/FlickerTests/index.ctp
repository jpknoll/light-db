<!-- app/View/FlickerTests/index.ctp -->

<?php 
	$this->Html->addCrumb('Flicker Tests', '/flicker_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Flicker Tests</h2>
<?php echo $this->Html->link('Search/Filter', array('controller' => 'flicker_tests', 'action' => 'find')); ?>
<table>
    <tr>
    	<th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
        <th>Dimmer</th>
        <th>Dimmer Level</th>
    </tr>

    <?php foreach ($tests as $test): ?>
    <tr>
    	<td><?php echo $this->Html->link($test['FlickerTest']['id'], array('controller' => 'flicker_tests', 'action' => 'view', $test['FlickerTest']['id'])); ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['FlickerTest']['test_datetime']; ?></td>
        <td><?php echo $test['Dimmer']['name']; ?></td>
        <td><?php echo $test['FlickerTest']['dimmer_level']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($test); ?>
</table>

<?php 
if(!empty($tests)){ 
    echo $this->Paginator->first(__('First', true), array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->prev('� Back', null, null, array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->numbers();
    echo " ";
    echo $this->Paginator->next('Next �', null, null, array('class' => 'disabled'));
    echo " ";
    echo $this->Paginator->last(__('Last', true), array('class' => 'disabled'));
} ?>

<?php 
	$this->Paginator->options(array('url' => $this->passedArgs));
?>