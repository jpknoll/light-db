<!-- app/View/Admin/index.ctp -->
<ul>
	<li><?php echo $this->Html->link('Roles', array('controller' => 'groups', 'action' => 'index')); ?></li>
	<li><?php echo $this->Html->link('Users', array('controller' => 'users', 'action' => 'admin_index')); ?></li>
	<li><?php echo $this->Html->link('Controls', array('controller' => 'permissions', 'action' => 'index')); ?></li>
</ul>