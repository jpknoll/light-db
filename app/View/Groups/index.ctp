<!-- app/View/Groups/index.ctp -->

<table>
<thead>
	<tr>
		<th>ID</th>
		<th>Role</th>
		<th></th>
	</tr>
</thead>
<tbody>
<?php foreach($groups as $group): ?>
	<tr>
		<td><?php echo $group['Group']['id']; ?></td>
		<td><?php echo $this->Html->link($group['Group']['groupname'], array('controller' => 'groups', 'action' => 'edit', $group['Group']['id'])) ?></td>
		<td><?php echo $this->Html->link('Delete', array('controller' => 'groups', 'action' => 'delete', $group['Group']['id'])) ?></td>
	</tr>
<?php endforeach; ?>
<?php unset($group); ?>
</tbody>
</table>

<?php echo $this->Html->link('New Group', array('action' => 'add')); ?>
