<!-- app/View/Groups/edit.ctp -->

<h2>Create new group</h2>

<?php echo $this->Form->create('Permissions'); ?>
<fieldset>
<?php echo $this->Form->input('Group.groupname', array('label' => 'Group Name')); ?>
<table>
	<thead>
		<tr>
			<th></th>
			<th>read</th>
			<th>write</th>
			<th>create</th>
			<th>delete</th>
		</tr>
	</thead>
	<tbody>
	<?php for($i = 0; $i < count($controls); $i++): ?>
		<tr>
			<td>
				<?php echo $controls[$i]['Aco']['alias']?>
				<?php echo $this->Form->input("Permission.$i.aco_id", array('type' => 'hidden', 'value' => $controls[$i]['Aco']['id'])); ?>				
			</td>
			<td><?php echo $this->Form->checkbox("Permission.$i._read",   array('label' => false, 'div' => false)); ?></td>
			<td><?php echo $this->Form->checkbox("Permission.$i._update", array('label' => false, 'div' => false)); ?></td>
			<td><?php echo $this->Form->checkbox("Permission.$i._create", array('label' => false, 'div' => false)); ?></td>
			<td><?php echo $this->Form->checkbox("Permission.$i._delete", array('label' => false, 'div' => false)); ?></td>
		</tr>
	<?php endfor; ?>
	</tbody>
</table>

<?php echo $this->Form->submit(__('Submit', true), array('name' => 'ok', 'div' => false)); ?>
<?php echo $this->Form->submit(__('Cancel', true), array('name' => 'cancel','div' => false)); ?>
</fieldset>
<?php echo $this->Form->end(); ?>
