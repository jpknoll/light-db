<!-- app/View/Groups/edit.ctp -->

<h2><?php echo $group['Group']['groupname']; ?></h2>

<?php echo $this->Form->create('Permissions'); ?>
<fieldset>
<table>
	<thead>
		<tr>
			<th></th>
			<th>read</th>
			<th>write</th>
			<th>create</th>
			<th>delete</th>
		</tr>
	</thead>
	<tbody>
	<?php for($i = 0; $i < count($controls); $i++): ?>
		<tr>
			<?php 
				unset($match);
				foreach ($permissions as $permission) {
					if ($permission['Permission']['aco_id'] == $controls[$i]['Aco']['id']) {
						$match = $permission['Permission'];
					} 
				}
				if (!isset($match)) {
					$match = array(
						'id' => 0,
						'_create' => 0,
						'_read' => 0,
						'_update' => 0,
						'_delete' => 0,
					);
				}
			?>
			<td>
				<?php echo $controls[$i]['Aco']['alias']?>
				<?php echo $this->Form->input("Permission.$i.id", array('type' => 'hidden', 'value' => $match['id'])); ?>
				<?php echo $this->Form->input("Permission.$i.aro_id", array('type' => 'hidden', 'value' => $role['Aro']['id'])); ?>	
				<?php echo $this->Form->input("Permission.$i.aco_id", array('type' => 'hidden', 'value' => $controls[$i]['Aco']['id'])); ?>				
			</td>
			<td><?php echo $this->Form->checkbox("Permission.$i._read",   array('label' => false, 'div' => false, 'checked' => $match['_read'])); ?></td>
			<td><?php echo $this->Form->checkbox("Permission.$i._update", array('label' => false, 'div' => false, 'checked' => $match['_update'])); ?></td>
			<td><?php echo $this->Form->checkbox("Permission.$i._create", array('label' => false, 'div' => false, 'checked' => $match['_create'])); ?></td>
			<td><?php echo $this->Form->checkbox("Permission.$i._delete", array('label' => false, 'div' => false, 'checked' => $match['_delete'])); ?></td>
		</tr>
	<?php endfor; ?>
	</tbody>
</table>

<?php echo $this->Form->submit(__('Submit', true), array('name' => 'ok', 'div' => false)); ?>
<?php echo $this->Form->submit(__('Cancel', true), array('name' => 'cancel','div' => false)); ?>
</fieldset>
<?php echo $this->Form->end(); ?>
