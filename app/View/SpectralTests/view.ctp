<!-- app/View/SpectralTests/view.ctp -->

<?php 
	$this->Html->addCrumb('Spectral Tests', '/spectral_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Spectral Test</h2>
<small class="edit-link"><?php echo $this->Html->link('edit', array('controller' => 'spectral_tests', 'action' => 'edit', $test['SpectralTest']['id'])); ?></small>
<div id="spectral-chart" style="width:100%; height:400px;"></div>
<?php echo $this->element('Charts/spectral-chart', array('id' => 'spectral-chart')); ?>
<script>
$.ajax({
	url: '<?php echo $this->Html->url(array(
		'controller' => 'spectral_test_data', 
		'action' => 'find',
		'spectral_test_id' => $test['SpectralTest']['id'])); ?>/.json',
	success: function(data)  {
	
		var chart = $('#spectral-chart').highcharts();
			
		// create series
		var series  = {
			name: '<?php echo $test['SpectralTest']['id'] ?>',
			data: [],
			type: 'line',
			marker: {enabled: false},
		}; 
		
		// append data
		for (i in data.spectral_test_data) {
			var point = data.spectral_test_data[i].SpectralTestData;
			series.data.push(
				[parseInt(point.nm), parseFloat(point.result)]
			);
		}
		
		// sort data, then append
		if (series.data.length > 0) {
			series.data.sort(function(a,b){ return a[0] - b[0]; });
			chart.addSeries(series);
		}
	},
	cache: false
});
</script>
<table>
    <tr>
        <th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
        <th>Orientation</th>
        
        <?php foreach ($facts as $fact): ?>
    	<th><?php echo $fact['SpectralTestFactType']['fact_type'];  ?></th>
    	<?php endforeach; ?>
    	<?php unset($fact); ?>
    	
    	<?php foreach ($data as $value): ?>
    	<th><?php echo $value['SpectralTestData']['nm']; ?></th>
    	<?php endforeach; ?>
    	<?php unset($value); ?>
    </tr>

    <tr>
        <td><?php echo $test['SpectralTest']['id']; ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['SpectralTest']['test_datetime']; ?></td>
        <td><?php echo $test['SpectralTestOrientationType']['orientation_type']; ?></td>
        
        <?php foreach ($facts as $fact): ?>
    	<td><?php echo $fact['SpectralTestFact']['fact'];  ?></td>
    	<?php endforeach; ?>
    	<?php unset($fact); ?>
    	
    	<?php foreach ($data as $value): ?>
    	<td><?php echo $value['SpectralTestData']['result']; ?></td>
    	<?php endforeach; ?>
    	<?php unset($value); ?> 	
    </tr>
</table>
