<!-- app/View/SpectralTests/report_chromatic_chart.ctp -->

<?php 
	$this->Html->addCrumb('Spectral Tests', '/spectral_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<div id="chromaticity-chart" style="width:400px; height:400px; float:left;"></div>
<?php echo $this->element('Charts/chromatic-chart', array('id' => 'chromaticity-chart')); ?>

<div id="chromaticity-chart-zoomed" style="width:400px; height:400px; float:left;"></div>
<?php echo $this->element('Charts/chromatic-chart-zoomed', array('id' => 'chromaticity-chart-zoomed')); ?>

<table id="chromaticity-table">
	<tr>
		<th><input type="checkbox" class="chromaticity-chart-series-selector-all" checked="true">Test Id</th>
		<th>Sample</th>
		<th>Orientation</th>
		<th>CIE x</th>
		<th>CIE y</th>
	</tr>
	<?php foreach($tests as $id => $test): ?>
	<tr id="chromaticity-test-<?php echo $test['SpectralTest']['id']; ?>">
		<td><input type="checkbox" class="chromaticity-chart-series-selector" checked="checked" data-test-id="<?php echo $test['SpectralTest']['id']; ?>"><?php echo $test['SpectralTest']['id']; ?></td>
		<td><?php echo $test['SpectralTest']['Sample']['public_name']; ?></td>
		<td><?php echo $test['SpectralTest']['SpectralTestOrientationType']['orientation_type']; ?></td>
		<td><?php echo $test['CIE x']; ?></td>
		<td><?php echo $test['CIE y']; ?></td>
	</tr>
	<?php endforeach; ?>
</table>

<script language="JavaScript">
	$(document).ready(function() {
		$('.chromaticity-chart-series-selector-all').click(function() {
			var checked = $(this).is(':checked');
			$('.chromaticity-chart-series-selector').each(function() {
				if (this.checked != checked) {
					this.click();
				}
			})
		});
	
		$('.chromaticity-chart-series-selector').click(function() {
			var checked = $(this).is(':checked');
			var id = $(this).attr('data-test-id');
			var chart = $('#chromaticity-chart-zoomed').highcharts();
			
			if (checked) {
				chart.get(id).show();
			}
			else {
				chart.get(id).hide();
			}
		});
	});
	
	$.ajax({
		url: '<?php echo $this->Html->url(array(
				'controller' => 'SpectralTestFacts', 
				'action' => 'find', 
				$this->passedArgs)); ?>/.json',
		success: function(data) {			
			var chart = $('#chromaticity-chart').highcharts(); 
			var chartZoomed = $('#chromaticity-chart-zoomed').highcharts();
			
			// fetch coordinates
			var cie_x_facts = data.spectral_test_facts.filter(function(fact) {
				return fact.SpectralTestFactType.fact_type == 'CIE x';
			});
			var cie_y_facts = data.spectral_test_facts.filter(function(fact) {
				return fact.SpectralTestFactType.fact_type == 'CIE y';
			});
			
			// cie coordinates
			for(i in cie_x_facts) {	
				// find corresponding y
				var cie_x_fact = cie_x_facts[i];
				var less_cie_y_facts = cie_y_facts.filter(function(fact) { return fact.Product.id = cie_x_fact.Product.id });
				if (less_cie_y_facts.length == 0) {
					continue;
				}
				var cie_y_fact = less_cie_y_facts[0];
				
				var series_id = cie_x_fact.Product.id;
				var series_name = cie_x_fact.Product.public_name;
				
				// round off to 4 digits as string, then back to float!
				var cie_x_value = parseFloat(cie_x_fact[0].fact_average).toFixed(4);
				var cie_y_value = parseFloat(cie_y_fact[0].fact_average).toFixed(4);
				cie_x_value = parseFloat(cie_x_value);
				cie_y_value = parseFloat(cie_y_value);
				
				series = {
					id: series_id,
					name: series_name,
					data: [[cie_x_value, cie_y_value]],
					type: 'scatter',
					marker: {
						fillColor: 'black'
					},
				};
				
				chart.addSeries(series);
				chartZoomed.addSeries(series);
			}
		}
	});
</script>
