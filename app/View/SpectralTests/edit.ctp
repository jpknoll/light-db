<!-- app/View/SpectralTests/edit.ctp -->

<?php 
	$this->Html->addCrumb('Spectral Tests', '/spectral_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Spectral Test</h2>
<?php 
	echo $this->Form->create('SpectralTest', array(
		'type' => 'post',
		'action' => 'edit',
	));
	
	echo $this->Form->input('SpectralTest.id', array('type' => 'hidden'));
	echo $this->Form->input('SpectralTest.test_datetime');
	echo $this->Form->input('SpectralTest.spectral_test_orientation_type_id', array('options' => $orientations, 'empty' => ''));
	echo $this->Form->input('SpectralTest.spectral_test_housing_type_id', array('options' => $housings, 'empty' => ''));

	echo $this->Form->submit('Submit', array('action' => 'edit', 'id' => 'submit-form'));
	echo $this->Form->submit('Cancel', array('action' => 'edit', 'id' => 'cancel-form'));
	echo $this->Form->end(); 
?>

<h2>Facts</h2>
<table id="SpectralTestFacts">
	<thead>
		<tr>
			<th></th>
			<th>Spectral Fact Type</th>
			<th>Value</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php for ($i = 0; $i < count($this->request->data['SpectralTestFacts']); $i++): ?>
		<tr id="<?php echo "SpectralTestFacts{$i}Row"; ?>">
			<td><?php echo $this->Form->input("SpectralTestFacts.{$i}.id", array('type' => 'hidden')); ?></td>
			<td><?php echo $this->Form->input("SpectralTestFacts.{$i}.spectral_test_fact_type_id", array(
				'label' => false, 
				'class' => 'spectral-test-fact-input',
				'options' => $spectral_test_fact_types, 
				'empty' => '')); ?></td>
			<td><?php echo $this->Form->input("SpectralTestFacts.{$i}.fact", array(
				'label' => false, 
				'class' => 'spectral-test-fact-input')); ?></td>
			<td><?php echo $this->Form->submit('Delete', array(
				'id' => "ProductFacts{$i}Delete", 
				'action' => 'delete', 
				'class' => 'delete-fact', 
				'onclick' => 'deleteFact(this);')); ?></td>
			<td><?php echo $this->Html->image('ajax.gif', array('id' => "SpectralTestFacts{$i}Ajax", 'style' => 'display:none')); ?></td>	
		</tr>
		<?php endfor; ?>
		
		<tr id="<?php echo "SpectralTestFacts{$i}Row"; ?>">
			<td><?php echo $this->Form->input("SpectralTestFacts.{$i}.id", array('type' => 'hidden')); ?></td>
			<td><?php echo $this->Form->input("SpectralTestFacts.{$i}.spectral_test_fact_type_id", array(
				'label' => false, 
				'options' => $spectral_test_fact_types, 
				'empty' => '')); ?></td>
			<td><?php echo $this->Form->input("SpectralTestFacts.{$i}.fact", array('label' => false)); ?></td>
			<td><?php echo $this->Form->submit('Add', array(
				'id' => "ProductFacts{$i}Add", 
				'action' => 'add', 
				'class' => 'add-fact', 
				'onclick' => 'addFact(this);')); ?></td>	
			<td><?php echo $this->Html->image('ajax.gif', array('id' => "SpectralTestFacts{$i}Ajax", 'style' => 'display:none')); ?></td>
		</tr>
	</tbody>
</table>

<h2>Data</h2>

<script>
	$('.spectral-test-fact-input').change(function(sender) {
		editFact(this);
	});

	function editFact(sender) {
		// strip out the iterator value
		var i = sender.id.replace('SpectralTestFacts', '').replace('SpectralTestFactTypeId', '').replace('Fact', '');
		
		// find the product fact id
		var id = $('#SpectralTestFacts' + i + 'Id').val();
		
		// fetch data
		var spectral_test_fact_type_id = $('#SpectralTestFacts' + i + 'SpectralTestFactTypeId').val();
		var fact = $('#SpectralTestFacts' + i + 'Fact').val();
		
		// build post
		var data = {
			SpectralTestFact: {
				id: id,
				spectral_test_fact_type_id: spectral_test_fact_type_id,
				fact: fact,
			}
		};
		
		// show ajax gif
		$('#SpectralTestFacts' + i + 'Ajax').show();
		
		// submit post
		$.post('/spectral_test_facts/edit/' + id, data, function(data, textStatus, jqXHR) {
			// hide ajax gif
			$('#SpectralTestFacts' + i + 'Ajax').hide();
		});
	}
</script>