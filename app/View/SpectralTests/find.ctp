<!-- app/View/SpectralTests/find.ctp -->

<?php 
	$this->Html->addCrumb('Spectral Tests', '/spectral_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Spectral Tests</h2>

<?php echo $this->Form->create('SpectralTest', array(
	'url' => array_merge(array('action' => 'find'), $this->request['pass']))); ?>
	<fieldset>
		<?php
			echo $this->Form->input('product_id', array('type' => 'select', 'options' => $Products, 'empty' => ''));
			echo $this->Form->input('sample_name');
			echo $this->Form->input('test_datetime', array('type' => 'text', 'class' => 'datepicker'));
			echo $this->Form->input('spectral_test_orientation_type_id', array('type' => 'select', 'options' => $SpectralTestOrientationTypes, 'empty' => ''));
			echo $this->Form->submit('Search', array('name' => 'data[SpectralTest][submit_search]')); #formhelper doesn't auto add data[Form] to submit
		?>
	</fieldset>
	<fieldset>
		<?php
			echo $this->Form->input('report', array('type' => 'select', 'options' => $reportTypes, 'empty' => ''));
			echo $this->Form->submit('Generate Report', array('name' => 'data[SpectralTest][submit_report]')); #formhelper doesn't auto add data[Form] to submit
		?>
	</fieldset>
<?php echo $this->Form->end(); ?>
<table>
    <tr>
    	<th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
        <th>Orientation</th>
    </tr>

    <?php foreach ($tests as $test): ?>
    <tr>
    	<td><?php echo $this->Html->link($test['SpectralTest']['id'], array('controller' => 'spectral_tests', 'action' => 'view', $test['SpectralTest']['id'])); ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['SpectralTest']['test_datetime']; ?></td>
        <td><?php echo $test['SpectralTestOrientationType']['orientation_type']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($test); ?>
</table>

<?php 
	echo $this->element('paginator', array('list' => $tests)); 
?>