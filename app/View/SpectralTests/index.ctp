<!-- app/View/SpectralTests/index.ctp -->

<?php 
	$this->Html->addCrumb('Spectral Tests', '/spectral_tests');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Spectral Tests</h2>
<?php echo $this->Html->link('Search/Filter', array('controller' => 'spectral_tests', 'action' => 'find')); ?>

<table>
    <tr>
    	<th>Id</th>
        <th>Sample Name</th>
        <th>Date Time</th>
        <th>Orientation</th>
    </tr>

    <?php foreach ($spectral_tests as $test): ?>
    <tr>
    	<td><?php echo $this->Html->link($test['SpectralTest']['id'], array('controller' => 'spectral_tests', 'action' => 'view', $test['SpectralTest']['id'])); ?></td>
        <td><?php echo $test['Sample']['public_name']; ?></td>
        <td><?php echo $test['SpectralTest']['test_datetime']; ?></td>
        <td><?php echo $test['SpectralTestOrientationType']['orientation_type']; ?></td>
    </tr>
    <?php endforeach; ?>
</table>

<?php 
	echo $this->element('paginator', array('list' => $spectral_tests));
?>