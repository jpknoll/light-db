<!-- app/View/pages/home.ctp -->

<h2></h2>
<div style="margin-left:10%; margin-right:10%; max-width: 1180px">
	<span>This database provides public access to LED lamp test results. It allows users to compare ongoing, third-party testing performed by CLTC to data provided by lighting product manufacturers.  Users can query and retrieve results based on access rights granted to each user type.</span>
	<br /><br />
	<?php if ($this->Session->check('Auth.User')): ?>
		<?php echo $this->Html->link('Products List', array('controller' => 'products', 'action' => 'index')); ?>
		<br /><br />
		<?php echo $this->Html->link('Standards & Labels', array('controller' => 'pages', 'action' => 'standards')); ?>
		<br /><br />
	<?php else: ?>
		You will need to log in or register to continue:
		<br /><br />
		<?php echo $this->element('usercp', array(null)); ?>
		<br /><br />
	<?php endif; ?>
	<?php echo $this->Html->image('cltc-integrating-sphere.png', array('alt' => 'cltc-integrating-sphere', 'style' => 'width: 100%; border: solid black 1px;')); ?>
</div>