<!-- app/View/pages/standards.ctp -->

<?php 
	$this->Html->addCrumb('Standards & Labels', '/pages/standards');
	$this->Html->addCrumb('Back', 'javascript:window.history.back()');
?>

<h2>Standards & Labels</h2>

<div style="margin-left:10%; margin-right:10%;">
	<section>
	<table>
	    <thead>
	        <tr>
	            <td/>
	            <th>Energy Star v1 Final Spec</th>
	            <th>Voluntary California Quality LED Lamp Spec</th>
	        </tr>
        </thead>
        <tbody>
	        <tr>
	            <td>CCT</td>
	            <td>2700K, 3000K, 3500K, 4000/4100K, 5000K, 6500K (w/in 7 MCAE from designated CCT)</td>
	            <td>2700K, 3000K within 4 MCAE</td>
	        </tr>
	        <tr>
	            <td>Efficacy (Omni &lt;15/&gt;15 watts)</td>
	            <td>55/65</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Efficacy (Directional &lt;20/&gt;20 watts)</td>
	            <td>40/50</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Elevated Temp Light Output</td>
	            <td>Maintain &le;90% initial rated ouput</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Luminous Intensity Distributions (Omnidirectional)</td>
	            <td>Lamp shall vary by no more than 25% from average of all measured values (no less than 5% total flux shall be emitted in 135&deg;-180&deg; zone)</td>
	            <td>Per Energy Star V. 1 Draft 2</td>
	        </tr>
	        <tr>
	            <td>Color Rendering</td>
	            <td>(Ra) &ge;80<br />R9 &gt; 0</td>
	            <td>(Ra) &ge;90<br />R9 &gt; 50</td>
	        </tr>
	        <tr>
	            <td>Color Maintanence</td>
	            <td>During first 6,000 hours shall be within total distance of 0.007 on the CIE 1976 u'v' diagram</td>
	            <td>During first 6,000 hours shall be within total distance of 0.007 on the CIE 1976 u'v' diagram</td>
	        </tr>
	        <tr>
	            <td>Color Angular Uniformity (Directional Lamps</td>
	            <td>Variation of chromaticity across the beam angle of the lamp shall be within a total distance of 0.006 from the weighted average point on the CIE 1976 (u'v') diagram</td>
	            <td>Variation of chromaticity across the beam angle of the lamp shall be within a total distance of 0.006 from the weighted average point on the CIE 1976 (u'v') diagram</td>
	        </tr>
	        <tr>
	            <td>Lumen Maintenance</td>
	            <td>See image to the right</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Rated Life</td>
	            <td>Decorative lamps &ge; 15,000 hrs All other lamps &ge; 25,000 hrs 90% operational at 6,000 hrs All lamps operational at 3,000 hrs</td>
	            <td>Per Energy Star requirements</td>
	        </tr>
	        <tr>
	            <td>Rapid Cycle Stress Test</td>
	            <td>Lamp, when cycled at 2 minutes on, 2 minutes off, of cycles or 5 minutes on 5 minutes off, shall survive the lesser number of cycles: one cycle per hour of rated life or 15,000 cycles</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Electrical Safety</td>
	            <td>Shall comply with: ANSI/UL 1993-2012 ANSI/UL 8750-2009</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Power Factor</td>
	            <td>&ge; 0.7</td>
	            <td>&ge; 0.9</td>
	        </tr>
	        <tr>
	            <td>Frequency</td>
	            <td>&ge; 120Hz</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Start Time</td>
	            <td>Within one second of application of electrical power</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Transient protection (Line Voltage)</td>
	            <td>Lamp shall survive 7 strikes of a 100 kHz ring wave, 2.5 kV level. All units shall be fully operational at the completion of testing</td>
	            <td>N/A</td>
	        </tr>
	        <tr>
	            <td>Maximum Light Output</td>
	            <td>Lamp light output on the maximum setting of a dimmer/control shall not fall below the lamp's baseline light output when operated without a dimmer by more than 20%</td>
	            <td>Lamp light output on the maximum setting of a dimmer/control shall not fall below the lamp's baseline light output when operated without a dimmer by more than 20%</td>
	        </tr>
	        <tr>
	            <td>Minimum Light Output</td>
	            <td>Lamp light output on a dimmer/control shall be no more than 20% of the maximum light output of the lamp on each tested dimmer/control</td>
	            <td>Lamp light output on a dimmer/control shall be no more than 20% of the maximum light output of the lamp on each tested dimmer/control</td>
	        </tr>
	        <tr>
	            <td>Flicker</td>
	            <td>Lamp average light output periodic frequency, highest percent flicker, and highest flicker index shall be reported</td>
	            <td>Lamps shall be free of flicker over full range of operation from 10%-100% ouput</td>
	        </tr>
	        <tr>
	            <td>Audible Noise</td>
	            <td>Lamp shall not emit noise above 24dBA at 1 meter or less</td>
	            <td>Lamps shall be free of noise over full range of operation from 10%-100% output</td>
	        </tr>
	        <tr>
	            <td>Toxics Reduction</td>
	            <td>&le; 23.0 watts shall contain &le; 2.5 mg mercury per lamp &gt; 23.0 watts shall contain &le; 3.0 mg mercury per lamp</td>
	            <td>N/A Must meet Title 20 and ROHS standards however</td>
	        </tr>
	        <tr>
	            <td>Labeling</td>
	            <td>Energy Star, Retail SKU #, CCT, Wattage, lumen output, beam angle, application exception language</td>
	            <td>Energy Star, Retail SKU #, CCT, Wattage, lumen output, beam angle, application exception language, plus dimming compatibility</td>
	        </tr>
	        <tr>
	            <td>Warranty</td>
	            <td>&lt; 15,000 lamp life 2 year minimum warranty &ge; 15,000 lamp life 3 year minimum warranty</td>
	            <td>Minimum 5 year warranty</td>
	        </tr>
	    </tbody>
	</table>
	</section>
	<section>
	<p>Lamp shall maintain minimum percentage of 0-hour light output after completion of the 6000-hr test duration per the table(s) below. The reported values shall be the average lumen maintenance of &ge; 9 surviving units and shall meet the minimum requirement for the designated life claim. Lamp may earn optional early interim certification after 3,000 hours, with a rated life claim &le; 25,000 hours, per the provisions below.</p>
	<table>
		<thead>
			<tr>
				<th>Maximum Life Claim (hours to L?)</th>
				<th>Minimum Lumen Maintenance After Test Duration</th>
				<th>Status After Completion of Test Duration</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>15,000</td>
				<td>86.7%</td>
				<td rowspan="3">Final certification testing completed.</td>
			</tr>
			<tr>
				<td>20,000</td>
				<td>89.9%</td>
			</tr>
			<tr>
				<td>25,000</td>
				<td>91.8%</td>
			</tr>
			<tr>
				<td>30,000</td>
				<td>93.1%</td>
				<td rowspan="5">Interim certification; continue testing per below.</td>
			</tr>
			<tr>
				<td>35,000</td>
				<td>94.1%</td>
			</tr>
			<tr>
				<td>40,000</td>
				<td>94.8%</td>
			</tr>
			<tr>
				<td>45,000</td>
				<td>95.4%</td>
			</tr>
			<tr>
				<td>50,000</td>
				<td>95.8%</td>
			</tr>
		</tbody>
	</table>
	<p>
		<strong>For Extended Lifetime Claims:</strong>
		<br />
		For lamp life claims &ge; 25,000 hours, lamp shall maintain &ge; 91.5% of 0-hour light output after completion of the test duration corresponsding to lamp's life claim per the table below.
	</p>
	<table>
		<thead>
			<tr>
				<th>Maximum Life Claim (hours to L?)</th>
				<th>Test Duration (hours) *</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>30,000</td>
				<td>7,500</td>
			</tr>
			<tr>
				<td>35,000</td>
				<td>8,750</td>
			</tr>
			<tr>
				<td>40,000</td>
				<td>10,000</td>
			</tr>
			<tr>
				<td>45,000</td>
				<td>11,250</td>
			</tr>
			<tr>
				<td>50,000</td>
				<td>12,500</td>
			</tr>
		</tbody>
	</table>
	</section>
	<section>
		<p><strong>Lighting Facts Label</strong></p>
		<?php echo $this->Html->image('lighting-facts-label.png', array('alt' => 'lighting-facts-label', 'style' => 'width: 100%; border: solid black 1px;')); ?>
	</section>
</div>