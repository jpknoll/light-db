<?php
class PermissionsHelper extends AppHelper {
    
    var $helpers = array('Session');
    
    function check($path){
        if($this->Session->check('Auth.Permissions.'.$path)) {
            return true;
        }
        return false;
    }
}