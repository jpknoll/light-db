<?php

class FlickerUploadsController extends AppController {
	public $helpers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	    'Search.Prg'
	);

	public function index() {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload', 'read')) {
			die('you are not authorized');
		}
	}

	public function view($id = null) {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload', 'read')) {
			die('you are not authorized');
		}
		
		// fetch flicker uploads
		$options = array(
			'conditions' => array(
				'id' => $id,
			),
			'contain' => array(
				'FlickerUploadAttachments',
				'FlickerUploadFacts',
			),
		);
		
		$flicker_upload = $this->FlickerUpload->find('first', $options);
		
		$this->set('flicker_upload', $flicker_upload);
		$this->set('_serialize', array('flicker_upload'));
	}

	public function delete($id = null) {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload', 'delete')) {
			die('you are not authorized');
		}
	}

	public function parse() {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload', 'create')) {
			die('you are not authorized');
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			$datasource = $this->FlickerUpload->getDataSource();
			$datasource->begin();
			$fail = false;
			
            $this->FlickerUpload->create();
			$this->FlickerUpload->set('user_id', $this->Session->read('Auth.User.id'));
			$this->FlickerUpload->save();
		
			$this->loadModel('FlickerUploadAttachment');
			$this->FlickerUploadAttachment->create();
			$this->FlickerUploadAttachment->set('flicker_upload_id', $this->FlickerUpload->getID());
			if (!$this->FlickerUploadAttachment->save($this->request->data['FlickerUploadAttachment'])) {
				$fail = true;
			}
			
			$this->loadModel('FlickerUploadData');
			foreach($this->request->data['FlickerUploadData'] as $flicker_upload_data) {
				$this->FlickerUploadData->create();
				$this->FlickerUploadData->set('flicker_upload_id', $this->FlickerUpload->getID());
				if (!$this->FlickerUploadData->save($flicker_upload_data)) {
					$fail = true;
				}
			}
			
			if (isset($this->request->data['FlickerUploadFact'])) {
				$this->loadModel('FlickerUploadFact');
				foreach($this->request->data['FlickerUploadFact'] as $flicker_upload_fact) {
					$this->FlickerUploadFact->create();
					$this->FlickerUploadFact->set('flicker_upload_id', $this->FlickerUpload->getID());
					if (!$this->FlickerUploadFact->save($flicker_upload_fact)) {
						$fail = true;
					}
				}
			}
			
			if (!$fail) {
            $this->Session->setFlash(__('Your upload has been recieved.'));
			$datasource->commit();
			return $this->redirect(array('action' => 'index'));
			}
			else {
	            $this->Session->setFlash(__('Unable to add your upload.'));
				$datasource->rollback();
			}
		}
	}
	
	public function admin() {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload.admin', 'read')) {
			die('you are not authorized');
		}
		
		// fetch flicker uploads
		$options = array(
			'contain' => array(
				'FlickerUploadAttachments',
				'FlickerUploadFacts',
				'FlickerUploadData',
				'User' => array(
					'username'
				),
			),
		);
		
		$flicker_uploads = $this->FlickerUpload->find('all', $options);
		
		$this->set('flicker_uploads', $flicker_uploads);
		$this->set('_serialize', array('flicker_uploads'));
	}
	
	public function admin_view($id = null) {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload.admin', 'read')) {
			die('you are not authorized');
		}

		// fetch flicker upload
		$options = array(
			'conditions' => array(
				'FlickerUpload.id' => $id,
			),
			'contain' => array(
				'FlickerUploadAttachments',
				'FlickerUploadFacts' => array(
					'FlickerUploadFactType',
				),
				'FlickerUploadData',
				'User' => array(
					'username'
				),
			),
		);
		
		$flicker_upload = $this->FlickerUpload->find('first', $options);
		
		$this->set('flicker_upload', $flicker_upload);
		$this->set('_serialize', array('flicker_upload'));
	}
	
	public function admin_release($id = null) {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload.admin', 'update')) {
			die('you are not authorized');
		}
	}
	
	public function admin_transfer($id = null) {
		// authenticate user role
		if (!$this->checkPermission('FlickerUpload.admin', 'update')) {
			die('you are not authorized');
		}
		
		// authenticate user role
		if (!$this->checkPermission('FlickerTest', 'create')) {
			die('you are not authorized');
		}
		
		// fetch flicker upload
		$options = array(
			'conditions' => array(
				'FlickerUpload.id' => $id,
			),
			'contain' => array(
				'FlickerUploadAttachments',
				'FlickerUploadFacts' => array(
					'FlickerUploadFactType',
				),
				'FlickerUploadData',
				'User' => array(
					'username'
				),
			),
		);
		
		$flicker_upload = $this->FlickerUpload->find('first', $options);
		$this->set('flicker_upload', $flicker_upload);
				
		// fetch product and sample dropdowns
		$this->loadModel('Product');
		$this->Product->displayField = 'public_name';
		$products = $this->Product->find('list');
		$this->set('products', $products);
		
		// fetch dimmers
		$this->loadModel('Dimmer');
		$this->Dimmer->displayField = 'name';
		$dimmers = $this->Dimmer->find('list');
		$this->set('dimmers', $dimmers);
		
		// fetch flicker test fact types
		$this->loadModel('FlickerTestFactType');
		$this->FlickerTestFactType->displayField = 'type';
		$flicker_test_fact_types = $this->FlickerTestFactType->find('list');
		$this->set('flicker_test_fact_types', $flicker_test_fact_types);
		
		if ($this->request->is('post') || $this->request->is('put')) {			
			// create flicker test
			$fail = false;
			$this->loadModel('FlickerTest');
			
			$datasource = $this->FlickerTest->getDataSource();
			$datasource->begin();
			
			$this->FlickerTest->create();
			if (!$this->FlickerTest->save($this->request->data['FlickerTest'])) {
				$fail = true;
			}
			
			// create flicker facts
			$this->loadModel('FlickerTestFact');
			foreach($this->request->data['FlickerTestFact'] as $fact) {
				if ($fact['value'] != '') {
					$this->FlickerTestFact->create();
					$this->FlickerTestFact->set('flicker_test_id', $this->FlickerTest->getID());
					if (!$this->FlickerTestFact->save($fact)) {
						$fail = true;
					}
				}
			}
			
			// create flicker data
			$this->loadModel('FlickerTestData');
			foreach($this->request->data['FlickerTestData'] as $result) {
				$this->FlickerTestData->create();
				$this->FlickerTestData->set('flicker_test_id', $this->FlickerTest->getID());
				if (!$this->FlickerTestData->save($result)) {
					$fail = true;
				}
			}
			
			if (!$fail) {
	            $this->Session->setFlash(__('Upload converted to Flicker Test.'));
				$datasource->commit();
				return $this->redirect(array('action' => 'index'));
				}
			else {
	            $this->Session->setFlash(__('Unable to convert upload.'));
				$datasource->rollback();
			}
		}
	}
}