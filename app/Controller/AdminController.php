<?php

// app/Controller/AdminController.php
class AdminController extends AppController {

	public $helpers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	);
	
	public function index() {		
		// authenticate user role
		if (!$this->checkPermission('Admin', 'read')) {
			die('you are not authorized');
		}
	}
	
	public function fix() {
		// authenticate user role
		if (!$this->checkPermission('Admin', 'read')) {
			die('you are not authorized');
		}
		
		$this->Acl->Aco->recover();
		$this->Acl->Aro->recover();
	}
	
	public function php_info() {
		// authenticate user role
		if (!$this->checkPermission('Admin', 'read')) {
			die('you are not authorized');
		}
	}
}