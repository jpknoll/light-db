<?php

App::uses('FlickerTest', 'Model');
App::uses('FlickerTestData', 'Model');
App::uses('FlickerTestDimmer', 'Model');

class FlickerTestsController extends AppController {
	public $helpers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	    'Search.Prg'
	);
	public $paginate = array(
		'limit' => 25,
		'order' => 'FlickerTest.id',
	);
	
	public function index() {
		// authenticate user role
		if (!$this->checkPermission('FlickerTest', 'read')) {
			die('you are not authorized');
		}
		
		$data = $this->paginate('FlickerTest');
		$this->set('tests', $data);
	}
	
	public function find() {
		// authenticate user role
		if (!$this->checkPermission('FlickerTest', 'read')) {
			die('you are not authorized');
		}
		
		# process passed args
		$this->Prg->commonProcess();
		
		# setup products dropdown
		$this->loadModel('Product');
		$this->Product->displayField = 'public_name';
		$products = $this->Product->find('list', array('order' => 'public_name'));
		$this->set('Products', $products);
		
		# setup orientation dropdown
		$this->loadModel('Dimmer');
		$this->Dimmer->displayField = 'name';
		$dimmers = $this->Dimmer->find('list', array('order' => 'name'));
		$this->set('Dimmers', $dimmers);
		
		# setup reports dropdown
		$this->set('reportTypes', array(
			'report_flicker_chart' => 'Flicker Chart',
		));
		
		# process search results
		$this->paginate['conditions'] = $this->FlickerTest->parseCriteria($this->passedArgs);
		
		$this->paginate['contain'] = array(
			'Sample',
			'Dimmer',
		);
		
		$tests = $this->paginate('FlickerTest');
		$this->set('tests', $tests);
	}
	
	public function view($id = null) {
		// authenticate user role
		if (!$this->checkPermission('FlickerTest', 'read')) {
			die('you are not authorized');
		}
		
		if (!$id) {
            throw new NotFoundException(__('Invalid test'));
		}

        $test = $this->FlickerTest->findById($id);
		if (!$test) {
            throw new NotFoundException(__('Invalid test'));
		}
        $this->set('test', $test);
	}
	
	public function add() {
		// authenticate user role
		if (!$this->checkPermission('FlickerTest', 'create')) {
			die('you are not authorized');
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
            $this->FlickerTest->create();
			if ($this->FlickerTest->save($this->request->data)) {
                $this->Session->setFlash(__('Your test has been saved.'));
				return $this->redirect(array('action' => 'index'));
			}
            $this->Session->setFlash(__('Unable to add your test.'));
		}
	}
}
