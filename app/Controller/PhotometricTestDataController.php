<?php

class PhotometricTestDataController extends AppController {
	
	public $name = 'PhotometricTestData';
	public $uses = array('PhotometricTestData');
	
	public $components = array(
		'RequestHandler',
		'Search.Prg',
	    'DebugKit.Toolbar',
	);

    public $presetVars = true; // using the model configuration
		
	public function find() {
		# process passed args
		$this->Prg->commonProcess();
		
		# process search results
		$this->loadModel('PhotometricTest');
		$data = $this->PhotometricTest->find('all', array(
			'conditions' => array(
				$this->PhotometricTest->parseCriteria($this->passedArgs)
			),
			'contain' => array(
				'Sample',
				'PhotometricTestData'
			),
		));
		$this->set('data', $data);
		$this->set('_serialize', array('data'));
	}
	
	public function summary() {
		# process passed args
		$this->Prg->commonProcess();
		
		# process search results
		$summary = $this->PhotometricTestData->find('all', $options = array(
			'conditions' => $this->PhotometricTestData->parseCriteria($this->passedArgs),
			'contain' => 'PhotometricTest',
			'fields' => array(
				'theta',
				'phi', 
				'MIN(PhotometricTestData.result) as result_min',
				'AVG(PhotometricTestData.result) as result_average',
				'MAX(PhotometricTestData.result) as result_max',
			),
			'group' => array('PhotometricTestData.theta', 'PhotometricTestData.phi'),
			'order' => array('PhotometricTestData.theta', 'PhotometricTestData.phi'),
		));
		
		# summarize
		$this->set('summary', $summary);
		$this->set('_serialize', array('summary'));
	}
	
	public function maxSlices() {
		# process filter terms
		$this->Prg->commonProcess();
		
		# results array
		$maxSlices = array();
		
		# find first sample with a photometric test
		# use inner join to filter out samples without tests 
		$this->loadModel('Sample');
		$sample_test = $this->Sample->find('first', array(
			'conditions' => array(
				$this->Sample->parseCriteria($this->passedArgs),
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'photometric_tests',
					'alias' => 'PhotometricTest',
					'type' => 'INNER',
					'conditions' => array(
						'Sample.id = PhotometricTest.sample_id',
					),
				),
			),
			'fields' => array(
				'Sample.*',
				'PhotometricTest.*',
			),
			'order' => array(
				'Sample.internal_name',
				'PhotometricTest.test_datetime', 
			),
		));
				
		# find max lumen value
		$maxLumen = $this->PhotometricTestData->find('first', array( 
			'conditions' => array(
				$this->PhotometricTestData->parseCriteria($this->passedArgs),
				'PhotometricTestData.photometric_test_id' => $sample_test['PhotometricTest']['id'],
			),
			'fields' => array(
				'PhotometricTestData.*',
			),
			'order' => array(
				'PhotometricTestData.result DESC',
			)
		));
		
		# fetch data for vertical slice
        $maxVerticalSlice = $this->_getVerticalSlice($sample_test['PhotometricTest']['id'], $maxLumen['PhotometricTestData']['theta']);
		$maxSlices['maxVerticalSlice'] = $maxVerticalSlice;
				
		# fetch data for horizontal slice
        $maxHorizontalSlice = $this->_getHorizontalSlice($sample_test['PhotometricTest']['id'], $maxLumen['PhotometricTestData']['phi']);
		$maxSlices['maxHorizontalSlice'] = $maxHorizontalSlice;
		
		// ship
		$this->set('maxSlices', $maxSlices);
		$this->set('_serialize', array('maxSlices'));
	}

	protected function _getVerticalSlice($test_id, $theta) {
		// fetch data
		$verticalSlice = $this->PhotometricTestData->find('all', array(
        	'conditions' => array(
        		$this->PhotometricTestData->parseCriteria($this->passedArgs),
        		'PhotometricTestData.photometric_test_id' => $test_id,
        		'PhotometricTestData.theta' => $theta,
        	),
        	'fields' => array(
        		'PhotometricTestData.*',
        	),
        	'order' => array(
        		'PhotometricTestData.phi',
        	),
        )); 
		
		// get mirror data
		if($theta >= 180) {
			$mirrorTheta = $theta - 180;
		}
		else {
			$mirrorTheta = $theta + 180;
		}
				
		$mirrorVerticalSlice = $this->PhotometricTestData->find('all', array(
        	'conditions' => array(
        		$this->PhotometricTestData->parseCriteria($this->passedArgs),
        		'PhotometricTestData.photometric_test_id' => $test_id,
        		'PhotometricTestData.theta' => $mirrorTheta,
        	),
        	'fields' => array(
        		'PhotometricTestData.*',
        	),
        	'order' => array(
        		'PhotometricTestData.phi DESC',
        	),
        ));
		
		// look for missing data
		if (!isset($mirrorVerticalSlice) || count($mirrorVerticalSlice) == 0) {
			$mirrorVerticalSlice = $verticalSlice;
		}
		
		// flip data and combine
		foreach($mirrorVerticalSlice as $point) {
			$point['PhotometricTestData']['theta'] = $theta;
			$point['PhotometricTestData']['phi'] = 360 - $point['PhotometricTestData']['phi'];
			array_push($verticalSlice, $point);
		} 
				
		return $verticalSlice;
	}
	
	protected function _getHorizontalSlice($test_id, $phi) {
		$horizontalSlice = $this->PhotometricTestData->find('all', array(
        	'conditions' => array(
        		$this->PhotometricTestData->parseCriteria($this->passedArgs),
        		'PhotometricTestData.photometric_test_id' => $test_id,
        		'PhotometricTestData.phi' => $phi,
        	),
        	'fields' => array(
        		'PhotometricTestData.*',
        	),
        	'order' => array(
        		'PhotometricTestData.theta',
        	),
        )); 
		
		# check for incomplete circle
		$thetaAngles = array_map(function($value) {
			return $value['PhotometricTestData']['theta'];
		}, $horizontalSlice);
		$minAngle = min($thetaAngles);
		$maxAngle = max($thetaAngles);
				
		if ($maxAngle != 360) {
			$mirroHorizontalSlice = array_map(function($value) {
				$value['PhotometricTestData']['theta'] = 360 - $value['PhotometricTestData']['theta'];
				return $value;
			}, $horizontalSlice);
			
			$horizontalSlice = array_merge($horizontalSlice, $mirroHorizontalSlice);
		}
		
		return $horizontalSlice;
	}
}
	