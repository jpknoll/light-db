<?php

App::uses('SpectralTest', 'Model');
App::uses('SpectralTestFact', 'Model');
App::uses('SpectralTestOrientationTypes', 'Model');

class SpectralTestsController extends AppController {
	public $helers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
		'Search.Prg',
	    'DebugKit.Toolbar',
	);
	public $paginate = array(
		'limit' => 100,
		'order' => 'SpectralTest.id',
	);
	public $presetVars = true; // using the model configuration
	
	public function index() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTest', 'read')) {
			die('you are not authorized');
		}
		
		// process passed args
		$this->Prg->commonProcess();
		
		// setup options
		$this->paginate['conditions'] = array(
			$this->SpectralTest->parseCriteria($this->Prg->parsedParams()),
		);
		$this->paginate['contain'] = array(
			'Sample', 
			'SpectralTestOrientationType',
			'SpectralTestHousingType',
		);
		
		$spectral_tests = $this->paginate('SpectralTest');
		$this->set('spectral_tests', $spectral_tests);
		$this->set('_serialize', array('spectral_tests'));
	}
	
	public function find() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTest', 'read')) {
			die('you are not authorized');
		}
		
		// process passed args
		$this->Prg->commonProcess();
		
		// check for report requests
		if (isset($this->passedArgs['submit_report'])) {
			$report = $this->passedArgs['report'];
			if ($report != '') {
				return $this->redirect(array_merge(array('controller' => 'spectral_tests', 'action' => $report), $this->passedArgs));
			}
		}
		
		// setup products dropdown
		$this->loadModel('Products');
		$this->Products->displayField = 'public_name';
		$products = $this->Products->find('list', array('order' => 'public_name'));
		$this->set('Products', $products);
		
		// setup orientation dropdown
		$this->loadModel('SpectralTestOrientationType');
		$this->SpectralTestOrientationType->displayField = 'orientation_type';
		$orientations = $this->SpectralTestOrientationType->find('list', array('order' => 'orientation_type'));
		$this->set('SpectralTestOrientationTypes', $orientations);
		
		// setup housing dropdown
		$this->loadModel('SpectralTestHousingType');
		$this->SpectralTestHousingType->displayField = 'housing_type';
		$housings = $this->SpectralTestHousingType->find('list', array('order' => 'housing_type'));
		$this->set('SpectralTestHousingTypes', $housings);
		
		// setup reports dropdown
		$this->set('reportTypes', array(
			'report_chromatic_chart' => 'Chromatic Chart',
		));
		
		// process search results
        $this->paginate['conditions'] = $this->SpectralTest->parseCriteria($this->passedArgs);
		
		$this->paginate['contain'] = array(
			'Sample' => array('public_name'),
			'SpectralTestOrientationType'
		);
		
		$tests = $this->paginate('SpectralTest');
		$this->set('tests', $tests);
		$this->set('_serialize', array('tests'));
	}
		
	public function view($id = null) {
		// authenticate user role
		if (!$this->checkPermission('SpectralTest', 'read')) {
			die('you are not authorized');
		}
		
        if (!$id) {
            throw new NotFoundException(__('Invalid test'));
		}

        $test = $this->SpectralTest->find('first', array(
        	'contain' => array(
        		'Sample',
        		'SpectralTestOrientationType',
			),
        	'conditions' => array(
        		'SpectralTest.id' => $id,
			),
		));
		if (!$test) {
            throw new NotFoundException(__('Invalid test'));
		}
        $this->set('test', $test);
		
		//fetch facts
		$this->loadModel('SpectralTestFact');
		$facts = $this->SpectralTestFact->find('all', array(
			'contain' => array(
				'SpectralTestFactType',
			),
			'conditions' => array(
				'spectral_test_id' => $id,
			),
		));
		$this->set('facts', $facts);
		
		//fetch data
		$this->loadModel('SpectralTestData');
		$data = $this->SpectralTestData->find('all', array(
			'contain' => false,
			'conditions' => array(
				'spectral_test_id' => $id,
        		'SpectralTestData.nm >=' => 400,	//limit nm to 400-800 visible range
				'SpectralTestData.nm <=' => 800
			)
		));
		$this->set('data', $data);
	}
	
	public function add() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTest', 'create')) {
			die('you are not authorized');
		}
		
        if ($this->request->is('test')) {
            $this->SpectralTest->create();
			if ($this->SpectralTest->save($this->request->data)) {
                $this->Session->setFlash(__('Your test has been saved.'));
				return $this->redirect(array('action' => 'index'));
			}
            $this->Session->setFlash(__('Unable to add your test.'));
		}
    }

	public function edit($id = null) {
		// authenticate user role
		if (!$this->checkPermission('SpectralTest', 'update')) {
			die('you are not authorized');
		}

		// check id field
		if (!$id) {
			throw new NotFoundException(__('Invalid spectral test'));
		}

		// has data been posted?
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SpectralTest->saveAssociated($this->request->data['SpectralTest'])) {
				$this->Session->setFlash(__('Spectral Test updated successfully'));
				return $this->redirect(array('action' => 'view', $id));
			}
			else {
				$this->Session->setFlash(__('Unable to update spectral test.'));
			}
		}
		// else fetch data
		else {
			$data = $this->SpectralTest->find('first', array(
	        	'conditions' => array(
	        		'SpectralTest.id' => $id,
				),
	        	'contain' => array(
	        		'Sample',
	        		'SpectralTestFacts',
	        		'SpectralTestData',
				),
			));
			
			if (!$data) {
	            throw new NotFoundException(__('Invalid test'));
			}		
			$this->request->data = $data;
		}
		
		// setup orientation dropdown
		$this->loadModel('SpectralTestOrientationType');
		$this->SpectralTestOrientationType->displayField = 'orientation_type';
		$orientations = $this->SpectralTestOrientationType->find('list', array('order' => 'orientation_type'));
		$this->set('orientations', $orientations);
		
		// setup housing dropdown
		$this->loadModel('SpectralTestHousingType');
		$this->SpectralTestHousingType->displayField = 'housing_type';
		$housings = $this->SpectralTestHousingType->find('list', array('order' => 'housing_type'));
		$this->set('housings', $housings);
		
		// setup fact type dropdown
		$this->loadModel('SpectralTestFactType');
		$this->SpectralTestFactType->displayField = 'fact_type';
		$spectral_test_fact_types = $this->SpectralTestFactType->find('list', array('order' => 'fact_type'));
		$this->set('spectral_test_fact_types', $spectral_test_fact_types);
	}

	public function summary() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTest', 'read')) {
			die('you are not authorized');
		}
		
		// process search results
		$this->Prg->commonProcess();
        $this->paginate['conditions'] = $this->SpectralTest->parseCriteria($this->passedArgs);
		
		$this->paginate['contain'] = array(
			'Sample' => array('public_name'),
			'SpectralTestOrientationType'
		);
		
		$tests = $this->paginate('SpectralTest');
		$this->set('tests', $tests);
		$this->set('_serialize', array('tests'));
	} 

	public function report_chromatic_chart() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTest', 'read')) {
			die('you are not authorized');
		}
		
		// process passed args
		$this->Prg->commonProcess();
		
		// fetch facts
		$this->loadModel('SpectralTestFact');
		$facts = $this->SpectralTestFact->find('all', array(
			'contain' => array(
				'SpectralTest' => array(
					'Sample',
					'SpectralTestOrientationType',
				),
				'SpectralTestFactType',
			),
			'conditions' => array(
				$this->SpectralTestFact->parseCriteria($this->passedArgs),
				'SpectralTestFactType.fact_type' => array('CIE x', 'CIE y'),
			),
		));
		$this->set('facts', $facts);
		
		$tests = array();
		foreach($facts as $fact) {
			$test_id = $fact['SpectralTest']['id'];
			$fact_type = $fact['SpectralTestFactType']['fact_type'];
			
			$tests[$test_id]['SpectralTest'] = $fact['SpectralTest'];
			$tests[$test_id][$fact_type] = $fact['SpectralTestFact']['fact'];
		}
		$this->set('tests', $tests);
	}
}
