<?php

class FlickerUploadFactTypesController extends AppController {

	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	);

	public function index() {
		
		$flicker_upload_fact_types = $this->FlickerUploadFactType->find('all', array(
		));
		
		$this->set('flicker_upload_fact_types', $flicker_upload_fact_types);
		$this->set('_serialize', array('flicker_upload_fact_types'));
	}
}