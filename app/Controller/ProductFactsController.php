<?php

class ProductFactsController extends AppController {
	
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar' => array(/* array of settings */),
	);
	
	public function add() {
		// authenticate user role
		if (!$this->checkPermission('ProductFact', 'create')) {
			die('you are not authorized');
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->ProductFact->create();
			$this->ProductFact->save($this->request->data);
			
			$data = $this->ProductFact->read();
			$this->set('data', $data);
			$this->set('_serialize', array('data'));
		}
	}
	
	public function view($id = null) {
		// authenticate user role
		if (!$this->checkPermission('ProductFact', 'read')) {
			die('you are not authorized');
		}
		
		$this->ProductFact->id = $id;
		$data = $this->ProductFact->read();
		$this->set('data', $data);
		$this->set('_serialize', array('data'));
	}
	
	public function edit($id = null) {
		// authenticate user role
		if (!$this->checkPermission('ProductFact', 'update')) {
			die('you are not authorized');
		}
		
		// has data been posted?
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->ProductFact->save($this->request->data['ProductFact']);
		}
		
		$this->ProductFact->id = $id;
		$data = $this->ProductFact->read();
		$this->set('data', $data);
		$this->set('_serialize', array('data'));
	}
	
	public function delete($id = null) {
		// authenticate user role
		if (!$this->checkPermission('ProductFact', 'delete')) {
			die('you are not authorized');
		}
		
		$this->ProductFact->id = $id;
		$this->ProductFact->delete();
	}
}
	