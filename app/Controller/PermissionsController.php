<?php

// app/Controller/PermissionsController.php
class PermissionsController extends AppController {

	public $name = 'Permissions';
	public $uses = 'Aco';

	public $helpers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	);
	
	public function index() {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'read')) {
			die('you are not authorized');
		}
		
		// fetch controls
		$data = $this->Acl->Aco->find('all', array(
			'order' => array('Aco.lft'),
		));
		$this->set('data', $data);
	}
	
	public function add() {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'create')) {
			die('you are not authorized');
		}
	
		// fetch parents
		$parents = $this->Acl->Aco->find('list', array('fields' => array('alias')));
		$this->set('parents', $parents);
	
		// on post
		if ($this->request->is('post') || $this->request->is('put')) {
			if (isset($this->request->data['cancel'])) {
				return $this->redirect(array('action' => 'index'));
			}
			
			if (!empty($this->request->data['Aco'])) {				
				// create group object
				$this->Acl->Aco->create();
				if ($this->Acl->Aco->save($this->request->data['Aco'])) {
					$this->Session->setFlash(__('The permission control has been created'));
	            	return $this->redirect(array('action' => 'index'));
				}
			}

			$this->Session->setFlash(__('The permission control could not be created. Please, try again.'));
			return;
        } 
	}
	
	public function edit($id = null) {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'update')) {
			die('you are not authorized');
		}
		
		// on post
		if ($this->request->is('post') || $this->request->is('put')) {
			if (isset($this->request->data['cancel'])) {
				return $this->redirect(array('action' => 'index'));
			}
			
			if ($this->Acl->Aco->save($this->request->data)) {
				$this->Session->setFlash(__('The permission control has been saved'));
	            return $this->redirect(array('action' => 'index'));
			}
			
			$this->Session->setFlash(__('The permission control could not be saved. Please, try again.'));
			return;
        } 
		else {
			// fetch parents
			$parents = $this->Acl->Aco->find('list', array(
				'conditions' => array('id !=' => $id), // control can't be it's own parent!
				'fields' => array('alias'),
				'order' => array('lft'),
			));
			$this->set('parents', $parents);
			
			// fetch control
			$this->Acl->Aco->id = $id;
			$this->request->data = $this->Acl->Aco->read();
		}
	}

	public function delete($id = null) {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'delete')) {
			die('you are not authorized');
		}
		
		// delete access role object
		$aro = $this->Acl->Aro->id = $id;
		if($this->Acl->Aro->delete()) {
			$this->Session->setFlash(__('Permission Control deleted'));
			$this->redirect(array('action' => 'index'));
		}
		else {
			$this->Session->setFlash(__('The permission control could not be deleted.'));
			return;
		}
	}
}