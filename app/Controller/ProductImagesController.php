<?php

class ProductImagesController extends AppController {
	public $helers = array();
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	);
	
	public function add() {
		if($this->ProductImage->save($this->request->data)) {
			$this->Session->setFlash(__('Product Image uploaded successfully'));
			return $this->redirect(array('controller' => 'Products', 'action' => 'edit', $this->request->data['ProductImage']['product_id']));
		}
		else {
			$this->Session->setFlash(__('Unable to upload product image.'));
		}
	}
	
	public function delete($id = null) {
		if (!$id) {
			throw new NotFoundException(__('Invalid product image'));
		}
			
		$image = $this->ProductImage->findById($id); 
		if (!$image) {
			throw new NotFoundException(__('Invalid product image'));
		}
			
		if ($this->ProductImage->delete($id)) {
			$this->Session->setFlash(__('Product Image deleted successfully'));
			return $this->redirect(array('controller' => 'Products', 'action' => 'edit', $image['ProductImage']['product_id']));
		}
		else {
			$this->Session->setFlash(__('Unable to delete product image.'));
		} 
	}

	public function order($id = null) {		
		if (!$id) {
			throw new NotFoundException(__('Invalid product'));
		}
			
		$this->loadModel('Product');
		$product = $this->Product->findById($id); 
		if (!$product) {
			throw new NotFoundException(__('Invalid product'));
		}
			
		$order = $this->request->data['order'];
		if (!$order) {
			throw new Exception(__("post data variable 'order' missing or malformed"));
		}
		
		#set order for images
		foreach($order as $i => $image_id) {
			$image = $this->ProductImage->findById($image_id);
			if (!$image) {
				throw new NotFoundException(__('Invalid product image: ' + $image_id));
			}
			$image['ProductImage']['order'] = $i;
			$this->ProductImage->save($image);
		}
		
		#reorder remaining images
		$images = $this->ProductImage->findByProductId($id);
		$reorder = sizeof($order);
		foreach($images as $i => $image) {
			if(!in_array($image['ProductImage']['id'], $order)) {
				$image['ProductImage']['order'] = $reorder;
				$this->ProductImage->save($image);
				$reorder++;
			}
		}
	}
}
		