<?php

class ProductsController extends AppController {
	public $helers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
		'Search.Prg',
	    'DebugKit.Toolbar',
	);
	public $paginate = array(
		'limit' => 100,
		'order' => 'Product.id',
		);
	public $presetVars = true; // using the model configuration
	
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function index() {
		// authenticate user role
		if (!$this->checkPermission('Product', 'read')) {
			die('you are not authorized');
		}
					
		// setup source dropdown
		$sources = $this->_get_allowed_sources();
		$this->set('sources', $sources);
		
		$this->loadModel('ProductLampType');
		$product_lamp_types = $this->ProductLampType->find('all', array(
			'contain' => false,
			'fields' => array('id', 'product_lamp_type',),
		));
		$this->set('product_lamp_types', $product_lamp_types);
		
		// fetch products
		$options = array(
			'contain' => array(
				'ProductLampType' => array(
					'fields' => array(
						'id',
						'product_lamp_type',
					),
				),
				'ProductFacts' => array(
					'conditions' => array(
						'source =' => $sources,
					),
					'ProductFactType' => array(
						'id',
						'name',
						'units',
						'description',
					),
					'fields' => array(
						'product_id',
						'value',
						'source',
					),
				),
			),
			'fields' => array(
				'id',
				'public_name',
			),
		);
		
		if ($this->checkPermission('ProductImage', 'read')) {
			$options['contain'][] = 'ProductImages';
		}
		
		if ($this->checkPermission('Product.internal_name', 'read')) {
			$options['fields'][] = 'internal_name';
		}
				
		$products = $this->Product->find('all', $options);
		$this->set('products', $products);
		
		// serialize for json
		$this->set('_serialize', array('products', 'sources', 'product_lamp_types'));
	}
	
	public function find() {
		// authenticate user role
		if (!$this->checkPermission('Product', 'read')) {
			die('you are not authorized');
		}

		//setup type dropdowns
		$this->loadModel('ProductFactType');
		$product_fact_types = $this->ProductFactType->find('all', array(
			'contain' => false,
			'fields' => array(
				'id',
				'name',
				'value_type',
			),
		));
		$this->set('product_fact_types', $product_fact_types);
		
		// setup source dropdown
		$sources = $this->_get_allowed_sources();
		$this->set('sources', $sources);
		
		// setup product lamp type dropdown
		$this->loadModel('ProductLampType');
		$product_lamp_types = $this->ProductLampType->find('all', array(
			'contain' => false,
			'fields' => array('id', 'product_lamp_type',),
		));
		$this->set('product_lamp_types', $product_lamp_types);
		
		// fetch products
		$options = array(
			'contain' => array(
				'ProductFacts' => array(
					'conditions' => array(
						'source =' => $sources,
					),
					'ProductFactType' => array(
						'id',
						'name',
						'units',
						'description',
					),
					'fields' => array(
						'product_id',
						'value',
						'source',
					),
				),
			),
			'fields' => array(
				'id',
				'public_name',
			),
		);
		
		if ($this->checkPermission('ProductImage', 'read')) {
			$options['contain'][] = 'ProductImages';
		}
		
		if ($this->checkPermission('Product.internal_name', 'read')) {
			$options['fields'][] = 'internal_name';
		}
		
		$products = $this->Product->find('all', $options);
		$this->set('products', $products);
		
		// serialize for json
		$this->set('_serialize', array('products', 'product_fact_types', 'sources', 'product_lamp_types'));
	}
	
	public function view($id = null) {
		// authenticate user role
		if (!$this->checkPermission('Product', 'read')) {
			die('you are not authorized');
		}
		
		if (!$id) {
			throw new NotFoundException(__('Invalid product'));
		}

		// source dropdown
		$sources = $this->_get_allowed_sources();
		$this->set('sources', $sources);
		
		//setup type lookup
		$this->loadModel('ProductFactType');
		$product_fact_types = $this->ProductFactType->find('all', array(
			'contain' => false,
			'fields' => array(
				'id',
				'name',
				'value_type',
			),
		));
		$this->set('product_fact_types', $product_fact_types);

		// fetch product information
		$options = array(
			'conditions' => array(
				'Product.id =' => $id,
			), 
			'contain' => array(
				'ProductLampType' => array(
					'fields' => array(
						'id',
						'product_lamp_type',
					),
				),
				'ProductFacts' => array(
					'conditions' => array(
						'source =' => $sources,
					),
					'ProductFactType' => array(
						'id',
						'name',
						'units',
						'description',
					),
					'fields' => array(
						'product_id',
						'value',
						'source',
					),
				),
			),
			'fields' => array(
				'id',
				'public_name',
			),
		);
		
		if ($this->checkPermission('ProductImage', 'read')) {
			$options['contain'][] = 'ProductImages';
		}
		
		if ($this->checkPermission('Product.internal_name', 'read')) {
			$options['fields'][] = 'internal_name';
		}
		
		if ($this->checkPermission('Product.manufacturer', 'read')) {
			$options['fields'][] = 'manufacturer';
		}
		
		if ($this->checkPermission('Product.part_number', 'read')) {
			$options['fields'][] = 'part_number';
		}
		
		$product = $this->Product->find('first', $options);
		if (!$product) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->set('product', $product);
						
		//fetch photometric summary
		$this->loadModel('Sample');
		$gonio_summary_temp = $this->Sample->find('all', array(
			'conditions' => array(
				'Sample.product_id' => $id,
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'photometric_tests',
					'alias' => 'PhotometricTests',
					'type' => 'INNER', 
					'conditions' => array(
						'Sample.id = PhotometricTests.sample_id',
					),
				),
			),
			'fields' => array(
				'AVG(PhotometricTests.wattage) as Wattage',
				'AVG(PhotometricTests.light_level) as Output',
				'AVG(PhotometricTests.light_level) / AVG(PhotometricTests.wattage) as Efficacy',
			),
		));
		$gonio_summary['Output'] = $gonio_summary_temp[0][0]['Output'];
		$gonio_summary['Wattage'] = $gonio_summary_temp[0][0]['Wattage'];
		$gonio_summary['Efficacy'] = $gonio_summary_temp[0][0]['Efficacy'];
		$this->set('gonio_summary', $gonio_summary);
				
		// fetch flicker summary data
		$flicker_data_summary_temp = $this->Sample->find('all', array(
			'conditions' => array(
				'Sample.product_id' => $id,
				'FlickerTestData.filter_frequency' => array(200, 10000),
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'flicker_tests',
					'alias' => 'FlickerTest',
					'type' => 'INNER',
					'conditions' => array(
						'Sample.id = FlickerTest.sample_id',
					),
				),
				array(
					'table' => 'dimmers',
					'alias' => 'Dimmer',
					'type' => 'INNER',
					'conditions' => array(
						'Dimmer.id = FlickerTest.dimmer_id',
					),
				),
				array(
					'table' => 'flicker_test_data',
					'alias' => 'FlickerTestData',
					'type' => 'INNER',
					'conditions' => array(
						'FlickerTest.id = FlickerTestData.flicker_test_id',
					),
				),
			),
			'fields' => array(
				'FlickerTest.dimmer_level',
				'Dimmer.name',
				'FlickerTestData.filter_frequency',
				'AVG(FlickerTestData.fundamental_frequency) as fundamental_frequency',
				'AVG(FlickerTestData.flicker_index) as flicker_index_average',
				'AVG(FlickerTestData.flicker_percent) as flicker_percent_average',
			),
			'group' => array(
				'FlickerTest.dimmer_id',
				'FlickerTest.dimmer_level',
				'FlickerTestData.filter_frequency',
			),
			'order' => array(
				'FlickerTest.dimmer_id',
				'FlickerTest.dimmer_level',
				'FlickerTestData.filter_frequency',
			),
		));
		
		//create pivot table
		$flicker_data_summary = array();
		foreach($flicker_data_summary_temp as $value) {
			$dimmer_type = $value['Dimmer']['name'];
			
			$dimmer_level = $value['FlickerTest']['dimmer_level'];
			if($dimmer_level == '0') {
				$dimmer_level = 'min';
			}
			else {
				$dimmer_level = $dimmer_level . ' %';
			}
			
			$filter_freq = $value['FlickerTestData']['filter_frequency'];
			
			$flicker_data_summary[$dimmer_type][$dimmer_level][$filter_freq]['index'] = $value[0]['flicker_index_average'];
			$flicker_data_summary[$dimmer_type][$dimmer_level][$filter_freq]['percent'] = $value[0]['flicker_percent_average'];
			
			if ($dimmer_type == 'Power Supply' && $dimmer_level == '100 %' && $filter_freq == '10000') {
				$this->set('flicker_data_summary_frequency', $value[0]['fundamental_frequency']);
			}
		}
				
		$this->set('flicker_data_summary', $flicker_data_summary);
		
		$this->set('flicker_data_summary_dimmers', array(
			'Power Supply',
			'Dimmer A',
			'Dimmer B',
			'Dimmer C',
			'Dimmer D',
			'Dimmer E',
			'Dimmer F',
		));
		
		$this->set('flicker_data_summary_levels', array(
			'100 %', '75 %', '50 %', '25 %', 'min',
		));
		
		// serialize for json
		$this->set('_serialize', array('product', 'samples', 'product_fact_types', 'sources'));
	}
		
	public function add() {
		// authenticate user role
		if (!$this->checkPermission('Product', 'create')) {
			die('you are not authorized');
		}
		
		// has data been posted?
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Product->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Product updated successfully'));
				return $this->redirect(array('action' => 'view', $this->Product->getLastInsertID()));
			}
			else {
				$this->Session->setFlash(__('Unable to update product.'));
			}
		}
	}

	public function edit($id = null) {
		// authenticate user role
		if (!$this->checkPermission('Product', 'update')) {
			die('you are not authorized');
		}

		// has data been posted?
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Product->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Product updated successfully'));
				return $this->redirect(array('action' => 'view', $id));
			}
			else {
				$this->Session->setFlash(__('Unable to update product.'));
			}
		}
		// otherwise, load up edit form
		else {
			// check id field
			if (!$id) {
				throw new NotFoundException(__('Invalid product'));
			}
	
			// fetch product information
			$product = $this->Product->find('first', array(
				'conditions' => array(
					'Product.id =' => $id,
				), 
				'contain' => array(
					'ProductFacts' => array(
						'conditions' => array(
							'source =' => $this->_get_allowed_sources(),
						),
						'fields' => array(
							'id',
							'value',
							'source',
						),
						'ProductFactType' => array(
							'id',
							'name',
							'units',
							'description',
						),
					),
					'ProductImages',
				),
				'fields' => array(
					'id',
					'public_name',
					'internal_name',
				),
			));
			if (!$product) {
				throw new NotFoundException(__('Invalid product'));
			}
			$this->request->data = $product;
			
			// setup dropdowns
			$this->loadModel('ProductFactType');
			$this->set('productFactTypes', $this->ProductFactType->find('list'));
			return;
			$this->set('productFactTypes', $this->ProductFactType->find('all', array(
				'contain' => false,
				'fields' => array(
					'id',
					'name',
					'value_type',
				),
			)));
		}
	}
	
	public function compare() {
		// authenticate user role
		if (!$this->checkPermission('Product', 'read')) {
			die('you are not authorized');
		}
		
		//if ($this->request->accepts('application/json')) {
			$product_ids = $this->request->query;
			$this->set('product_ids', $product_ids);
			
			// setup drowdowns		
			$sources = $this->_get_allowed_sources();
			$this->set('sources', $sources);
					
			// setup lookup values
			$this->loadModel('ProductFactType');
			$product_fact_types = $this->ProductFactType->find('all', array(
				'contain' => false,
				'fields' => array(
					'id',
					'name',
					'value_type',
				),
			));
			$this->set('product_fact_types', $product_fact_types);
					
			// fetch products
			$options = array(
				'conditions' => array(
					'id' => $product_ids, 
				),
				'contain' => array(
					'ProductFacts' => array(
						'conditions' => array(
							'source =' => $sources,
						),
						'ProductFactType' => array(
							'id',
							'name',
							'units',
							'description',
						),
						'fields' => array(
							'product_id',
							'value',
							'source',
						),
					),
				),
				'fields' => array(
					'id',
					'public_name',
				),
			);
			
			if ($this->checkPermission('ProductImage', 'read')) {
				$options['contain'][] = 'ProductImages';
			}
			
			if ($this->checkPermission('Product.internal_name', 'read')) {
				$options['fields'][] = 'internal_name';
			}
			
			$products = $this->Product->find('all', $options);
			$this->set('products', $products);
			
			// serialize for json
			$this->set('_serialize', array('products', 'product_fact_types', 'sources'));
		//}
	}	

	public function get_samples($id = null) {
		$this->layout = 'ajax';
		
		$product = $this->Product->find('first', array(
			'conditions' => array(
				'Product.id' => $id,
			),
			'contain' => array(
				'Samples' => array(
					'id',
					'public_name',
				),
			),
			'fields' => array(),
		));
		
		$this->set('samples', $product['Samples']); 
	}

	protected function _get_allowed_sources() {
		// fetch user object
		$user = $this->Session->read('Auth.User');
		if (!isset($user)) {
			return array();
		}
		
		// fetch group access role object
	    $aro = $this->Acl->Aro->find('first', array(
	        'conditions' => array(
	            'Aro.model' => 'Group',
	            'Aro.foreign_key' => $user['group_id'],
	        ),
	    ));
		
		// user doesn't belong to any groups
		if (!isset($aro['Aro'])) {
			// fetch user access role object instead
		    $aro = $this->Acl->Aro->find('first', array(
		        'conditions' => array(
		            'Aro.model' => 'User',
		            'Aro.foreign_key' => $user['id'],
		        ),
		    ));
		}

		// user had no permissions, early return
		if (!isset($aro['Aro'])) {
			return array();
		}

		// fetch source aco's		
		$acos = $this->Acl->Aco->find('all', array(
			'conditions' => array(
				'model' => 'ProductFactSource',
				'alias LIKE' => 'ProductFactSource.source.%',
			)
		));
		
		// check aro's permissions against source aco's
		$allowed = array();
		foreach($acos as $aco) {
			if ($this->Acl->check($aro['Aro'], $aco['Aco'], 'read')) {
				$pieces = explode('.', $aco['Aco']['alias']);
				array_push($allowed, $pieces[2]);
			}
		}
		return $allowed;
	}

	private function array_or($array1, $array2) {
		foreach($array1 as $i => $value) {
			if (!in_array($value, $array2)) {
				unset($array1[$i]);
			}
		}
		return $array1;
	}
	
	private function array_union_deep($array1, $array2) {
		foreach($array2 as $key => $value) {			
			if (!isset($array1[$key])) {
				$array1[$key] = $array2[$key];
			}
			else if (is_array($array2[$key])) {
				$array1[$key] = $this->array_union_deep($array1[$key], $array2[$key]);
			}
		}
		return $array1;
	}
}