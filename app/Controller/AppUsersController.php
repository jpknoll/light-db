<?php

App::uses('UsersController', 'Users.Controller');
App::uses('PermissionsListener', 'Event');

class AppUsersController extends UsersController {

    public $name = 'Users';
	public $uses = 'AppUser';

/**
 * Forward requests to the plugin if they don't exist
 */
	public function render($view = null, $layout = null) {
	    if (is_null($view)) {
	        $view = $this->action;
	    }
	    $viewPath = substr(get_class($this), 0, strlen(get_class($this)) - 10);
	    if (!file_exists(APP . 'View' . DS . $viewPath . DS . $view . '.ctp')) {
	        $this->plugin = 'Users';
	    } else {
	        $this->viewPath = $viewPath;
	    }
	    return parent::render($view, $layout);
	}
	
/**
 * Fix parent UsersController to use extended model
 */
	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->User = ClassRegistry::init('AppUser');
	    $this->set('model', 'AppUser');
	}
	
	public function add() {
		if ($this->Auth->user()) {
			$this->Session->setFlash(__d('users', 'You are already registered and logged in!'));
			$this->redirect('/');
		}

		if (!empty($this->request->data)) {
			// set default group
			$this->loadModel('Group');
			$group = $this->Group->find('first', array(
				'conditions' => array(
					'default' => 1,
				),
			));
			$this->request->data[$this->modelClass]['group_id'] = $group['Group']['id'];
			
			// register user
			$user = $this->{$this->modelClass}->register($this->request->data);
			if ($user !== false) {
				
				$Event = new CakeEvent(
					'Users.Controller.Users.afterRegistration',
					$this,
					array(
						'data' => $this->request->data,
					)
				);
				$this->getEventManager()->dispatch($Event);
				if ($Event->isStopped()) {
					$this->redirect(array('action' => 'login'));
				}
				
				// send admin emails
				$this->_sendAdminInformEmail($this->{$this->modelClass}->data);

				// send user email
				$this->_sendVerificationEmail($this->{$this->modelClass}->data);
				$this->Session->setFlash(__d('users', 'Your account has been created. You should receive an e-mail shortly to authenticate your account. Once validated you will be able to login.'));
				$this->redirect(array('action' => 'login'));
				return;
			} else {
				unset($this->request->data[$this->modelClass]['password']);
				unset($this->request->data[$this->modelClass]['temppassword']);
				$this->Session->setFlash(__d('users', 'Your account could not be created. Please, try again.'), 'default', array('class' => 'message warning'));
			}
		}
	}
	
	public function login() {
		$permissions = new PermissionsListener();
		$this->getEventManager()->attach($permissions);
			
		parent::login();
	}

	public function logout() {
		parent::logout();
		
		$this->Session->delete('Auth.Permissions');	
		$this->Session->setFlash('You have been logged out');
		return $this->redirect($this->Auth->logout());
	}

	public function admin_index() {
		// authenticate user role
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'read')) {
			die('you are not authorized');
		}
		
		parent::admin_index();
	}
	
	public function admin_view($id = null) {
		// authenticate user role
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'read')) {
			die('you are not authorized');
		}
		
		parent::admin_view($id);
	}
	
	public function admin_add() {
		// authenticate user role
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'create')) {
			die('you are not authorized');
		}
		
		// fetch dropdowns
		$this->loadModel('Group');
		$groups = $this->Group->find('list', array(
			'fields' => array('groupname'),
		));
		$this->set('groups', $groups);
		
		parent::admin_add();
	}
	
	public function admin_edit($userId = null) {
		// authenticate user role
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'update')) {
			die('you are not authorized');
		}
		
		// fetch dropdowns
		$this->loadModel('Group');
		$groups = $this->Group->find('list', array(
			'fields' => array('groupname'),
		));
		$this->set('groups', $groups);
		
		try {
			$result = $this->{$this->modelClass}->edit($userId, $this->request->data);
			if ($result === true) {
				$this->Session->setFlash(__d('users', 'User saved'));
				$this->redirect(array('action' => 'admin_index'));
			} else {
				$this->request->data = $result;
			}
		} catch (OutOfBoundsException $e) {
			$this->Session->setFlash($e->getMessage());
			$this->redirect(array('action' => 'admin_index'));
		}

		if (empty($this->request->data)) {
			$this->request->data = $this->{$this->modelClass}->read(null, $userId);
		}
		$this->set('roles', Configure::read('Users.roles'));
	}
	
	public function admin_delete($id = null) {
		// authenticate user role
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'delete')) {
			die('you are not authorized');
		}
		
		parent::admin_delete($id);
		
		$this->redirect(array('action'=> 'admin_index'));
	}
	
	public function admin_search() {
		// authenticate user role
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'read')) {
			die('you are not authorized');
		}
		
		parent::admin_search();
	}
	
	protected function _sendAdminInformEmail($userData, $options = array()) {
		$defaults = array(
			'from' => Configure::read('App.defaultEmail'),
			'subject' => __d('users', 'Account verification'),
			'template' => $this->_pluginDot() . 'admin_inform',
			'layout' => 'default',
			'emailFormat' => CakeEmail::MESSAGE_TEXT
		);

		$options = array_merge($defaults, $options);

		$admins = $this->AppUser->find('all', array(
			'contain' => array('Group'),
			'conditions' => array(
				'Group.groupname' => 'Administrator',
			),
		));
		
		foreach($admins as $admin) {
			$Email = $this->_getMailInstance();
			$Email->to($admin['AppUser']['email'])
				->from($options['from'])
				->emailFormat($options['emailFormat'])
				->subject($options['subject'])
				->template($options['template'], $options['layout'])
				->viewVars(array(
					'model' => $this->modelClass,
						'user' => $userData
					))
				->send();
		}
	}
}

