<?php

class SpectralTestFactsController extends AppController {
	
	public $components = array(
		'RequestHandler',
		'Search.Prg',
	    'DebugKit.Toolbar',
	);

    public $presetVars = true; // using the model configuration
 
 	public function summary() {
 		// authenticate user role
		if (!$this->checkPermission('SpectralTestFact', 'read')) {
			die('you are not authorized');
		}
		
		// process prg component
		$this->Prg->commonProcess();
				
		// fetch data
 		$spectral_test_facts = $this->SpectralTestFact->find('all', array(
 			'conditions' => array(
 				$this->SpectralTestFact->parseCriteria($this->Prg->parsedParams())
			),
			'contain' => array(
				'SpectralTest',
				'SpectralTestFactType',
			),
			'fields' => array(
				'SpectralTestFactType.*',
				'SpectralTestFact.spectral_test_fact_type_id',
				'MIN(SpectralTestFact.fact) as `fact_min`',
				'AVG(SpectralTestFact.fact) as `fact_average`',
				'MAX(SpectralTestFact.fact) as `fact_max`',
			),
			'group' => array(
				'SpectralTestFact.spectral_test_fact_type_id'
			),
		));
		
		$this->set('spectral_test_facts', $spectral_test_facts);
		$this->set('_serialize', array('spectral_test_facts'));
	}
	
	public function find() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTestFact', 'read')) {
			die('you are not authorized');
		}
		
		// process passed args
		$this->Prg->commonProcess();
		
		// fetch data
		$spectral_test_facts = $this->SpectralTestFact->find('all', array(
			'conditions' => array(
				$this->SpectralTestFact->parseCriteria($this->Prg->parsedParams()),
			),
			'contain' => array(
				'SpectralTest',
				'SpectralTestFactType',
			),
		));
		
		$this->set('spectral_test_facts', $spectral_test_facts);
		$this->set('_serialize', array('spectral_test_facts'));
	}
	
	public function edit($id = null) {
		// authenticate user role
		if (!$this->checkPermission('SpectralTestFact', 'update')) {
			die('you are not authorized');
		}
		
		// has data been posted?
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->SpectralTestFact->save($this->request->data['SpectralTestFact']);
		}
		else {
			$this->SpectralTestFact->id = $id;
			$data = $this->SpectralTestFact->read();
			$this->request->data = $data;
		}
	}
	
	public function group_by_product() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTestFact', 'read')) {
			die('you are not authorized');
		}
		
		// process prg component
		$this->Prg->commonProcess();
		
		// fetch data
 		$options = array(
 			'conditions' => array(
 				$this->SpectralTestFact->parseCriteria($this->Prg->parsedParams())
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'spectral_tests',
					'alias' => 'SpectralTest',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTest.id = SpectralTestFact.spectral_test_id',
					),
				),
				array(
					'table' => 'spectral_test_fact_types',
					'alias' => 'SpectralTestFactType',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTestFactType.id = SpectralTestFact.spectral_test_fact_type_id',
					),
				),
				array(
					'table' => 'samples',
					'alias' => 'Sample',
					'type' => 'INNER',
					'conditions' => array(
						'Sample.id = SpectralTest.sample_id',
					),
				),
				array(
					'table' => 'products',
					'alias' => 'Product',
					'type' => 'INNER', 
					'conditions' => array(
						'Product.id = Sample.product_id',
					),
				),
			),
			'fields' => array(
				'Product.id',
				'Product.public_name',
				'SpectralTestFactType.*',
				'MIN(SpectralTestFact.fact) as fact_min',
				'AVG(SpectralTestFact.fact) as fact_average',
				'MAX(SpectralTestFact.fact) as fact_max',
			),
			'group' => array(
				'Product.id',
				'SpectralTestFact.spectral_test_fact_type_id',
			),
		);
		$spectral_test_facts = $this->SpectralTestFact->find('all', $options);
		
		// summarize
		$this->set('spectral_test_facts', $spectral_test_facts);
		$this->set('_serialize', array('spectral_test_facts'));
	}

	public function group_by_orientation() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTestFact', 'read')) {
			die('you are not authorized');
		}
		
		// process passed args
		$this->Prg->commonProcess();
								
		// fetch data
		$options = array(
			'conditions' => array(
				$this->SpectralTestFact->parseCriteria($this->Prg->parsedParams()),
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'spectral_test_fact_types',
					'alias' => 'SpectralTestFactType',
					'type' => 'LEFT',
					'conditions' => array(
						'SpectralTestFactType.id = SpectralTestFact.spectral_test_fact_type_id',
					),
				),
				array(
					'table' => 'spectral_tests',
					'alias' => 'SpectralTest',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTest.id = SpectralTestFact.spectral_test_id',
					),
				),
				array(
					'table' => 'spectral_test_orientation_types',
					'alias' => 'SpectralTestOrientationType',
					'type' => 'LEFT',
					'conditions' => array(
						'SpectralTestOrientationType.id = SpectralTest.spectral_test_orientation_type_id',
					),
				),
			),
			'fields' => array(
				'SpectralTestFactType.*',
				'MIN(SpectralTestFact.fact) as `fact_min`',
				'AVG(SpectralTestFact.fact) as `fact_average`',
				'MAX(SpectralTestFact.fact) as `fact_max`',
				'SpectralTestOrientationType.*',
			),
			'group' => array(
				'SpectralTestFact.spectral_test_fact_type_id',
				'SpectralTest.spectral_test_orientation_type_id',
			),
		);
		$spectral_test_facts = $this->SpectralTestFact->find('all', $options);
		
		$this->set('spectral_test_facts', $spectral_test_facts);
		$this->set('_serialize', array('spectral_test_facts'));
	}

	public function group_by_configuration() {
		// authenticate user role
		if (!$this->checkPermission('SpectralTestFact', 'read')) {
			die('you are not authorized');
		}
		
		// process passed args
		$this->Prg->commonProcess();
								
		// fetch data
		$options = array(
			'conditions' => array(
				$this->SpectralTestFact->parseCriteria($this->Prg->parsedParams()),
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'spectral_test_fact_types',
					'alias' => 'SpectralTestFactType',
					'type' => 'LEFT',
					'conditions' => array(
						'SpectralTestFactType.id = SpectralTestFact.spectral_test_fact_type_id',
					),
				),
				array(
					'table' => 'spectral_tests',
					'alias' => 'SpectralTest',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTest.id = SpectralTestFact.spectral_test_id',
					),
				),
				array(
					'table' => 'spectral_test_orientation_types',
					'alias' => 'SpectralTestOrientationType',
					'type' => 'LEFT',
					'conditions' => array(
						'SpectralTestOrientationType.id = SpectralTest.spectral_test_orientation_type_id',
					),
				),
				array(
					'table' => 'spectral_test_housing_types',
					'alias' => 'SpectralTestHousingType',
					'type' => 'LEFT',
					'conditions' => array(
						'SpectralTestHousingType.id = SpectralTest.spectral_test_housing_type_id',
					),
				),
			),
			'fields' => array(
				'SpectralTestFactType.*',
				'MIN(SpectralTestFact.fact) as `fact_min`',
				'AVG(SpectralTestFact.fact) as `fact_average`',
				'MAX(SpectralTestFact.fact) as `fact_max`',
				'SpectralTestOrientationType.*',
				'SpectralTestHousingType.*',
			),
			'group' => array(
				'SpectralTestFact.spectral_test_fact_type_id',
				'SpectralTest.spectral_test_orientation_type_id',
				'SpectralTest.spectral_test_housing_type_id',
			),
		);
		$spectral_test_facts = $this->SpectralTestFact->find('all', $options);
		
		$this->set('spectral_test_facts', $spectral_test_facts);
		$this->set('_serialize', array('spectral_test_facts'));
	}
}
