<?php

// app/Controller/GroupsController.php
class GroupsController extends AppController {

	public $helpers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	);
	
	public function index() {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'read')) {
			die('you are not authorized');
		}
		
		// fetch groups
		$groups = $this->Group->find('all');
		$this->set('groups', $groups);
	}
	
	public function add() {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'create')) {
			die('you are not authorized');
		}
	
		// fetch controls
		$controls = $this->Acl->Aco->find('all', array(
			'order' => array('Aco.lft'),
		));
		$this->set('controls', $controls);
		
		// on post
		if ($this->request->is('post') || $this->request->is('put')) {
			if (isset($this->request->data['cancel'])) {
				return $this->redirect(array('action' => 'index'));
			}
			
			if (!empty($this->request->data)) {
				
				// start transaction
				$fail = false;
				$dataSource = $this->{$this->modelClass}->getDataSource();
				$dataSource->begin();
				
				// create group object
				$this->Group->create();
				if ($this->Group->save($this->request->data)) {
					$group_id = $this->Group->id;
					
					// create access role object
					$this->Acl->Aro->create();
					$fail |= !$this->Acl->Aro->save(array(
						'model' => 'Group',
						'foreign_key' => $group_id,
						'alias' => $this->request->data['Group']['groupname'],
					));
					$aro_id = $this->Acl->Aro->id;
					
					// permutate through 
					foreach($this->request->data['Permission'] as $permission) {
						if ($permission['_create'] == 1 ||
				            $permission['_read'] == 1 ||
				            $permission['_update'] == 1 ||
				            $permission['_delete'] == 1) {
			            	// this is a new permission with allows
			            	// create new access control object permission
			            	$permission['aro_id'] = $aro_id;
			            	$this->Acl->Aro->Permission->create();
							$fail |= !$this->Acl->Aro->Permission->save($permission);
						}
					}
				}
				else {
					// group create failed
					$fail = true;
				}
				
				if (!$fail) {
					$dataSource->commit();
					$this->Session->setFlash(__('The group has been created'));
	            	return $this->redirect(array('action' => 'index'));
				}
				else {
					$dataSource->rollback();
				}
			}

			$this->Session->setFlash(__('The group could not be created. Please, try again.'));
			return;
        } 
	}
	
	public function edit($id = null) {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'update')) {
			die('you are not authorized');
		}
		
		// fetch group
		$this->Group->id = $id;
		$group = $this->Group->find('first', array(
			'conditions' => array('id' => $id),
		));
		$this->set('group', $group);

		if (!isset($group)) {
            throw new NotFoundException(__('Invalid group'));
        }
				
		// fetch access role object
		$role = $this->Acl->Aro->find('first', array(
			'conditions' => array(
				'model' => 'group',
				'foreign_key' => $id
			),
		));
		$this->set('role', $role);
	
		// fetch controls
		$controls = $this->Acl->Aco->find('all', array(
			'order' => array('Aco.lft'),
		));
		$this->set('controls', $controls);
		
		// fetch permissions
		$permissions = $this->Acl->Aro->Permission->find('all', array(
	        'conditions' => array(
	            'Permission.aro_id' => $role['Aro']['id'],
	        ),
	    ));
		$this->set('permissions', $permissions);
		
		// on post
		if ($this->request->is('post') || $this->request->is('put')) {
			if (isset($this->request->data['cancel'])) {
				return $this->redirect(array('action' => 'index'));
			}
			
			// start transaction			
			$fail = false;
			$dataSource = $this->{$this->modelClass}->getDataSource();
			$dataSource->begin();
			
			// permutate through 
			foreach($this->request->data['Permission'] as $permission) {
				if ($permission['id'] != 0) {
					// this is an existing permission, update it
					$this->Acl->Aro->Permission->id = $permission['id'];
					$fail |= !$this->Acl->Aro->Permission->saveAssociated($permission);
				}
				else if ($permission['_create'] == 1 ||
		            $permission['_read'] == 1 ||
		            $permission['_update'] == 1 ||
		            $permission['_delete'] == 1) {
	            	// this is a new permission with allows
	            	$this->Acl->Aro->Permission->create();
					$fail |= !$this->Acl->Aro->Permission->save($permission);
				}
				else {
					// ignore this permission
				}
			}
			
			if (!$fail) {
				$dataSource->commit();
				$this->Session->setFlash(__('The group permissions have been saved'));
	            return $this->redirect(array('action' => 'index'));
			}
			else {
				$dataSource->rollback();
				$this->Session->setFlash(__('The group permissions could not be saved. Please, try again.'));
				return;
			}
        } 
	}

	public function delete($id = null) {
		// authenticate user group
		if (!$this->Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.group_id')), 'Admin', 'delete')) {
			die('you are not authorized');
		}
		
		// start transaction			
		$fail = false;
		$dataSource = $this->{$this->modelClass}->getDataSource();
		$dataSource->begin();
		
		// delete group object
		$this->Group->id = $id;
		$fail |= !$this->Group->delete();
		
		// delete access role object
		$aro = $this->Acl->Aro->find('first', array(
			'conditions' => array(
				'model' => 'Group',
				'foreign_key' => $id,
			)
		));
		$this->Acl->Aro->id = $aro['Aro']['id'];
		$fail |= !$this->Acl->Aro->delete();
		
		if (!$fail) {
			$dataSource->commit();
			$this->Session->setFlash(__('Group deleted'));
			$this->redirect(array('action' => 'index'));
		}
		else {
			$dataSource->rollback();
			$this->Session->setFlash(__('The group could not be deleted.'));
			return;
		}
	}
}