<?php

class SpectralTestDataController extends AppController {
	
	public $name = 'SpectralTestData';
	public $uses = array('SpectralTestData');
	
	public $components = array(
		'RequestHandler',
		'Search.Prg',
	    'DebugKit.Toolbar',
	);

    public $presetVars = true; // using the model configuration
		
	public function find() {
		# process passed args
		$this->Prg->commonProcess();
		
		# process search results				
		$spectral_test_data = $this->SpectralTestData->find('all', array(
			'conditions' => array(
				$this->SpectralTestData->parseCriteria($this->passedArgs),
				'SpectralTestData.nm >=' => 400,		//limit nm to 400-800 visible range
				'SpectralTestData.nm <=' => 800
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'spectral_tests',
					'alias' => 'SpectralTest',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTestData.spectral_test_id = SpectralTest.id',
					),
				),
				array(
					'table' => 'spectral_test_orientation_types',
					'alias' => 'SpectralTestOrientationType',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTest.spectral_test_orientation_type_id = SpectralTestOrientationType.id',
					),
				),
			),
			'fields' => array(
				'SpectralTest.id',
				'SpectralTestData.nm', 
				'SpectralTestData.result',
				'SpectralTestData.unit',
				'SpectralTestOrientationType.orientation_type'
			),
			'order' => array('SpectralTest.spectral_test_orientation_type_id', 'SpectralTestData.nm'),
		));
		$this->set('spectral_test_data', $spectral_test_data);
		$this->set('_serialize', array('spectral_test_data'));
	}
	
	public function summary() {
		# process search results
		$this->Prg->commonProcess();
			
		$options = array(
			'conditions' => array(
				$this->SpectralTestData->parseCriteria($this->passedArgs),
				'SpectralTestData.nm >=' => 400,		//limit nm to 400-800 visible range
				'SpectralTestData.nm <=' => 800
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'spectral_tests',
					'alias' => 'SpectralTest',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTestData.spectral_test_id = SpectralTest.id',
					),
				),
				array(
					'table' => 'spectral_test_orientation_types',
					'alias' => 'SpectralTestOrientationType',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTest.spectral_test_orientation_type_id = SpectralTestOrientationType.id',
					),
				),
			),
			'fields' => array(
				'SpectralTestData.nm', 
				'SpectralTestData.unit',
				'SpectralTestOrientationType.orientation_type',
				'MIN(SpectralTestData.result) as result_min',
				'AVG(SpectralTestData.result) as result_average',
				'MAX(SpectralTestData.result) as result_max',
			),
			'group' => array('SpectralTest.spectral_test_orientation_type_id', 'SpectralTestData.nm'),
			'order' => array('SpectralTest.spectral_test_orientation_type_id', 'SpectralTestData.nm'),
		);
		
		$summary = $this->SpectralTestData->find('all', $options);
		
		# summarize
		$this->set('summary', $summary);
		$this->set('_serialize', array('summary'));
	}

	public function group_by_configuration() {		
		// process passed args
		$this->Prg->commonProcess();
								
		// fetch data
		$options = array(
			'conditions' => array(
				$this->SpectralTestData->parseCriteria($this->Prg->parsedParams()),
				'SpectralTestData.nm >=' => 400,		//limit nm to 400-800 visible range
				'SpectralTestData.nm <=' => 800
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'spectral_tests',
					'alias' => 'SpectralTest',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTest.id = SpectralTestData.spectral_test_id',
					),
				),
				array(
					'table' => 'spectral_test_orientation_types',
					'alias' => 'SpectralTestOrientationType',
					'type' => 'LEFT',
					'conditions' => array(
						'SpectralTestOrientationType.id = SpectralTest.spectral_test_orientation_type_id',
					),
				),
				array(
					'table' => 'spectral_test_housing_types',
					'alias' => 'SpectralTestHousingType',
					'type' => 'LEFT',
					'conditions' => array(
						'SpectralTestHousingType.id = SpectralTest.spectral_test_housing_type_id',
					),
				),
			),
			'fields' => array(
				'SpectralTestData.nm', 
				'SpectralTestData.unit',
				'SpectralTestOrientationType.orientation_type',
				'SpectralTestHousingType.housing_type',
				'MIN(SpectralTestData.result) as result_min',
				'AVG(SpectralTestData.result) as result_average',
				'MAX(SpectralTestData.result) as result_max',
			),
			'group' => array(
				'SpectralTest.spectral_test_orientation_type_id', 
				'SpectralTest.spectral_test_housing_type_id',
				'SpectralTestData.nm'
			),

		);
		$spectral_test_data = $this->SpectralTestData->find('all', $options);
		
		$this->set('spectral_test_data', $spectral_test_data);
		$this->set('_serialize', array('spectral_test_data'));
	}
}
	