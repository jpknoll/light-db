<?php

class PhotometricTestsController extends AppController {
	public $helers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
	    'DebugKit.Toolbar',
	    'Search.Prg',
	);
	public $paginate = array(
		'limit' => 25,
		'order' => 'PhotometricTest.id',
		);
	
	public function index() {
		$this->paginate['recursive'] = 0;

		$data = $this->paginate('PhotometricTest');
		$this->set('tests', $data);
	}
	
	public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid test'));
		}

        $test = $this->PhotometricTest->findById($id);
		if (!$test) {
            throw new NotFoundException(__('Invalid test'));
		}
        $this->set('test', $test);
		
		$this->loadModel('PhotometricTestFact');
		$facts = $this->PhotometricTestFact->findAllByPhotometricTestId($id);
		$this->set('facts', $facts);
	}
	
	public function find() {
		# process passed args
		$this->Prg->commonProcess();
		
		# check for report requests
		if (isset($this->passedArgs['submit_report'])) {
			$report = $this->passedArgs['report'];
			if ($report != '') {
				return $this->redirect(array_merge(array('controller' => 'photometric_tests', 'action' => $report), $this->passedArgs));
			}
		}
		
		# setup products dropdown
		$this->loadModel('Products');
		$this->Products->displayField = 'public_name';
		$products = $this->Products->find('list', array('order' => 'public_name'));
		$this->set('Products', $products);
		
		# setup report dropdown
		$this->set('reportTypes', array(
			'report_photometric_chart' => 'Photometric Chart',
		));
		
		# process search results
		$this->paginate['conditions'] = $this->PhotometricTest->parseCriteria($this->passedArgs);
		
		$this->paginate['contain'] = array(
			'Sample',
		);

		$tests = $this->paginate('PhotometricTest');
		$this->set('tests', $tests);
		$this->set('_serialize', array('tests'));
	}
	
	public function add() {
        if ($this->request->is('test')) {
            $this->PhotometricTest->create();
			if ($this->PhotometricTest->save($this->request->data)) {
                $this->Session->setFlash(__('Your test has been saved.'));
				return $this->redirect(array('action' => 'index'));
			}
            $this->Session->setFlash(__('Unable to add your test.'));
		}
    }
	
	public function report_photometric_chart() {
		# process passed args
		$this->Prg->commonProcess();
		
		# process search results
		$this->paginate['conditions'] = $this->PhotometricTest->parseCriteria($this->passedArgs);
		
		$this->paginate['contain'] = array(
			'Sample',
		);

		$tests = $this->paginate('PhotometricTest');
		$this->set('tests', $tests);
		$this->set('_serialize', array('tests'));
	}
}
