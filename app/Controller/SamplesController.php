<?php

class SamplesController extends AppController {
	public $helers = array('Html', 'Form', 'Paginator');
	public $components = array(
		'RequestHandler',
		'Search.Prg',
	    'DebugKit.Toolbar',
	);
	public $paginate = array(
		'limit' => 10,
		'order' => 'Sample.id',
	);
	public $presetVars = true; // using the model configuration
	
	public function index() {
		// authenticate user role
		if (!$this->checkPermission('Sample', 'read')) {
			die('you are not authorized');
		}
		
		$samples = $this->paginate('Sample');
		$this->set('samples', $samples);
	}
	
	public function find() {
		// authenticate user role
		if (!$this->checkPermission('Sample', 'read')) {
			die('you are not authorized');
		}
		
		//PRG Common Process (fix url!)
		$this->Prg->commonProcess();
		
		# process search results
        $this->paginate['conditions'] = $this->Sample->parseCriteria($this->passedArgs);
		
		$samples = $this->paginate('Sample');
		$this->set('samples', $samples);
	}
	
	public function view($id = null) {
		// authenticate user role
		if (!$this->checkPermission('Sample', 'read')) {
			die('you are not authorized');
		}
		
		if (!$id) {
			throw new NotFoundException(__('Invalid sample'));
		}
		
		// fetch sample information
		$sample = $this->Sample->find('first', array(
			'conditions' => array(
				'Sample.id =' => $id,
			), 
			'contain' => array(
				'Product',
			),
			'fields' => array(
				'Sample.id',
				'Sample.public_name',
				'Product.id',
				'Product.public_name',
			),
		));
		if (!$sample) {
			throw new NotFoundException(__('Invalid sample'));
		}
		$this->set('sample', $sample);
		
		//summarize spectral test facts
		$spectral_facts_summary = $this->Sample->find('all', array(
			'conditions' => array(
				'Sample.id' => $id,
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'spectral_tests',
					'alias' => 'SpectralTest',
					'type' => 'INNER',
					'conditions' => array(
						'Sample.id = SpectralTest.sample_id',
					),
				),
				array(
					'table' => 'spectral_test_orientation_types',
					'alias' => 'SpectralTestOrientationType',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTestOrientationType.id = SpectralTest.spectral_test_orientation_type_id',
					),
				),
				array(
					'table' => 'spectral_test_facts',
					'alias' => 'SpectralTestFact',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTest.id = SpectralTestFact.spectral_test_id',
					),
				),
				array(
					'table' => 'spectral_test_fact_types',
					'alias' => 'SpectralTestFactType',
					'type' => 'INNER',
					'conditions' => array(
						'SpectralTestFactType.id = SpectralTestFact.spectral_test_fact_type_id',
					),
				),
			),
			'fields' => array(
				'SpectralTestOrientationType.*',
				'SpectralTestFactType.*',
				'AVG(SpectralTestFact.fact) as fact_average',
			),
			'group' => array(
				'SpectralTest.spectral_test_orientation_type_id',
				'SpectralTestFact.spectral_test_fact_type_id',
			),
		));
		
		//create pivot table
		$spectral_facts_summary_pivot = array();
		$spectral_facts_summary_counts = array();
		$spectral_facts_summary_sums = array();
		foreach($spectral_facts_summary as $value) {
			$orientation_type = $value['SpectralTestOrientationType']['orientation_type'];
			$fact_type = $value['SpectralTestFactType']['fact_type'];
			$spectral_facts_summary_pivot[$orientation_type][$fact_type] = $value[0]['fact_average'];
			
			//keep track of fact counts
			if(isset($spectral_facts_summary_counts[$fact_type])) {
				$spectral_facts_summary_counts[$fact_type] += 1;
				$spectral_facts_summary_sums[$fact_type] += $value[0]['fact_average'];
			} 
			else {
				$spectral_facts_summary_counts[$fact_type] = 1;
				$spectral_facts_summary_sums[$fact_type] = $value[0]['fact_average'];
			}
		}
				
		foreach($spectral_facts_summary_pivot as $key => $value) {
			$spectral_facts_summary_pivot[$key]['Efficacy'] =
				$spectral_facts_summary_pivot[$key]['Output'] / $spectral_facts_summary_pivot[$key]['Normal Power'];  
				
			if(isset($spectral_facts_summary_counts['Efficacy'])) {
				$spectral_facts_summary_counts['Efficacy'] += 1;
				$spectral_facts_summary_sums['Efficacy'] += $spectral_facts_summary_pivot[$key]['Efficacy'];
			} 
			else {
				$spectral_facts_summary_counts['Efficacy'] = 1;
				$spectral_facts_summary_sums['Efficacy'] = $spectral_facts_summary_pivot[$key]['Efficacy'];
			}
		}

		//get average of averages
		foreach($spectral_facts_summary_counts as $key => $value) {
			$spectral_facts_summary_pivot['All Orientations'][$key] = 
				$spectral_facts_summary_sums[$key] / $spectral_facts_summary_counts[$key];
		}
		
		$this->set('spectral_facts_summary_pivot', $spectral_facts_summary_pivot);
				
		//fetch photometric summary
		$gonio_summary_temp = $this->Sample->find('all', array(
			'conditions' => array(
				'Sample.id' => $id,
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'photometric_tests',
					'alias' => 'PhotometricTests',
					'type' => 'INNER', 
					'conditions' => array(
						'Sample.id = PhotometricTests.sample_id',
					),
				),
			),
			'fields' => array(
				'AVG(PhotometricTests.wattage) as Wattage',
				'AVG(PhotometricTests.light_level) as Output',
			),
		));
		
		
		$gonio_summary['Output'] = $gonio_summary_temp[0][0]['Output'];
		$gonio_summary['Wattage'] = $gonio_summary_temp[0][0]['Wattage'];
		
		if($gonio_summary['Wattage'] != 0) {
			$value = $gonio_summary['Output'] / $gonio_summary['Wattage'];
			$gonio_summary['Efficacy'] = round($value, 2);
		}
		else {
			$gonio_summary['Efficacy'] = 'N/A';
		}
		
		$this->set('gonio_summary', $gonio_summary);
				
		// fetch flicker summary data
		$flicker_data_summary_temp = $this->Sample->find('all', array(
			'conditions' => array(
				'Sample.id' => $id,
				'FlickerTestData.filter_frequency' => array(200, 10000),
			),
			'contain' => false,
			'joins' => array(
				array(
					'table' => 'flicker_tests',
					'alias' => 'FlickerTest',
					'type' => 'INNER',
					'conditions' => array(
						'Sample.id = FlickerTest.sample_id',
					),
				),
				array(
					'table' => 'dimmers',
					'alias' => 'Dimmer',
					'type' => 'INNER',
					'conditions' => array(
						'Dimmer.id = FlickerTest.dimmer_id',
					),
				),
				array(
					'table' => 'flicker_test_data',
					'alias' => 'FlickerTestData',
					'type' => 'INNER',
					'conditions' => array(
						'FlickerTest.id = FlickerTestData.flicker_test_id',
					),
				),
			),
			'fields' => array(
				'FlickerTest.dimmer_level',
				'Dimmer.name',
				'FlickerTestData.filter_frequency',
				'AVG(FlickerTestData.fundamental_frequency) as fundamental_frequency',
				'AVG(FlickerTestData.flicker_index) as flicker_index_average',
				'AVG(FlickerTestData.flicker_percent) as flicker_percent_average',
			),
			'group' => array(
				'FlickerTest.dimmer_id',
				'FlickerTest.dimmer_level',
				'FlickerTestData.filter_frequency',
			),
			'order' => array(
				'FlickerTest.dimmer_id',
				'FlickerTest.dimmer_level',
				'FlickerTestData.filter_frequency',
			),
		));
		
		//create pivot table
		$flicker_data_summary = array();
		foreach($flicker_data_summary_temp as $value) {
			$dimmer_type = $value['Dimmer']['name'];
			
			$dimmer_level = $value['FlickerTest']['dimmer_level'];
			if($dimmer_level == '0') {
				$dimmer_level = 'min';
			}
			else {
				$dimmer_level = $dimmer_level . ' %';
			}
			
			$filter_freq = $value['FlickerTestData']['filter_frequency'];
			
			$flicker_data_summary[$dimmer_type][$dimmer_level][$filter_freq]['index'] = $value[0]['flicker_index_average'];
			$flicker_data_summary[$dimmer_type][$dimmer_level][$filter_freq]['percent'] = $value[0]['flicker_percent_average'];
			
			if ($dimmer_type == 'Power Supply' && $dimmer_level == '100 %' && $filter_freq == '10000') {
				$this->set('flicker_data_summary_frequency', $value[0]['fundamental_frequency']);
			}
		}
				
		$this->set('flicker_data_summary', $flicker_data_summary);
		
		$this->set('flicker_data_summary_dimmers', array(
			'Power Supply',
			'Dimmer A',
			'Dimmer B',
			'Dimmer C',
			'Dimmer D',
			'Dimmer E',
			'Dimmer F',
		));
		
		$this->set('flicker_data_summary_levels', array(
			'100 %', '75 %', '50 %', '25 %', 'min',
		));
	}
}