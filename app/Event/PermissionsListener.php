<?php

App::uses('CakeEventListener', 'Event');

class PermissionsListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Users.Controller.Users.afterLogin' => 'configureAuthPermissions',
        );
    }

    public function configureAuthPermissions(CakeEvent $event) {
    					
		$controller = $event->subject();
						
		// fetch user object
		$user = $controller->Auth->user();
	    if (!isset($user)) {
    		return;
		}
		
	    // fetch group aro
	    $aro = $controller->Acl->Aro->find('first', array(
	        'conditions' => array(
	            'Aro.model' => 'Group',
	            'Aro.foreign_key' => $user['group_id'],
	        ),
	    ));
		
		// user doesn't belong to any groups
		if (!isset($aro['Aro'])) {
			// fetch user aro instead
		    $aro = $controller->Acl->Aro->find('first', array(
		        'conditions' => array(
		            'Aro.model' => 'User',
		            'Aro.foreign_key' => $user['id'],
		        ),
		    ));
		}

		// user had no permissions, early return
		if (!isset($aro['Aro'])) {
			return;
		}
			
		// fetch all aco's
	    $acos = $controller->Acl->Aco->children();
	    foreach($acos as $aco){
	    	
	    	// fetch permissions for aco and aro
		    $permission = $controller->Acl->Aro->Permission->find('first', array(
		        'conditions' => array(
		            'Permission.aro_id' => $aro['Aro']['id'],
		            'Permission.aco_id' => $aco['Aco']['id'],
		        ),
		    ));
			
			// if permission exists
		    if(!isset($permission['Permission']['id'])) {
		    	continue;
			}
			// if there are any yes permissions
	        if ($permission['Permission']['_create'] == 1 ||
	            $permission['Permission']['_read'] == 1 ||
	            $permission['Permission']['_update'] == 1 ||
	            $permission['Permission']['_delete'] == 1) {
	            
				// check permissions on parent aco
            	if(!empty($permission['Aco']['parent_id'])){
            		$parentAco = $controller->Acl->Aco->find('first', array(
                        'conditions' => array(
                            'id' => $permission['Aco']['parent_id']
                        )	
                    ));
					if (!empty($parentAco)) {
	            		$controller->Session->write(
	                        'Auth.Permissions.'.$permission['Aco']['alias'],
	                        //.'.'.$parentAco['Aco']['alias'], 
	                        true
	                    );
					}
                }
				else {
					// record permission in session
	            	$controller->Session->write(
	                    'Auth.Permissions.'.$permission['Aco']['alias'],
	                     true
	                );
				}
	        }
        }
	}
}
