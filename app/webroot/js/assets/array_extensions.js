Array.prototype.sum = function() {
	var result = 0; 
	for (var i = 0, len = this.length; i < len; i++) {
		result += parseFloat(this[i]);
	}
	return result;
}

Array.prototype.mean = function() {
	var sum = this.sum();
	return sum / this.length;
}

Array.prototype.min = function() {
	var result = this[0];
	for (var i = 1, len = this.length; i < len; i++) {
		result = Math.min(result, this[i]);
	}
	return result;
}

Array.prototype.max = function() {
	var result = this[0];
	for (var i = 1, len = this.length; i < len; i++) {
		result = Math.max(result, this[i]);
	}
	return result;
}

Array.prototype.max_index = function() {
	
	if (this.length == 0) {
		return -1;
	}

	var result = -1;
	var max = this[1];
	
	for (var i = 0, len = this.length; i < len; i++) {
		if (this[i] > max) {
			result = i;
			max = this[i];
		}
	}
	return result;
}