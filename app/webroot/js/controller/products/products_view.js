var app = angular.module('project', ['productFactFilters', 'chieffancypants.loadingBar'])
	.value();

angular.module('productFactFilters', []).filter('productFactTypeFilter', function() {
	return function(types, source) {
		return types;
	};
});

app.config(function(cfpLoadingBarProvider) {
	cfpLoadingBarProvider.parentSelector = '#header';
});

app.controller('ProductsController', function ($scope, $http, cfpLoadingBar) {
	cfpLoadingBar.start();
	
	// fetch product fact value for given product, fact_name, and source
	$scope.getProductFact = function(fact_name, source) {
		// show some information
		if (source === undefined || source === null || source === '') {
			if ($scope.sources === undefined) { return; }
			$scope.sources[0];
		}
				
		if ($scope.product === undefined) {	return;	}
		
		var less_facts = $scope.product.ProductFacts.filter(function (fact) { 
			return fact.ProductFactType.name == fact_name && fact.source == source;
		});
		
		if (less_facts == undefined || less_facts.length == 0) return 'N/A';
		return less_facts[0].value;
	};
		
	// fetch sample count for given orientation
	$scope.getSpectralTestCountByConfiguration = function(configuration) {
		if ($scope.spectral_tests === undefined) {
			return 0;
		}
		
		var less_tests = $scope.spectral_tests.filter(function (spectral_test) {
			return spectral_test.configuration == configuration;
		});
		
		return less_tests.length;
	};
	
	// fetch spectra fact value for given orientation
	$scope.getSpectralFact = function(fact_type, configuration) {
		var less_facts;
		if (configuration == "All Configurations") {
			if ($scope.spectral_facts_summary === undefined) {
				return 'N/A';
			}
			
			less_facts = $scope.spectral_facts_summary.filter(function (fact) {
				return fact.SpectralTestFactType.fact_type == fact_type;
			});
		} else {
			if ($scope.spectral_facts_by_configuration === undefined) {
				return 'N/A';
			}
			
			less_facts = $scope.spectral_facts_by_configuration.filter(function (fact) {
				return fact.SpectralTestFactType.fact_type == fact_type 
					&& fact.configuration == configuration;
			});
		}
		
		if (less_facts.length == 0) {
			return 'N/A';
		}
		
		return parseFloat(less_facts[0][0].fact_average);
	};
	
	$scope.getSpectralEfficacy = function(configuration) {
		var wattage = $scope.getSpectralFact("Normal Power", configuration);
		var output  = $scope.getSpectralFact("Output", configuration);
		
		if (output !== undefined 
			&& wattage !== undefined 
			&& parseFloat(wattage) != 0) {
			return parseFloat(output) / parseFloat(wattage);
		}
	};
	
	// init controller
	var pathname_parts = window.location.pathname.split('/');
	$scope.product_id = pathname_parts[pathname_parts.length - 1];
	
	// fetch product data from server
	$http.get(window.location.pathname + '/.json').success(function(data) {
		// data
		$scope.product = data.product;
		
		// lookups
		$scope.product_fact_types = data.product_fact_types;
		
		// create and filter out sources
		$scope.sources = [];
		for (var i = 0; i < data.sources.length; i++) {
			var less_facts = $scope.product.ProductFacts.filter(function (fact) { 
				return fact.source == data.sources[i];
			});
			
			if (less_facts.length > 0) {
				$scope.sources.push(data.sources[i]);
			}
		}
		$scope.sources.sort();
	});
	
	// fetch sample/spectral_tests from server
	$http.get('/spectral_tests/index/product_id:' + $scope.product_id + '/.json').success(function(data) {
		$scope.spectral_tests = data.spectral_tests;
		
		// unique configurations
		$scope.spectral_test_configurations = {};
		for (var i = 0; i < $scope.spectral_tests.length; i++) {
			var orientation = $scope.spectral_tests[i].SpectralTestOrientationType;
			var housing = $scope.spectral_tests[i].SpectralTestHousingType;
			
			var configuration = orientation.orientation_type;
			if (housing !== undefined && housing.housing_type !== null) {
				configuration += ' - ' + housing.housing_type;
			}
			$scope.spectral_test_configurations[configuration] = true;
			
			$scope.spectral_tests[i].configuration = configuration;
		}
		
		// convert to array, add all configurations
		$scope.spectral_test_configurations = $.map($scope.spectral_test_configurations, function(val, key) { return key; });
	});
	
	// fetch spectral facts from server
	$http.get('/spectral_test_facts/summary/product_id:' + $scope.product_id + '/.json').success(function(data) {		
		$scope.spectral_facts_summary = data.spectral_test_facts;
	});
	
	// fetch spectral facts from server by orientation/housing configuration
	$http.get('/spectral_test_facts/group_by_configuration/product_id:' + $scope.product_id + '/.json').success(function(data) {		
		// data
		$scope.spectral_facts_by_configuration = data.spectral_test_facts;
		
		// set configuration data
		$scope.spectral_test_fact_configurations = {};
		for (var i = 0; i < $scope.spectral_facts_by_configuration.length; i++) {
			var orientation = $scope.spectral_facts_by_configuration[i].SpectralTestOrientationType;
			var housing = $scope.spectral_facts_by_configuration[i].SpectralTestHousingType;
			
			var configuration = orientation.orientation_type;
			if (housing !== undefined && housing.housing_type !== null) {
				configuration += ' - ' + housing.housing_type;
			}
			$scope.spectral_test_fact_configurations[configuration] = true;
			
			$scope.spectral_facts_by_configuration[i].configuration = configuration;
		}
		
		// convert to array, add all configurations
		$scope.spectral_test_fact_configurations = $.map($scope.spectral_test_fact_configurations, function(val, key) { return key; });
	});
});
