var app = angular.module('project', ['productFilters'])
	.value();

angular.module('productFilters', [])
	.filter('productLampTypeFilter', function() { return function(products, product_lamp_type) {
		if (product_lamp_type === undefined || !('ProductLampType' in product_lamp_type)) {
			return products;
		}
		return products.filter(function(product) { return product.ProductLampType.id == product_lamp_type.ProductLampType.id; });
	}; })
	.filter('productFactFilter', function() { return function(products, queries, query_mode, source) {
		var out = {};
		if (query_mode == "AND") {
			// start with all products
			out = products;
		}
		
		// track if we have any valid queries
		var valid_query = false;
		
		// iterate over queries first, 
		// faster as we can shrink our products list each query (AND only)
		for (var query_id in queries) {
			var query = queries[query_id];
			var key = query.key.ProductFactType.name;
			
			// validate query
			// fetch value
			var query_compare_func;
			var query_value;
			switch (query.key.ProductFactType.value_type) {
				case 'number':
					query_value = query.text;
					
					// create/validate comparer function
					switch (query.comparer){
						case '<':
							query_compare_func = function(a, b) { return a < b; };
							break;
						case '>':
							query_compare_func = function(a, b) { return a > b; };
							break;
						case '=':
							query_compare_func = function(a, b) { return a == b; };
							break;
						case '<=':
							query_compare_func = function(a, b) { return a <= b; };
							break;
						case '>=':
							query_compare_func = function(a, b) { return a >= b; };
							break;
						default:
							continue; // ignore query
							break;
					}
					break;
					
				case 'boolean':
					query_value = query.bool;
					break;
					
				case 'select':
					query_value = query.option;
					break;
			}
			
			// ignore query if empty
			if (query_value === '' || query_value === null || query_value === undefined) {
				continue;
			}
			
			if (query_mode == "AND") {
				// iterate over only matches from previous loop
				products = out;
				out = {};
			}
			
			// a query is valid
			valid_query = true;
			
			// loop through products and facts
			// single successful fact passes the product
			for (var product_id in products) {
			jmp_product: {
				var product = products[product_id];
				var facts = product.ProductFacts;
				
				// filter facts if source not empty
				if (source != '') {
					facts = facts.filter(function(item) { return item.source == source; });
				}
				
				for (var fact_id in facts) {
					var fact = facts[fact_id];
					var fact_value = fact.value;
					
					if (fact.ProductFactType.name != key) {
						continue; // go to next fact
					}
					
					// run query!
					switch (query.key.ProductFactType.value_type) {
						case 'number':
							if (query_compare_func(Number(fact_value), Number(query_value))) {
								out[product_id] = product;
								break jmp_product;  // early exit on success
							}
							break;
							
						case 'boolean':
						case 'select':
							if (fact_value.toLowerCase() == query_value.toLowerCase()) {
								out[product_id] = product;
								break jmp_product;  // early exit on success
							}
							break;
					}
				}
			}}
		}
		
		if (valid_query) {
			return out;
		}
		else {
			return products;
		}
	}; });

app.controller('ProductsController', function ($scope, $http, $filter) {
	// create new query
	$scope.addQuery = function() {
		$scope.queries.push({
			key: $scope.product_fact_types[0], 
		});
	};
	
	// delete query
	$scope.removeQuery = function(index) {
		$scope.queries.splice(index, 1);
	};
	
	// fetch count of all products
	$scope.productCount = function() {
		// products are kept in an object literal by product_id key
		if ($scope.products === undefined) return;
		return $.map($scope.products, function (item, key) { return item; }).length;
	};
	
	// fetch count of products after query
	$scope.filterProductCount = function() {
		// only way to get filtered count safely is to rerun the filter 
		// (accessing $scope from within the filter could trigger a process loop)
		var less_products = $filter('productFactFilter')($scope.products, $scope.queries, $scope.query_mode);
		if (less_products === undefined) return;
		return $.map(less_products, function (item, key) { return item; }).length;
	};
	
	// fetch product fact value for given product, fact_name, and source
	$scope.getFactValue = function(product, fact_name, source) {
		// show some information
		if (source === undefined || source === null || source === '') {
			source = 'CLTC';
		}
		
		var less_facts = product.ProductFacts.filter(function (fact) { 
			return fact.ProductFactType.name == fact_name && fact.source == source 
		});
		
		if (less_facts == undefined || less_facts.length == 0) return 'N/A';
		return less_facts[0].value;
	};
	
	// init controller
	// fetch data from server
	$http.get('/products/find.json').success(function(data) {
		$scope.products = data.products;
		
		// prepare product_fact_type ranges and selects
		var facts = $.map(data.products, function (item, key) { return item.ProductFacts; });
		for (var type_id in data.product_fact_types) {
			// filter relevant facts
			var name = data.product_fact_types[type_id].ProductFactType.name;
			var less_facts = facts.filter(function(item) { return item.ProductFactType.name == name; });
			
			switch (data.product_fact_types[type_id].ProductFactType.value_type) {
				case 'number':
					// fetch actual numbered values, record min/max
					var values = less_facts
						.map(function(item) { return Number(item.value); })
						.filter(function(value) { return !isNaN(value); });
					
					data.product_fact_types[type_id].ProductFactType.max = Array.max(values);
					data.product_fact_types[type_id].ProductFactType.min = Array.min(values);
					break;
					
				case 'select':
					// fetch unique values, add empty, sort
					var options = Array.unique(less_facts.map(function(item) { return item.value; }));
					options.push('');
					options.sort();
						
					data.product_fact_types[type_id].ProductFactType.options = options;
					break;
					
				case 'boolean':
					break;
			}
		}
		$scope.product_fact_types = data.product_fact_types;

		// create first query
		$scope.queries = [{
			key: $scope.product_fact_types[0], 
		}];
		
		// set filter modes
		$scope.query_modes = ['AND', 'OR'];
		$scope.query_mode = $scope.query_modes[0];
		
		// set filter dropdowns
		$scope.query_comparers = ['', '<', '>', '=', '<=', '>='];
		$scope.query_bools = ['', 'YES', 'NO'];
		
		// source dropdown
		$scope.sources = data.sources;
		$scope.sources.unshift('');
		$scope.source = $scope.sources[0];
		
		// lamp type dropdown
		$scope.product_lamp_types = data.product_lamp_types;
		$scope.product_lamp_types.unshift({});
		$scope.product_lamp_type = $scope.product_lamp_types[0];
	});
});

$(document).ready(function(){
    // Function to get the Max value in Array
    Array.max = function( array ){
        return Math.max.apply( Math, array );
    };

    // Function to get the Min value in Array
    Array.min = function( array ){
       return Math.min.apply( Math, array );
    };
    
    // Function to get the uniques in Array
    Array.unique = function(array){
	   var u = {}, a = [];
	   for(var i = 0, l = array.length; i < l; ++i){
	      if(u.hasOwnProperty(array[i])) {
	         continue;
	      }
	      a.push(array[i]);
	      u[array[i]] = 1;
	   }
	   return a;
	};
});