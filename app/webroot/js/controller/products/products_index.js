var app = angular.module('project', ['productFilters', 'chieffancypants.loadingBar'])
	.value();

angular.module('productFilters', []).filter('productLampTypeFilter', function() {
	return function(products, product_lamp_type) {
		if (product_lamp_type === undefined || !('ProductLampType' in product_lamp_type)) {
			return products;
		}
		
		return products.filter(function(product) { return product.ProductLampType.id == product_lamp_type.ProductLampType.id; });
	}
});


app.controller('ProductsController', function ($scope, $http, cfpLoadingBar) {
	cfpLoadingBar.start();
	
	// fetch product fact value for given product, fact_name, and source
	$scope.getFactValue = function(product, fact_name, source) {
		// show some information
		if (source === undefined || source === null || source === '') {
			source = 'CLTC';
		}
		
		var less_facts = product.ProductFacts.filter(function (fact) { 
			return fact.ProductFactType.name == fact_name && fact.source == source;
		});
		
		if (less_facts == undefined || less_facts.length == 0) return 'N/A';
		return less_facts[0].value;
	};
	
	$scope.submitCompare = function() {
		var href = "/products/compare?";
		if ('product' in $scope.compares[0]) {
			href += "0=" + $scope.compares[0].product.Product.id;
		}
		if ('product' in $scope.compares[1]) {
			href += "&1=" + $scope.compares[1].product.Product.id;
		}
		if ('product' in $scope.compares[2]) {
			href += "&2=" + $scope.compares[2].product.Product.id;
		}
		window.location = href;
	};
	
	$scope.toggleProduct = function(product) {	
		if (product.compare) {
			var free_compares = $scope.compares.filter(function(item) { return !('product' in item); });
			if (free_compares.length == 0) {
				product.compare = false;
				return;
			}
			
			free_compares[0].product = product;
			free_compares[0].show_img = ('ProductImages' in product) ? true : false;
		}
		else {
			var matches = $scope.compares.filter(function(item, index) { return item.product === product; });
			var index = $scope.compares.indexOf(matches[0]);
			$scope.compares.splice(index, 1);
			$scope.compares.push({});
		}
	};
	
	$scope.toggleCompare = function(compare) {
		compare.product.compare = false;
		$scope.toggleProduct(compare.product);
	};
	
	// init controller
	// fetch data from server
	$http.get('/products/index.json').success(function(data) {
		$scope.products = data.products;
				
		// source dropdown
		$scope.sources = data.sources;
		$scope.sources.sort();
		$scope.source = $scope.sources[0];
		
		// lamp type dropdown
		$scope.product_lamp_types = data.product_lamp_types;
		$scope.product_lamp_types.unshift({});
		$scope.product_lamp_type = $scope.product_lamp_types[0];
		
		$scope.compares = [{}, {}, {}];
	});
	
});