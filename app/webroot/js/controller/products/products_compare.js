var app = angular.module('project', ['productFactFilters'])
	.value();

angular.module('productFactFilters', []).filter('productFactTypeFilter', function() {
	return function(types, source) {
		return types;
	};
});


app.controller('ProductsController', function ($scope, $http) {
	
	// fetch product fact value for given product, fact_name, and source
	$scope.getFactValue = function(product, fact_name, source) {
		// show some information
		if (source === undefined || source === null || source === '') {
			source = 'CLTC';
		}
		
		var less_facts = product.ProductFacts.filter(function (fact) { 
			return fact.ProductFactType.name == fact_name && fact.source == source;
		});
		
		if (less_facts == undefined || less_facts.length == 0) return 'N/A';
		return less_facts[0].value;
	};
	
	// init controller
	// fetch data from server
	$http.get('/products/compare.json' + window.location.search).success(function(data) {
		$scope.products = data.products;
		
		$scope.product_fact_types = data.product_fact_types;
		
		// source dropdown
		$scope.sources = data.sources;
		$scope.sources.sort();
		$scope.source = $scope.sources[0];
	});
});