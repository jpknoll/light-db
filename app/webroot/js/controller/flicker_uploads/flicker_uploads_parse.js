/*
 * Author: John Knoll (jpknoll@ucdavis.edu)
 * 4/15/2014
 */
var app = angular.module('project', ['angularFileUpload', 'chieffancypants.loadingBar'])
	.value();

app.controller('FlickerController', function($scope, $http, cfpLoadingBar) {
	
	// init
	$http.get('/flicker_upload_fact_types/.json').success(function(data) {
		$scope.flicker_upload_fact_types = data.flicker_upload_fact_types;
	});
	
	/*
	 * Find FlickerUploadFactType, case insensitive
	 */
	$scope.find_flickerUploadFactType = function(flicker_upload_fact_type) {
		var flicker_upload_fact_type = flicker_upload_fact_type.toLowerCase();
		var less_types = $scope.flicker_upload_fact_types.filter(function(type) { 
			return type.FlickerUploadFactType.flicker_upload_fact_type.toLowerCase() == flicker_upload_fact_type; 
		});
		
		return less_types[0];
	};
	
	/*
	 * Add an empty fact
	 */
	$scope.add_flickerUploadFact = function() {
		if ($scope.flicker_upload_facts === undefined) {
			$scope.flicker_upload_facts = [];
		}
		$scope.flicker_upload_facts.push({
			value: '',
		});
		
		return false;
	};
	
	/*
	 * Remove fact at index
	 */
	$scope.delete_flickerUploadFact = function(index) {
		if ($scope.flicker_upload_facts === undefined) {
			return false;
		}
		$scope.flicker_upload_facts.splice(index, 1);
		
		return false;
	};
	
	/*
	 * read content of files[0]
	 */
	$scope.readFile = function($files) {
		cfpLoadingBar.start();
		
		$scope.file = $files[0];
		var reader = new FileReader();
        reader.onload = function (loadEvent) {
            $scope.file.content = loadEvent.target.result;
            $scope.processFile();
        };
        reader.readAsText($files[0]);
	};
	
	/*
	 * process facts and data from file content
	 */
	$scope.processFile = function() { 
		// process!
		$scope.generateFacts();
		$scope.generateData();
				
		cfpLoadingBar.complete();
	};
	
	$scope.generateFacts = function() {
		$scope.flicker_upload_facts = [];
		
		// process model for headers
		var content = $scope.file.content.trim();
		var lines = content.split('\n');
		
		var headers = lines[0].split(',');
		var metas   = lines[1].split(',');
		
		for (var i = 0, len = headers.length; i < len; i++) {
			var flicker_upload_fact_type = $scope.find_flickerUploadFactType(headers[i].trim());
			
			$scope.flicker_upload_facts.push({
				flicker_upload_fact_type_id: flicker_upload_fact_type.FlickerUploadFactType.id,
				value: metas[i].trim(),
			});
		}
	};
	
	$scope.generateData = function() {
		$scope.flicker_upload_data = [];
		
		// process model for signal
		var content = $scope.file.content.trim();
		var lines = content.split('\n');
		lines = lines.splice(2, lines.length - 2);
		
		var signal = lines.map(function(line) { return parseFloat(line); });
		
		// setup processor		
		$scope.flicker = new Flicker(signal, 1);
		
		var freqs = [60,120,200,240,300,500,1000,3000,10000];
		for (var i = 0; i < freqs.length; i++) {
			$scope.calculateForCutoff(freqs[i]);
		}
	};
	
	$scope.calculateForCutoff = function(cutoff) {
		var target = $scope.flicker.applyLowPass(cutoff);		
		
		var target_index = target.getIndex();
		var target_percent = target.getPercent();
		var target_fund_freq = target.getFundFreq();
		var raw_fund_freq = $scope.flicker.getFundFreq();
		
		$scope.flicker_upload_data.push({
			filter_frequency: 		cutoff,
			flicker_index: 			target_index.toFixed(3),
			flicker_percent: 		target_percent.toFixed(3),
			fundamental_frequency:	target_fund_freq,
		});
	};
		
	$scope.generateRawChart = function() {
		// build raw display set
		var raw_subset = [];
		var dt = $scope.flicker.dt;
		for (var i = 0; i < 10000; i++) {
			raw_subset.push([i * dt, $scope.flicker.signal[i]]);
		}
		var raw_average = $scope.flicker.signal.mean();
		var raw_average_subset = [[0, raw_average],[(10000-1) * dt, raw_average]];
		
		$('#raw-chart').highcharts({
			title: 'Raw Data',
			chart: {
				renderTo: 'container',
				animation: false,
			},
			plotOptions: {
				series: {
					animation: false,
				}
			},
			tooltip: {
				animation: false,
			},
			series: [{
				name: 'raw',
				data: raw_subset,
				marker: {enabled: false},
				animation: false,
				shadow: false,
			},{
				name: 'raw average',
				data: raw_average_subset,
				marker: {enabled: false},
				animation: false,
				shadow: false,
			}],
			yAxis: {
				min: 0
			},
		});
	};
	
	$scope.generateFFTChart = function() {		
		// build fft display set
		var fft_subset = [];
		var df = $scope.flicker.df;
		for (var i = 1; i < 500; i++) {
			fft_subset.push([i*df, $scope.flicker.power[i]]);
		}
		
		$('#fft-chart').highcharts({
			title: 'Frequency Domain',
			chart: {
				renderTo: 'container',
				animation: false,
			},
			plotOptions: {
				series: {
					animation: false,
				}
			},
			tooltip: {
				animation: false,
			},
			series: [{
				name: 'fft',
				data: fft_subset,
				marker: {enabled: false},
				animation: false,
				shadow: false,
			}],
		});
	};

	$scope.generateFilteredChart = function(filter_fq) {	
		var target = $scope.flicker.applyLowPass(filter_fq);	
			
		// create dataset subset and chart
		var target_dataset = []
		var dt = $scope.flicker.dt;
		for (var i = 0; i < 10000; i++) {
			target_dataset.push([i * dt, target.signal[i]]);
		}
		var target_average = target.signal.mean();
		var target_average_dataset = [[0, target_average],[(10000-1) * dt, target_average]];
		
		$('#' + filter_fq + '-hz-filtered-chart').highcharts({
			title: 'Filtered at ' + filter_fq + 'kHz',
			chart: {
				renderTo: 'container',
				animation: false,
			},
			plotOptions: {
				series: {
					animation: false,
				}
			},
			tooltip: {
				animation: false,
			},
			labels: {
				items: [
					'percent flicker: ',
					'flicker index: ',
					'filt. fund. freq.: ',
					'unfilt. fund. freq: ',
				],
			},
			series: [{
				name: filter_fq + 'kHz Filtered',
				data: target_dataset,
				marker: {enabled: false},
				animation: false,
				shadow: false,
			},{
				name: 'Average',
				data: target_average_dataset,
				marker: {enabled: false},
				animation: false,
				shadow: false,
			}],
			yAxis: {
				min: 0
			},
		});
	};
	
	$scope.uploadResults = function() {
		$
	};
});

/*
 * Input: Signal to be analyzed
 * 		  Length in seconds of signal
 */
function Flicker(signal, signal_duration) {
	
	// vars, etc
	this.signal = signal;
	this.signal_duration = signal_duration;
	this.signal_rate = signal.length / signal_duration;
	this.dt = 1 / this.signal_rate;
	this.df = this.signal_rate / signal.length;
	
	// create empty imag array, feed to fft
	this.fft_real = [];
	this.fft_imag = [];
	for (var i = 0, len = this.signal.length; i < len; i++) {
		this.fft_real[i] = this.signal[i];
		this.fft_imag[i] = 0;
	}
	transform(this.fft_real, this.fft_imag);
	
	// scale fft
	for (var i = 0, len = this.fft_imag.length; i < len; i++) {
		this.fft_real[i] *= this.dt;
		this.fft_imag[i] *= this.dt;
	}
	
	// calculate spectrum magnitude
	this.power = [];
	for (var i = 0, len = this.fft_real.length; i < len; i++) {
		this.power[i] = (this.fft_real[i] * this.fft_real[i]) + (this.fft_imag[i] * this.fft_imag[i]); 
	}
}

/*
 * Scale max down to 1
 */
Flicker.prototype.normalize = function() {
	var max = this.signal.max();
	var result = [];
	for (var i = 0, len = this.signal.length; i < len; i++) {
		result[i] = this.signal[i] / max;
	}
	return result;
}

/*
 * Returns new Flicker object
 */
Flicker.prototype.applyLowPass = function(filter_fq) {
	var fft_real_filt = [];
	var fft_imag_filt = [];
	
	// filter out higher frequencies, leaving filter_fq
	// keep symmetric frequencies
	var half_f = this.getNyquist();
	for (var i = 0, len = this.fft_real.length; i < len; i++) {
		if (i <= filter_fq) {
			fft_real_filt[i] = this.fft_real[i];
			fft_imag_filt[i] = this.fft_imag[i];
		}
		else if (i >= (len - filter_fq)) {
			fft_real_filt[i] = this.fft_real[i];
			fft_imag_filt[i] = this.fft_imag[i];
		}
		else {
			fft_real_filt[i] = 0;
			fft_imag_filt[i] = 0;
		}
	}
	inverseTransform(fft_real_filt, fft_imag_filt);
	
	// scale output
	for (var i = 0, len = fft_real_filt; i < len; i++) {
		fft_real_filt[i] *= this.df;
	}
	
	return new Flicker(fft_real_filt, this.signal_duration);
}

/*
 * Area above average / Total Area
 */
Flicker.prototype.getIndex = function() {
	var total = this.signal.sum();
	var average = total / this.signal.length;
	
	// add area above average
	var above = 0;
	for (var i = 0, len = this.signal.length; i < len; i++) {
		if (this.signal[i] > average) {
			above += (this.signal[i] - average);
		}
	}
	
	return above / total;
}

/* 
 * Ratio of light change to average(?)
 */
Flicker.prototype.getPercent = function() {
	var min = this.signal.min();
	var max = this.signal.max();
	
	return 100 * (max - min) / (max + min);
}

/*
 * Half sampling rate, point of symmetry on FFT
 */
Flicker.prototype.getNyquist = function() {
	return this.signal_rate / 2;
}

/*
 * Fundamental Frequency; from FFT, max index, ignore DC
 */
Flicker.prototype.getFundFreq = function() {
	// ignore symmetric frequencies
	var max_i = this.getNyquist() / this.df;
	
	var target = this.power.slice(1, max_i);
	return this.df * (target.max_index() + 1);
}