var app = angular.module('project', ['chieffancypants.loadingBar'])
	.value();

app.controller('FlickerController', function($scope, $http, cfpLoadingBar) {
	
	$scope.flicker_test_facts = [{}];
	
	$scope.add_flickerTestFact = function() {
		$scope.flicker_test_facts.push({});
	};
	
	$scope.delete_flickerTestFact = function(index) {
		$scope.flicker_test_facts.splice(index, 1);
	};
});